"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const log_1 = require("@uk/log");
const log = new log_1.Log("UK.BOT");
class Bot {
    constructor() {
        this.sessionMap = new Map();
        this.statesMap = new Map();
    }
    get sessions() {
        const rv = this.sessionMap.values();
        rv['length'] = this.sessionMap.size;
        return rv;
    }
    newState(handlers) {
        handlers[Bot.StateSym] = true;
        for (const item in handlers) {
            if (handlers[item] instanceof _1.Handler) {
                handlers[item]['name'] = item;
            }
        }
        return handlers;
    }
    newAbstractState(handlers) {
        return handlers;
    }
    handler(func) {
        return new _1.Handler(func);
    }
    async start(opts) {
        this.opts = opts;
        for (const namespaceName in opts.stateNamespaces) {
            const namespace = opts.stateNamespaces[namespaceName];
            if (typeof namespace === 'object') {
                for (const stateName in namespace) {
                    const state = namespace[stateName];
                    if (typeof state === 'object') {
                        if (state[Bot.StateSym] === true) {
                            state.stateName = namespaceName + "." + stateName;
                            if (this.statesMap.has(state.stateName))
                                throw new Error(`State name "${state.stateName}" is not unique!`);
                            this.statesMap.set(state.stateName, state);
                            log.debug(`Registered state ${state.stateName}`);
                        }
                    }
                }
            }
        }
        for (const conn of this.opts.connectors) {
            await conn.start(this);
        }
    }
    async onMessage(peer, msg) {
        const id = this.getSessionId(peer);
        const session = this.sessionMap.get(id) || await this.createSession(peer, id);
        const state = session.state;
        session['lastContact'] = new Date();
        if (this.opts && this.opts.onMessage) {
            if (!(await this.opts.onMessage(session, msg)))
                return;
        }
        if (msg.preCheckoutQuery && state.onConfirmationPayment) {
            return state.onConfirmationPayment(session, msg.preCheckoutQuery);
        }
        if (msg.successfulPayment && state.onSuccessfulPayment) {
            return state.onSuccessfulPayment(session, msg.successfulPayment);
        }
        if (msg.text && await session.callCallback(msg))
            return;
        if (msg.callback && await session.callCallback(msg))
            return;
        if (msg.mediaGroup && state.onMediaGroup && await state.onMediaGroup(session, msg) === true)
            return;
        if (state.onMessage && await state.onMessage(session, msg) === true)
            return;
        if (msg.command && state.onCommand)
            await state.onCommand(session, msg.command, msg);
        if (msg.contact && state.onContact)
            await state.onContact(session, msg.contact, msg);
        if (msg.location && state.onLocation)
            await state.onLocation(session, msg.location, msg);
        if (msg.images && state.onImages)
            await state.onImages(session, msg.images, msg);
        if (msg.document && state.onDocument)
            await state.onDocument(session, msg.document, msg);
        if (msg.audio && state.onAudio)
            await state.onAudio(session, msg.audio, msg);
        if (msg.videos && state.onVideos)
            await state.onVideos(session, msg.videos, msg);
        if (msg.text && state.onText)
            await state.onText(session, msg.text, msg);
        if (msg.mediaGroup && state.onMediaGroup)
            await state.onMediaGroup(session, msg);
        if (msg.sticker && state.onSticker)
            await state.onSticker(session, msg.sticker);
    }
    async createSession(peer, id) {
        const session = await this.opts.sessionFactory(peer, peer.connector);
        this.sessionMap.set(id, session);
        return session;
    }
    getSessionId(peer) {
        return peer.sessionId;
    }
}
exports.Bot = Bot;
(function (Bot) {
    Bot.StateSym = Symbol();
})(Bot = exports.Bot || (exports.Bot = {}));
//# sourceMappingURL=bot.js.map