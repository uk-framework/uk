"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("@uk/log");
const log = new log_1.Log('UK.BOTSESSION');
class Handler {
    constructor(func) {
        this.func = func;
    }
}
exports.Handler = Handler;
class Session {
    constructor(state, peer, connector) {
        this.state = state;
        this.peer = peer;
        this.connector = connector;
        this.callbacks = new Map();
        this.log = new log_1.Log('UK.BOTSESSION:' + this.peer.sessionId);
        this.lastContact = new Date(0);
    }
    static Handler(func) {
        return new Handler(func);
    }
    async callCallback(msg) {
        return new Promise(async (resolve, reject) => {
            const cbid = (msg.callback) ? msg.callback.data : msg.text;
            if (this.callbacks.get(cbid)) {
                try {
                    const test = this.callbacks.get(cbid);
                    await test.func.call(this.state, this, msg);
                    log.debug("Called: ", { name: test.name });
                    resolve(true);
                }
                catch (ex) {
                    reject(ex);
                }
            }
            else {
                resolve(false);
            }
        });
    }
    ifCallback(value) {
        return (this.callbacks.get(value)) ? true : false;
    }
    async sendAction(action) {
        await this.connector.sendAction(this.peer, action);
    }
    async confirmPayment(data) {
        await this.connector.confirmPayment(data);
    }
    async send(message, options) {
        if (typeof message === 'string') {
            message = {
                text: message
            };
        }
        if (message && message.keyboard) {
            let keys = (!message.keyboard.inline) ? message.keyboard.input : message.keyboard.inline;
            if (keys && keys.length) {
                keys.map(i => {
                    i.map(j => {
                        if (j.onClick) {
                            this.callbacks.set(j['value'] || j.text, j.onClick);
                        }
                    });
                });
            }
        }
        return this.connector.send(this.peer, message);
    }
    setState(state) {
        this.log.debug('Change state', { prev: this.state['stateName'], next: state.stateName });
        if (this.state && this.state.onLeave)
            this.state.onLeave(this);
        this['state'] = state;
        this.callbacks.clear();
        if (this.state.onSet)
            this.state.onSet(this);
        return this;
    }
}
exports.Session = Session;
//# sourceMappingURL=session.js.map