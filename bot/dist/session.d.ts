import { Peer } from "./peer";
import { State } from "./state";
import { Out } from "./out";
import { Action } from "./action";
import { Connector } from "./connector";
import { In } from '.';
export declare type HandlerFunc<TSession extends Session> = (session: TSession, message: In.Message) => void;
export declare class Handler {
    func: HandlerFunc<any>;
    readonly name: string;
    constructor(func: HandlerFunc<any>);
}
export declare class Session<TAppSession extends Session = Session<any>, TState extends State<TAppSession> = State<TAppSession>> {
    readonly state: TState;
    readonly peer: Peer;
    readonly connector: Connector;
    static Handler<TSession extends Session, TState extends State<TSession>>(this: {
        new (state: TState, peer: Peer, connector: Connector, ...args: any[]): TSession;
    }, func: HandlerFunc<TSession>): Handler;
    constructor(state: TState, peer: Peer, connector: Connector);
    protected callbacks: Map<string, Handler>;
    private log;
    readonly lastContact: Date;
    callCallback(msg: In.Message): Promise<boolean>;
    ifCallback(value: string): boolean;
    sendAction(action: Action): Promise<void>;
    confirmPayment(data: Out.ConfirmPayment): Promise<void>;
    send(message: Out.Message): Promise<In.Info | void>;
    send(message: string, options?: Out.MessageNoText): Promise<In.Info | void>;
    setState(state: TState): this;
}
