export interface Handlers<TSession extends Session> {
    onSet(session: TSession): Promise<any>;
    onLeave(session: TSession): Promise<any>;
    onMessage(session: TSession, message: In.Message): Promise<boolean | undefined | void | never>;
    onText(session: TSession, text: string, message: In.Message): Promise<any>;
    onCommand(session: TSession, command: In.Command, message: In.Message): Promise<any>;
    onContact(session: TSession, contact: In.Contact, message: In.Message): Promise<any>;
    onLocation(session: TSession, location: In.Location, message: In.Message): Promise<any>;
    onImages(session: TSession, images: In.Image[], message: In.Message): Promise<any>;
    onDocument(session: TSession, document: In.Document, message: In.Message): Promise<any>;
    onAudio(session: TSession, audio: In.Audio, message: In.Message): Promise<any>;
    onVideos(session: TSession, videos: In.Video[], message: In.Message): Promise<any>;
    onMediaGroup(session: TSession, message: In.Message): Promise<any>;
    onSticker(session: TSession, sticker: In.Sticker): Promise<any>;
    onConfirmationPayment(session: TSession, query: In.PreCheckoutQuery): Promise<any>;
    onSuccessfulPayment(session: TSession, payment: In.SuccessfulPayment): Promise<any>;
    readonly stateName: string;
}
export declare type State<TSession extends Session> = Partial<Handlers<TSession>>;
import { In } from "./in";
import { Session } from "./session";
