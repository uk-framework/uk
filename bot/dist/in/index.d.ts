export declare namespace In {
    interface Message {
        text?: string;
        images?: Image[];
        contact?: Contact;
        command?: Command;
        caption?: string;
        document?: Document;
        audio?: Audio;
        videos?: Video[];
        mediaGroup?: boolean;
        sticker?: Sticker;
        location?: Location;
        callback?: Callback;
        info: Info;
        preCheckoutQuery?: PreCheckoutQuery;
        successfulPayment?: SuccessfulPayment;
    }
    interface Info {
        id: number;
        chatId: number | string;
    }
    interface Callback {
        data: string;
    }
    interface Location {
        lat: number;
        lng: number;
    }
    interface Contact {
        name?: {
            first?: string;
            last?: string;
        };
        phoneNumber: string;
        userId?: string;
    }
    interface Img extends File {
        width?: number;
        height?: number;
    }
    interface Image {
        all: Img[];
        small?: Img;
        big?: Img;
        caption?: string;
    }
    interface Audio extends File {
        caption?: string;
    }
    interface Video extends File {
        caption?: string;
    }
    interface Document extends File {
        caption?: string;
    }
    interface Command {
        name: string;
        args?: string[];
    }
    interface File {
        id: string;
        path: string;
        mimeType?: string;
        name?: string;
        size?: number;
    }
    interface Sticker {
        id: string;
        emoji: string;
        stickerSet?: StickerSet;
    }
    interface StickerSet {
        name: string;
        stickers: Sticker[];
    }
    interface PreCheckoutQuery {
        id: string;
        currency: string;
        totalAmount: number;
        invoicePayload: string;
        orderInfo: OrderInfo;
    }
    interface OrderInfo {
        name?: string;
        phoneNumber?: string;
        email?: string;
    }
    interface SuccessfulPayment {
        currency: string;
        totalAmount: number;
        invoicePayload: string;
        shippingOptionId?: string;
        orderInfo?: OrderInfo;
        telegramPaymentChargeId: string;
        providerPaymentChargeId: string;
    }
}
