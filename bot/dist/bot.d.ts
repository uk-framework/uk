import { Connector, Session, Peer, In, State, Handler, HandlerFunc } from ".";
export declare class Bot<TSession extends Session, TState extends State<TSession> = State<TSession>> {
    protected sessionMap: Map<string, TSession>;
    protected statesMap: Map<string, Partial<import("./state").Handlers<any>>>;
    protected opts: Bot.Options<TSession>;
    get sessions(): IterableIterator<TSession> & {
        length: number;
    };
    newState<T extends TState>(handlers: T): T;
    newAbstractState<T extends TState>(handlers: T): T;
    handler(func: HandlerFunc<TSession>): Handler;
    start(opts: Bot.Options<TSession>): Promise<void>;
    protected onMessage(peer: Peer, msg: In.Message): Promise<any>;
    protected createSession(peer: Peer, id: string): Promise<TSession>;
    protected getSessionId(peer: Peer): string;
}
export declare namespace Bot {
    interface Options<TSession extends Session> {
        connectors: Connector[];
        stateNamespaces: {
            [namespace: string]: any;
        };
        sessionFactory: (peer: Peer, connector: Connector) => Promise<TSession>;
        onMessage?: (session: TSession, msg: In.Message) => Promise<boolean>;
    }
    const StateSym: unique symbol;
}
