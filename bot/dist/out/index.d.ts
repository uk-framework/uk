/// <reference types="node" />
import { Handler } from '../session';
export declare namespace Out {
    interface Edit {
        id: number;
        chatId?: number;
    }
    interface KeyBase {
        text: string;
        onClick?: Handler;
    }
    interface KeyValue extends KeyBase {
        value?: string;
        url?: string;
        share?: string;
        pay?: boolean;
    }
    interface KeyContact extends KeyBase {
        requestContact: boolean;
    }
    interface KeyLocation extends KeyBase {
        requestLocation: boolean;
    }
    type KeyNoValue = KeyBase | KeyContact | KeyLocation;
    type Key = KeyValue | KeyNoValue;
    type Keyboard = Key[][];
    type InlineKeyboard = KeyValue[][];
    interface MessageBase {
        edit?: Edit;
        keyboard?: {
            hide?: boolean;
            input?: Keyboard;
            inline?: InlineKeyboard;
            oneTime?: boolean;
            resize?: boolean;
        };
        notification?: "silent" | "full" | boolean;
        disableWebPagePreview?: boolean;
    }
    interface MessageText extends MessageBase {
        text: string;
    }
    interface MessageHTML extends MessageBase {
        html: string;
    }
    interface MessageSticker extends MessageBase {
        stickerId: string;
    }
    interface Image {
        image: Buffer | string;
        type?: string;
        caption?: string;
    }
    interface MessageImage extends MessageBase {
        images: Image[] | Image;
    }
    interface MessageVideo extends MessageBase {
        video: string;
    }
    interface MessageDocument extends MessageBase {
        document: {
            name: string;
            data: Buffer;
            type?: string;
        };
    }
    interface MessagePayment extends MessageBase {
        invoice: {
            title: string;
            description: string;
            payload: string;
            providerToken: string;
            startParameter: string;
            currency: 'UAH' | 'USD';
            prices: {
                amount: number;
                label: string;
            }[];
        };
    }
    type MessageNoText = MessageImage | MessageVideo | MessageSticker | MessageDocument | MessagePayment;
    type Message = MessageText | MessageHTML | MessageNoText;
    interface ConfirmPayment {
        queryId: string;
        ok: boolean;
        error?: string;
    }
}
