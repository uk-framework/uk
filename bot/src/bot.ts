import { Connector, Session, Peer, In, State, Handler, HandlerFunc } from "."
import { Log } from "@uk/log"

const log = new Log("UK.BOT");

export class Bot<TSession extends Session, TState extends State<TSession> = State<TSession>> {
    protected sessionMap = new Map<string, TSession>();
    protected statesMap = new Map<string, State<any>>();
    protected opts: Bot.Options<TSession>;

    get sessions(): IterableIterator<TSession> & { length: number } {
        const rv = this.sessionMap.values();
        rv['length'] = this.sessionMap.size;
        return rv as any;
    }

    newState<T extends TState>(handlers: T): T {
        handlers[Bot.StateSym] = true;
        for (const item in handlers) {
            if (handlers[item] instanceof Handler) {
                handlers[item]['name'] = item;
            }
        }
        return handlers as any;
    }

    newAbstractState<T extends TState>(handlers: T): T {
        return handlers as any;
    }

    handler(func: HandlerFunc<TSession>) {
        return new Handler(func);
    }

    async start(opts: Bot.Options<TSession>) {
        this.opts = opts;
        for (const namespaceName in opts.stateNamespaces) {
            const namespace = opts.stateNamespaces[namespaceName];
            if (typeof namespace === 'object') {
                for (const stateName in namespace) {
                    const state = namespace[stateName];
                    if (typeof state === 'object') {
                        if (state[Bot.StateSym] === true) {
                            state.stateName = namespaceName + "." + stateName;
                            if (this.statesMap.has(state.stateName)) throw new Error(`State name "${state.stateName}" is not unique!`);
                            this.statesMap.set(state.stateName, state);
                            log.debug(`Registered state ${state.stateName}`);
                        }
                    }
                }
            }
        }
        for (const conn of this.opts.connectors) {
            await conn.start(this);
        }
    }

    protected async onMessage(peer: Peer, msg: In.Message) {
        const id = this.getSessionId(peer);
        const session = this.sessionMap.get(id) || await this.createSession(peer, id);
        const state = session.state;
        (<any>session)['lastContact'] = new Date();
        if (this.opts && this.opts.onMessage) {
            if (!(await this.opts.onMessage(session, msg))) return;
        }

        if (msg.preCheckoutQuery && state.onConfirmationPayment) {
            return state.onConfirmationPayment(session, msg.preCheckoutQuery);
        }

        if (msg.successfulPayment && state.onSuccessfulPayment) {
            return state.onSuccessfulPayment(session, msg.successfulPayment);            
        }

        if (msg.text && await session.callCallback(msg)) return;
        if (msg.callback && await session.callCallback(msg)) return;

        if (msg.mediaGroup && state.onMediaGroup && await state.onMediaGroup(session, msg) === true) return;
        if (state.onMessage && await state.onMessage(session, msg) === true) return;
        if (msg.command && state.onCommand) await state.onCommand(session, msg.command, msg);
        if (msg.contact && state.onContact) await state.onContact(session, msg.contact, msg);
        if (msg.location && state.onLocation) await state.onLocation(session, msg.location, msg);
        if (msg.images && state.onImages) await state.onImages(session, msg.images, msg);
        if (msg.document && state.onDocument) await state.onDocument(session, msg.document, msg);
        if (msg.audio && state.onAudio) await state.onAudio(session, msg.audio, msg);
        if (msg.videos && state.onVideos) await state.onVideos(session, msg.videos, msg);
        if (msg.text && state.onText) await state.onText(session, msg.text, msg);
        if (msg.mediaGroup && state.onMediaGroup) await state.onMediaGroup(session, msg);
        if (msg.sticker && state.onSticker) await state.onSticker(session, msg.sticker);
    }

    protected async createSession(peer: Peer, id: string) {
        const session = await this.opts.sessionFactory(peer, peer.connector);
        this.sessionMap.set(id, session as TSession);
        return session;
    }

    protected getSessionId(peer: Peer) {
        // TODO
        return peer.sessionId;
    }
}

export namespace Bot {
    export interface Options<TSession extends Session> {
        connectors: Connector[]
        stateNamespaces: { [namespace: string]: any }
        sessionFactory: (peer: Peer, connector: Connector) => Promise<TSession>
        onMessage?: (session: TSession, msg: In.Message) => Promise<boolean>
    }
    export const StateSym = Symbol();
}
