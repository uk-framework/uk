import { Handler } from '../session'

export namespace Out {
    export interface Edit {
        id: number
        chatId?: number
    }

    export interface KeyBase {
        text: string
        onClick?: Handler
    }

    export interface KeyValue extends KeyBase {
        value?: string
        url?: string
        share?: string
        pay?: boolean
    }

    export interface KeyContact extends KeyBase {
        requestContact: boolean
    }

    export interface KeyLocation extends KeyBase {
        requestLocation: boolean
    }

    export type KeyNoValue = KeyBase | KeyContact | KeyLocation;
    export type Key = KeyValue | KeyNoValue;

    export type Keyboard = Key[][];
    export type InlineKeyboard = KeyValue[][];

    export interface MessageBase {
        edit?: Edit
        keyboard?: {
            hide?: boolean
            input?: Keyboard
            inline?: InlineKeyboard;
            oneTime?: boolean;
            resize?: boolean;
        };
        notification?: "silent" | "full" | boolean
        disableWebPagePreview?: boolean
    }

    export interface MessageText extends MessageBase {
        text: string
    }
    export interface MessageHTML extends MessageBase {
        html: string
    }

    export interface MessageSticker extends MessageBase {
        stickerId: string
    }

    export interface Image {
        image: Buffer | string
        type?: string,
        caption?: string
    }

    export interface MessageImage extends MessageBase {
        images: Image[] | Image
    }

    export interface MessageVideo extends MessageBase {
        video: string
    }
    export interface MessageDocument extends MessageBase {
        document: {
            name: string,
            data: Buffer,
            type?: string
        }
    }
    export interface MessagePayment extends MessageBase {
        invoice: {
            title: string
            description: string
            payload: string
            providerToken: string
            startParameter: string
            currency: 'UAH' | 'USD'
            prices: { amount: number, label: string }[]
        }
    }
    export type MessageNoText = MessageImage | MessageVideo | MessageSticker | MessageDocument | MessagePayment;
    export type Message = MessageText | MessageHTML | MessageNoText;

    export interface ConfirmPayment {
        queryId: string;
        ok: boolean;
        error?: string;
    }
}
