import { Bot } from "./bot"
import { Out } from "./out"
import { In } from "./in"
import { Action } from "./action"
import { Peer } from "./peer"
import { Session } from "./session"

export abstract class Connector {
    abstract start(bot: Bot<any>): void
    abstract send(peer: Peer, message: Out.Message): Promise<In.Info | void>
    abstract sendAction(peer: Peer, action: Action): Promise<void>
    abstract confirmPayment(data: Out.ConfirmPayment): Promise<void>
    abstract getInfoBot(): Promise<Peer>
}
