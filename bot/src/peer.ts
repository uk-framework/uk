import { Connector } from "."

export interface Peer {
    id: string
    sessionId: string
    connector: Connector
    username: string
    name?: {
        first: string
        last: string
    }
}
