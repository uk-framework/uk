import { Peer } from "./peer"
import { State } from "./state"
import { Out } from "./out"
import { Action } from "./action";
import { Connector } from "./connector"
import { Bot } from "./bot"
import { Log } from "@uk/log"
import { In } from '.'

const log = new Log('UK.BOTSESSION');

export type HandlerFunc<TSession extends Session> = (session: TSession, message: In.Message) => void

export class Handler {
    readonly name: string

    constructor(public func: HandlerFunc<any>) {
    }
}

export class Session<TAppSession extends Session = Session<any>, TState extends State<TAppSession> = State<TAppSession>> {
    static Handler<TSession extends Session, TState extends State<TSession>>(
        this: { new(state: TState, peer: Peer, connector: Connector, ...args: any[]): TSession },
        func: HandlerFunc<TSession>
    ) {
        return new Handler(func);
    }

    constructor(readonly state: TState, readonly peer: Peer, readonly connector: Connector) {
    }

    protected callbacks = new Map<string, Handler>();
    private log = new Log('UK.BOTSESSION:' + this.peer.sessionId);
    readonly lastContact = new Date(0);

    public async callCallback(msg: In.Message): Promise<boolean> {
        return new Promise<boolean>(async (resolve: any, reject: any) => {
            const cbid = (msg.callback) ? msg.callback.data : msg.text;
            if (this.callbacks.get(cbid)) {
                try {
                    const test = this.callbacks.get(cbid);
                    await test.func.call(this.state, this, msg);
                    log.debug("Called: ", { name: test.name })
                    resolve(true);
                } catch (ex) {
                    reject(ex);
                }
            } else {
                resolve(false);
            }
        });
    }

    ifCallback(value: string) {
        return (this.callbacks.get(value)) ? true : false;
    }

    async sendAction(action: Action): Promise<void> {
        await this.connector.sendAction(this.peer, action);
    }

    async confirmPayment(data: Out.ConfirmPayment): Promise<void> {
        await this.connector.confirmPayment(data);
    }

    async send(message: Out.Message): Promise<In.Info | void>;
    async send(message: string, options?: Out.MessageNoText): Promise<In.Info | void>;
    async send(message: Out.Message | string, options?: Out.MessageNoText): Promise<In.Info | void> {
        if (typeof message === 'string') {
            message = {
                text: message
            }
        }
        if (message && message.keyboard) {
            let keys = (!message.keyboard.inline) ? message.keyboard.input : message.keyboard.inline;
            if (keys && keys.length) {
                keys.map(i => {
                    i.map(j => {
                        if (j.onClick) {
                            this.callbacks.set(j['value'] || j.text, j.onClick);
                        }
                    })
                })
            }
        }
        return this.connector.send(this.peer, message);
    }

    setState(state: TState) {
        this.log.debug('Change state', { prev: this.state['stateName'], next: state.stateName })
        if (this.state && this.state.onLeave) this.state.onLeave(this as any);
        (<any>this)['state'] = state;
        this.callbacks.clear();
        if (this.state.onSet) this.state.onSet(this as any);
        return this;
    }
}
