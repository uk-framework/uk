export namespace In {
    export interface Message {
        text?: string
        images?: Image[]
        contact?: Contact
        command?: Command
        caption?: string
        document?: Document
        audio?: Audio
        videos?: Video[]
        mediaGroup?: boolean,
        sticker?: Sticker
        location?: Location
        callback?: Callback
        info: Info
        preCheckoutQuery?: PreCheckoutQuery
        successfulPayment?: SuccessfulPayment
    }

    export interface Info {
        id: number
        chatId: number | string
    }

    export interface Callback {
        data: string
    }

    export interface Location {
        lat: number
        lng: number
    }
    export interface Contact {
        name?: {
            first?: string;
            last?: string;
        };
        phoneNumber: string;
        userId?: string;
    }
    export interface Img extends File {
        width?: number
        height?: number
    }
    export interface Image {
        all: Img[]
        small?: Img
        big?: Img
        caption?: string
    }
    export interface Audio extends File {
        caption?: string
    }
    export interface Video extends File {
        caption?: string
    }
    export interface Document extends File {
        caption?: string
    }
    export interface Command {
        name: string
        args?: string[]
    }
    export interface File {
        id: string
        path: string
        mimeType?: string
        name?: string
        size?: number
    }
    export interface Sticker {
        id: string,
        emoji: string,
        stickerSet?: StickerSet
    }
    export interface StickerSet {
        name: string,
        stickers: Sticker[]
    }
    export interface PreCheckoutQuery {
      id: string
      currency: string
      totalAmount: number
      invoicePayload: string
      orderInfo: OrderInfo
    }

    export interface OrderInfo {
      name?:	string;
      phoneNumber?: string;
      email?: string;
    }

    export interface SuccessfulPayment {
      currency: string;
      totalAmount: number;
      invoicePayload:	string;
      shippingOptionId?: string;
      orderInfo?: OrderInfo;
      telegramPaymentChargeId: string;
      providerPaymentChargeId: string;
    }
}
