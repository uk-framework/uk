import * as TelegramBot from "node-telegram-bot-api";
import { Connector, Bot, Out, In, Action, Peer } from "@uk/bot";
import { Log } from "@uk/log";
export declare class TelegramConnector extends Connector {
    readonly opts: TelegramConnector.Options;
    protected bot: Bot<any>;
    protected tgbot: TelegramBot;
    protected log: Log;
    protected botInfo: Peer;
    constructor(opts: TelegramConnector.Options);
    start(bot: Bot<any>): Promise<void>;
    getInfoBot(): Promise<Peer>;
    send(peer: Peer, msg: Out.Message): Promise<In.Info | void>;
    serializeMarkdown(msg: Out.Message, sendopts: TelegramBot.SendMessageOptions): void;
    private deserializeMessageResualt;
    sendAction(peer: Peer, action: Action): Promise<void>;
    confirmPayment(data: Out.ConfirmPayment): Promise<void>;
    serializeAction(action: Action): "typing" | "upload_photo";
    private deserializeCallback;
    private deserializeSuccessfulPayment;
    private deserializePreCheckoutQuery;
    private mediaGroups;
    private onMessage;
    private deserializeLocation;
    private deserializeContact;
    private deserializePeer;
    private deserializeText;
    private deserializeImages;
    private handleMediaGroup;
    private deserializeDocument;
    private deserializeAudio;
    private deserializeVoice;
    private deserializeVideos;
    private deserializeVideoNote;
    private deserializeCommands;
    private deserializeSticker;
}
export declare namespace TelegramConnector {
    interface Options {
        key: string;
        mediaGroupTimeout?: number;
        webHook?: {
            url: string;
        };
    }
}
