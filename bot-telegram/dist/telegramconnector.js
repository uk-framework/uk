"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TelegramBot = require("node-telegram-bot-api");
const bot_1 = require("@uk/bot");
const log_1 = require("@uk/log");
const tool_1 = require("@uk/tool");
const log = new log_1.Log("UK.BOTTG");
class TelegramConnector extends bot_1.Connector {
    constructor(opts) {
        super();
        this.opts = opts;
        this.log = new log_1.Log(log.tag);
        this.mediaGroups = new Map();
        if (!this.opts.mediaGroupTimeout)
            this.opts.mediaGroupTimeout = 1000;
    }
    async start(bot) {
        return log.try(log_1.Log.Level.INFO, "start", async (lp) => {
            if (!this.opts.key)
                throw new Error("Telegram bot key is required!");
            this.bot = bot;
            this.tgbot = new TelegramBot(this.opts.key, {
                polling: true
            });
            lp.finally({
                botInfo: Object.assign(Object.assign({}, this.botInfo = await this.getInfoBot()), { connector: undefined })
            });
            this.tgbot.on('message', (msg) => {
                this.onMessage(msg);
            });
            this.tgbot.on('callback_query', (callback) => {
                this.onMessage(callback.message, callback);
            });
            this.tgbot.on('pre_checkout_query', async (preCheckoutQuery) => {
                const tgmsg = {
                    message_id: preCheckoutQuery.id,
                    chat: {
                        id: preCheckoutQuery.from.id,
                    },
                    from: preCheckoutQuery.from,
                };
                this.onMessage(tgmsg, null, preCheckoutQuery);
            });
        });
    }
    async getInfoBot() {
        const rv = await this.tgbot.getMe();
        if (rv instanceof Error) {
            this.log.error(rv.message);
        }
        else {
            const bot = rv;
            return {
                connector: this,
                id: bot.id.toString(),
                sessionId: undefined,
                name: {
                    first: bot.first_name,
                    last: bot.last_name
                },
                username: bot.username
            };
        }
    }
    async send(peer, msg) {
        const rv = this.log.try(log_1.Log.Level.DEBUG, "send", async (payload) => {
            const sendopts = {};
            let msgResult;
            if ('disableWebPagePreview' in msg) {
                sendopts.disable_web_page_preview = msg.disableWebPagePreview;
            }
            ;
            if ('keyboard' in msg) {
                const keyboard = msg.keyboard;
                if (keyboard.hide) {
                    sendopts.reply_markup = {
                        remove_keyboard: true
                    };
                }
                else if (keyboard.input) {
                    sendopts.reply_markup = {
                        keyboard: keyboard.input.map(r => r.map((k) => {
                            return {
                                text: k.text,
                                request_contact: k["requestContact"]
                            };
                        })),
                        resize_keyboard: (keyboard.resize === true || keyboard.resize === false) ? keyboard.resize : true,
                        one_time_keyboard: keyboard.oneTime
                    };
                }
                else if (keyboard.inline) {
                    sendopts.reply_markup = {
                        inline_keyboard: keyboard.inline.map(r => r.map((k) => {
                            return {
                                text: k.text,
                                callback_data: k.value,
                                url: k.url,
                                switch_inline_query: k.share
                            };
                        }))
                    };
                }
            }
            if ('edit' in msg && msg.edit) {
                let options = {};
                let text;
                if ('text' in msg) {
                    options.parse_mode = 'Markdown';
                    this.serializeMarkdown(msg, sendopts);
                    text = msg.text;
                }
                if ('html' in msg) {
                    options.parse_mode = 'HTML';
                    text = msg.html;
                }
                if ('keyboard' in msg && msg.keyboard.inline) {
                    if ('inline_keyboard' in sendopts.reply_markup) {
                        options.reply_markup = sendopts.reply_markup;
                        options.inline_message_id = msg.edit.id.toString();
                    }
                }
                options.message_id = msg.edit.id;
                options.chat_id = msg.edit.chatId;
                if (text)
                    await this.tgbot.editMessageText(text, options);
                return;
            }
            if ('invoice' in msg) {
                msgResult = await this.tgbot.sendInvoice(peer.id, msg.invoice.title, msg.invoice.description, msg.invoice.payload, msg.invoice.providerToken, msg.invoice.startParameter, msg.invoice.currency, msg.invoice.prices, Object.assign({}, sendopts));
            }
            if ('stickerId' in msg) {
                await this.tgbot.sendSticker(peer.id, msg.stickerId, sendopts);
            }
            if ('text' in msg) {
                sendopts.parse_mode = 'Markdown';
                this.serializeMarkdown(msg, sendopts);
                msgResult = await this.tgbot.sendMessage(peer.id, msg.text, sendopts);
            }
            if ('html' in msg) {
                sendopts.parse_mode = 'HTML';
                msgResult = await this.tgbot.sendMessage(peer.id, msg.html, sendopts);
            }
            if ('document' in msg) {
                await this.tgbot.sendDocument(peer.id, msg.document.data, sendopts, {
                    filename: msg.document.name,
                    contentType: msg.document.type
                });
            }
            if ('images' in msg) {
                if (msg.images instanceof Array) {
                    let media = [];
                    for (const img of msg.images) {
                        if (typeof img.image == 'string') {
                            media.push({
                                type: 'photo',
                                media: img.image,
                            });
                        }
                    }
                    const opts = {
                        disable_notification: sendopts.disable_notification,
                        reply_to_message_id: sendopts.reply_to_message_id
                    };
                    await this.tgbot.sendMediaGroup(peer.id, media, opts);
                }
                else {
                    const opts = {
                        disable_notification: sendopts.disable_notification,
                        reply_to_message_id: sendopts.reply_to_message_id,
                        caption: msg.images.caption,
                        reply_markup: sendopts.reply_markup
                    };
                    await this.tgbot.sendPhoto(peer.id, msg.images.image, opts);
                }
            }
            return this.deserializeMessageResualt(msgResult);
        });
        if (rv instanceof Error)
            throw rv;
        else
            return rv;
    }
    serializeMarkdown(msg, sendopts) {
        if ('text' in msg) {
            [/\*/gm, /`/gm, /_/gm, /```/gm].forEach(regexp => {
                if (!sendopts.parse_mode)
                    return;
                const arr = msg.text.match(regexp);
                if ((arr && !!(arr.length % 2)))
                    sendopts.parse_mode = undefined;
            });
            if (!sendopts.parse_mode)
                return;
            const arr = msg.text.match(/\[/gm);
            const arr2 = msg.text.match(/\]/gm);
            if ((arr && !arr2) || (!arr && arr2))
                sendopts.parse_mode = undefined;
            if ((arr && arr2 && !!((arr.length + arr2.length) % 2)))
                sendopts.parse_mode = undefined;
        }
    }
    deserializeMessageResualt(result) {
        if (!result)
            return;
        if (result instanceof Error) {
            this.log.error(result);
            return;
        }
        else {
            return {
                id: result.message_id,
                chatId: result.chat.id
            };
        }
    }
    async sendAction(peer, action) {
        await this.tgbot.sendChatAction(peer.id, this.serializeAction(action));
    }
    async confirmPayment(data) {
        await this.tgbot.answerPreCheckoutQuery(data.queryId, data.ok, { error_message: data.error });
    }
    serializeAction(action) {
        let tgAction;
        switch (action) {
            case 'sendingPhoto':
                tgAction = 'upload_photo';
                break;
            default: tgAction = 'typing';
        }
        return tgAction;
    }
    deserializeCallback(ctx) {
        ctx.ukmsg.callback = {
            data: ctx.tgcb.data
        };
    }
    deserializeSuccessfulPayment(ctx) {
        const { successful_payment: payment } = ctx.tgmsg;
        if (payment) {
            ctx.ukmsg.successfulPayment = {
                currency: payment.currency,
                totalAmount: payment.total_amount,
                invoicePayload: payment.invoice_payload,
                telegramPaymentChargeId: payment.telegram_payment_charge_id,
                providerPaymentChargeId: payment.provider_payment_charge_id,
            };
            if (payment.order_info) {
                ctx.ukmsg.successfulPayment.orderInfo = {
                    email: payment.order_info.email,
                    name: payment.order_info.name,
                    phoneNumber: payment.order_info.phone_number,
                };
            }
        }
    }
    deserializePreCheckoutQuery(ctx) {
        const { tgPreCheckout } = ctx;
        ctx.peer = {
            connector: this,
            id: ctx.tgPreCheckout.from.id.toString(),
            sessionId: `${this.botInfo.id.toString()}${ctx.tgPreCheckout.from.id.toString()}`,
            name: {
                first: ctx.tgPreCheckout.from.first_name,
                last: ctx.tgPreCheckout.from.last_name
            },
            username: ctx.tgPreCheckout.from.username
        };
        ctx.ukmsg.preCheckoutQuery = {
            id: tgPreCheckout.id,
            currency: tgPreCheckout.currency,
            totalAmount: tgPreCheckout.total_amount,
            invoicePayload: tgPreCheckout.invoice_payload,
            orderInfo: tgPreCheckout.order_info,
        };
    }
    async onMessage(tgmsg, callback, preCheckoutQuery) {
        this.log.debug('onMessage', {
            tgmsg,
            callback,
            preCheckoutQuery
        });
        const ctx = {
            peer: undefined,
            tgmsg: tgmsg,
            ukmsg: { info: { id: tgmsg.message_id, chatId: tgmsg.chat.id } },
            tgcb: callback,
            tgPreCheckout: preCheckoutQuery,
        };
        this.deserializePeer(ctx);
        const mediagrpid = tgmsg['media_group_id'];
        if (mediagrpid && mediagrpid.length) {
            let mg = this.mediaGroups.get(mediagrpid);
            if (mg) {
                mg.messages.push(tgmsg);
                clearTimeout(mg.timer);
                mg.timer = setTimeout(() => this.handleMediaGroup(mediagrpid), this.opts.mediaGroupTimeout);
            }
            else {
                mg = {
                    messages: [tgmsg],
                    timer: setTimeout(() => this.handleMediaGroup(mediagrpid), this.opts.mediaGroupTimeout)
                };
                this.mediaGroups.set(mediagrpid, mg);
                await this.bot['onMessage'](ctx.peer, { mediaGroup: true, info: { id: tgmsg.message_id, chatId: tgmsg.chat.id } });
            }
        }
        else {
            try {
                if (callback) {
                    ctx.tgmsg = undefined;
                    this.deserializeCallback(ctx);
                    this.tgbot.answerCallbackQuery({ callback_query_id: callback.id });
                }
                else if (preCheckoutQuery) {
                    ctx.tgmsg = undefined;
                    this.deserializePreCheckoutQuery(ctx);
                }
                else {
                    await this.deserializeText(ctx);
                    await this.deserializeImages(ctx);
                    await this.deserializeDocument(ctx);
                    await this.deserializeAudio(ctx);
                    await this.deserializeVoice(ctx);
                    await this.deserializeVideos(ctx);
                    await this.deserializeVideoNote(ctx);
                    await this.deserializeSticker(ctx);
                    this.deserializeCommands(ctx);
                    this.deserializeContact(ctx);
                    this.deserializeLocation(ctx);
                    this.deserializeSuccessfulPayment(ctx);
                }
                await this.bot['onMessage'](ctx.peer, ctx.ukmsg);
            }
            catch (err) {
                this.log.error(err);
            }
        }
    }
    deserializeLocation(ctx) {
        if (ctx.tgmsg.location !== undefined) {
            ctx.ukmsg.location = {
                lat: ctx.tgmsg.location.latitude,
                lng: ctx.tgmsg.location.longitude
            };
        }
    }
    deserializeContact(ctx) {
        if (ctx.tgmsg.contact !== undefined) {
            ctx.ukmsg.contact = {
                phoneNumber: ctx.tgmsg.contact.phone_number,
                name: {
                    last: ctx.tgmsg.contact.last_name,
                    first: ctx.tgmsg.contact.first_name,
                },
                userId: ctx.tgmsg.contact.user_id.toString()
            };
        }
    }
    deserializePeer(ctx) {
        ctx.peer = {
            connector: this,
            id: ctx.tgmsg.from.id.toString(),
            sessionId: `${this.botInfo.id.toString()}${ctx.tgmsg.chat.id.toString()}`,
            name: {
                first: ctx.tgmsg.from.first_name,
                last: ctx.tgmsg.from.last_name
            },
            username: ctx.tgmsg.from.username
        };
    }
    async deserializeText(ctx) {
        ctx.ukmsg.text = ctx.tgmsg.text;
    }
    async deserializeImages(ctx) {
        if (ctx.tgmsg.photo !== undefined) {
            let image = {
                all: [],
                caption: ctx.tgmsg.caption,
            };
            for (let item of ctx.tgmsg.photo) {
                const path = await this.tgbot.getFileLink(item.file_id);
                if (typeof path === 'string') {
                    const img = {
                        path: path,
                        id: item.file_id,
                        size: item.file_size,
                        height: item.height,
                        width: item.width,
                        mimeType: `image/${tool_1.parsePath(path).ext.replace(/^\./, '')}`,
                        name: tool_1.parsePath(path).basename
                    };
                    image.all.push(img);
                    if (item === ctx.tgmsg.photo[0]) {
                        image.small = img;
                    }
                    else if (item === ctx.tgmsg.photo[ctx.tgmsg.photo.length - 1]) {
                        image.big = img;
                    }
                }
                else
                    this.log.error(path);
            }
            ctx.ukmsg.images = ctx.ukmsg.images || [];
            ctx.ukmsg.images.push(image);
        }
    }
    async handleMediaGroup(mediaGroupId) {
        const mg = this.mediaGroups.get(mediaGroupId);
        this.mediaGroups.delete(mediaGroupId);
        const ctx = {
            peer: undefined,
            tgmsg: mg.messages[0],
            ukmsg: {}
        };
        this.deserializePeer(ctx);
        for (const tgmsg of mg.messages) {
            tgmsg['media_group_id'] = '';
            ctx.tgmsg = tgmsg;
            await this.deserializeImages(ctx);
            await this.deserializeVideos(ctx);
        }
        await this.bot['onMessage'](ctx.peer, ctx.ukmsg);
    }
    async deserializeDocument(ctx) {
        if (ctx.tgmsg.document !== undefined) {
            ctx.ukmsg.caption = ctx.tgmsg.caption;
            const path = await this.tgbot.getFileLink(ctx.tgmsg.document.file_id);
            if (typeof (path) === 'string') {
                ctx.ukmsg.document = {
                    id: ctx.tgmsg.document.file_id,
                    path: path,
                    name: ctx.tgmsg.document.file_name,
                    mimeType: ctx.tgmsg.document.mime_type,
                    size: ctx.tgmsg.document.file_size
                };
            }
            else
                this.log.error(path);
        }
    }
    async deserializeAudio(ctx) {
        if (ctx.tgmsg.audio !== undefined) {
            ctx.ukmsg.caption = ctx.tgmsg.caption;
            const path = await this.tgbot.getFileLink(ctx.tgmsg.audio.file_id);
            if (typeof (path) === 'string') {
                ctx.ukmsg.audio = {
                    id: ctx.tgmsg.audio.file_id,
                    path: path,
                    name: ctx.tgmsg.audio.title,
                    mimeType: ctx.tgmsg.audio.mime_type,
                    size: ctx.tgmsg.audio.file_size
                };
            }
            else
                this.log.error(path);
        }
    }
    async deserializeVoice(ctx) {
        if (ctx.tgmsg.voice !== undefined) {
            ctx.ukmsg.caption = ctx.tgmsg.caption;
            const path = await this.tgbot.getFileLink(ctx.tgmsg.voice.file_id);
            if (typeof (path) === 'string') {
                ctx.ukmsg.audio = {
                    id: ctx.tgmsg.voice.file_id,
                    path: path,
                    name: tool_1.parsePath(path).basename,
                    mimeType: ctx.tgmsg.voice.mime_type,
                    size: ctx.tgmsg.voice.file_size
                };
            }
            else
                this.log.error(path);
        }
    }
    async deserializeVideos(ctx) {
        if (ctx.tgmsg.video !== undefined) {
            const path = await this.tgbot.getFileLink(ctx.tgmsg.video['file_id']);
            if (typeof (path) === 'string') {
                const video = {
                    id: ctx.tgmsg.video['file_id'],
                    path: path,
                    name: tool_1.parsePath(path).basename,
                    mimeType: ctx.tgmsg.video.mime_type,
                    caption: ctx.tgmsg.caption,
                    size: ctx.tgmsg.video['file_size']
                };
                ctx.ukmsg.videos = ctx.ukmsg.videos || [];
                ctx.ukmsg.videos.push(video);
            }
            else
                this.log.error(path);
        }
    }
    async deserializeVideoNote(ctx) {
        if (ctx.tgmsg.video_note !== undefined) {
            const path = await this.tgbot.getFileLink(ctx.tgmsg.video_note.file_id);
            if (typeof (path) === 'string') {
                const video = {
                    id: ctx.tgmsg.video_note.file_id,
                    path: path,
                    name: tool_1.parsePath(path).basename,
                    mimeType: "video/mp4",
                    caption: ctx.tgmsg.caption,
                    size: ctx.tgmsg.video_note.file_size
                };
                ctx.ukmsg.videos = ctx.ukmsg.videos || [];
                ctx.ukmsg.videos.push(video);
            }
            else
                this.log.error(path);
        }
    }
    deserializeCommands(ctx) {
        if (ctx.tgmsg.text && ctx.tgmsg.text.startsWith('/')) {
            let cmd = ctx.tgmsg.text.split(' ');
            ctx.ukmsg.command = { name: cmd[0], args: cmd.slice(1, cmd.length) };
            delete ctx.ukmsg.text;
        }
    }
    async deserializeSticker(ctx) {
        if (ctx.tgmsg.sticker != undefined) {
            let set;
            try {
                set = await this.tgbot['getStickerSet'](ctx.tgmsg.sticker.set_name);
            }
            catch (_a) {
                set = undefined;
            }
            ctx.ukmsg.sticker = {
                id: ctx.tgmsg.sticker.file_id,
                emoji: ctx.tgmsg.sticker.emoji,
                stickerSet: set && {
                    name: ctx.tgmsg.sticker.set_name,
                    stickers: set.stickers.map((item) => {
                        return {
                            id: item.file_id,
                            emoji: item.emoji
                        };
                    })
                }
            };
        }
    }
}
exports.TelegramConnector = TelegramConnector;
//# sourceMappingURL=telegramconnector.js.map