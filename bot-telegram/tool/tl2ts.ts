const schema = require('telegram-mtproto/schema/api-57.json');

(function () {
    let tab = 0;

    function cap(str: string) {
        return str.charAt(0).toUpperCase() + str.substr(1);
    }

    function out(line?: string): void {
        let t = tab;
        if (line === undefined) line = "";
        while (t-- > 0) line = "    " + line;
        console.log(line || "");
    }

    const types = {} as {
        [ns: string]: {
            [type: string]: {
                [predicate: string]: {
                    id?: number
                    params?: { name: string, type: string }[],
                    predicate?: string
                }
            }
        }
    }

    const methods = {} as {
        [ns: string]: {
            [method: string]: {
                method: string
                type: string
                params: { name: string, type: string }[]
            }
        }
    }

    for (const tgctor of schema.constructors) {
        const { predicate, type } = tgctor;
        const names = predicate.split('.');
        const ns = names.length > 1 ? names[0] : "global";
        const pn = names.length > 1 ? names[1] : predicate;
        const tn = names.length > 1 ? type.split(".")[1] : type;

        const nsobj = types[ns] = types[ns] || {};
        const typeobj = nsobj[tn] = nsobj[tn] || {};
        const predobj = typeobj[pn] = typeobj[pn] || tgctor;
        //    out(`export interface ${} {}`);
    }
    for (const tgmethod of schema.methods) {
        const names = tgmethod.method.split('.');
        const ns = names[0];
        const mn = names[1];
        const nsobj = methods[ns] = methods[ns] || {};
        nsobj[mn] = tgmethod;
    }

    out('// Generated with tl2ts by koloboid@gmail.com');
    out();
    out('export abstract class Telegram {');
    tab++;
    out();
    out('abstract call<T>(name: string, args?: object): Promise<T>;');
    out();
    for (const ns in types) {
        if (ns !== 'global') {
            out(`${ns} = new Telegram.${cap(ns)}(this);`);
        }
    }
    tab--;
    out('}');
    out();
    out(`export namespace Telegram {`);
    tab++;
    out(`export class Namespace { constructor(protected _$: Telegram) {} }`);
    out(`export interface TGObject<T> { _: T }`);
    out();
    for (const ns in types) {
        const nsobj = types[ns];

        out(`export class ${cap(ns)} extends Namespace {`);
        tab++;
        for (const mn in methods[ns]) {
            const tgmethod = methods[ns][mn];
            if (tgmethod.params.length === 0) {
                out(`${mn}() {`);
            } else {
                out(`${mn}(args: ${cap(ns)}.Args.${cap(mn)}) {`);
            }
            tab++;
            out(`return this._$.call<${translateType(tgmethod.type)}>('${ns}.${mn}'${tgmethod.params.length ? ', args' : ''});`);
            tab--;
            out('}');
        }
        tab--;
        out(`}`);

        out(`export namespace ${cap(ns)} {`);
        tab++;
        for (const tn in nsobj) {
            const typeobj = nsobj[tn];
            switch (tn) {
                case 'Bool':
                    out('export type Bool = boolean;');
                    continue;
                case 'True':
                    out('export type True = boolean;');
                    continue;
                case 'Vector t':
                    out('export type Vector<T> = Array<T>;');
                    continue;
                case 'Null':
                    out('export type Null = null;');
                    continue;
            }
            let predicates = "";
            for (const predicatename in typeobj) {
                const predicate = typeobj[predicatename];
                predicates += predicatename + " | ";
                out(`export interface ${predicatename} {`);
                tab++;
                out(`_: "${predicate.predicate}"`);
                if (predicate.params) {
                    for (const param of predicate.params) {
                        out(`${param.name}${typeWithSemicolon(param.type)}`);
                    }
                }
                tab--;
                out(`}`);
            }
            predicates = predicates.substr(0, predicates.length - 3);
            out(`export type ${tn} = ${predicates};`);
//            if (tn.startsWith('Input')) {
                out(`export namespace ${tn} {`);
                tab++;
                out(`export namespace Args {`);
                tab++;
                for (const predicatename in typeobj) {
                    const predicate = typeobj[predicatename];
                    if (predicate.params && predicate.params.length > 0) {
                        out(`export interface ${predicatename} {`);
                        tab++;
                        for (const param of predicate.params) {
                            out(`${param.name}${typeWithSemicolon(param.type)}`);
                        }
                        tab--;
                        out(`}`);
                    }
                }
                tab--;
                out(`}`);
                for (const predicatename in typeobj) {
                    const predicate = typeobj[predicatename];
                    const args = predicate.params.length > 0 ? `args: Args.${predicatename}` : "";
                    out(`export function ${predicatename}(${args}): ${predicatename} {`);
                    tab++;
                    out(`return { ${args ? "...args, " : ""} _: "${predicate.predicate}" }`);
                    tab--;
                    out(`}`);
                    out(`export function is${cap(predicatename)}(val: ${tn}): val is ${predicatename} {`);
                    tab++;
                    out(`return val && val._ === '${predicate.predicate}';`);
                    tab--;
                    out(`}`);
                }
                tab--;
                out(`}`);
//            }
        }
        out(`export namespace Args {`);
        tab++;
        for (const mn in methods[ns]) {
            const tgmethod = methods[ns][mn];
            if (tgmethod.params.length > 0) {
                out(`export interface ${cap(mn)} {`);
                tab++;
                for (const arg of tgmethod.params) {
                    out(`${arg.name}${typeWithSemicolon(arg.type)}`);
                }
                tab--;
                out('}');
            }
        }
        tab--;
        out(`}`);
        tab--;
        out(`}`);
    }
    tab--;

    out('}');

    function typeWithSemicolon(type: string): string {
        if (type.startsWith("flags") || type === '#') return "?: " + translateType(type);
        else return ": " + translateType(type);
    }
    function translateType(type: string): string {
        switch (type) {
            case 'true':
                return 'boolean /* true */';
            case 'int':
                return 'number /* int */';
            case 'string':
                return 'string';
            case 'long':
                return 'string /* long */';
            case 'double':
                return 'number /* double */';
            case 'bytes':
                return 'number[]';
            case '#':
                return 'number'
        }
        if (type.startsWith('Vector')) return translateType(type.substring(7, type.length - 1)) + "[]";
        if (type.startsWith('flags')) return translateType(type.substr(type.indexOf('?') + 1));
        if (type.indexOf('.') > 0) return cap(type);
        else return 'Global.' + type;
    }
})();
