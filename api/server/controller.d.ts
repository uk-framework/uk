import { Application } from "./application";
export declare const controllerMethods: unique symbol;
export declare function expose(target: any, key: string): void;
export declare class Controller<TApp extends Application<TApp['$connectedUser'], TApp['$connectedPeer']>> {
    protected $app: TApp;
    protected readonly $nsname: string;
    protected get $peer(): TApp["$connectedPeer"];
    protected get $user(): TApp["$connectedUser"];
    constructor($app: TApp, $nsname?: string);
}
