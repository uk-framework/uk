/// <reference types="socket.io" />
/// <reference types="node" />
import * as http from "http";
export declare class Application<TUser = {}, TClient = {}> {
    $connectedPeer: TClient;
    $connectedUser: TUser;
    $socket: SocketIO.Socket;
    static start(opts: Application.Options): Map<string, Application<{}, {}>>;
    private $init;
    protected init(): Promise<void>;
    private static ioServer;
}
export declare namespace Application {
    interface BaseOptions {
    }
    interface PortOptions extends BaseOptions {
        port: number;
    }
    interface HttpServerOptions extends BaseOptions {
        httpServer: http.Server;
    }
    type Options = PortOptions & HttpServerOptions;
}
