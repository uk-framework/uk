"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.controllerMethods = Symbol();
function expose(target, key) {
    const statics = target.constructor;
    let methods = statics[exports.controllerMethods];
    if (!statics.hasOwnProperty(exports.controllerMethods)) {
        methods = statics[exports.controllerMethods] = [...(methods || [])];
    }
    if (methods.indexOf(key) < 0)
        methods.push(key);
}
exports.expose = expose;
class Controller {
    constructor($app, $nsname = undefined) {
        this.$app = $app;
        this.$nsname = $nsname;
    }
    get $peer() { return this.$app.$connectedPeer; }
    get $user() { return this.$app.$connectedUser; }
}
exports.Controller = Controller;
//# sourceMappingURL=controller.js.map