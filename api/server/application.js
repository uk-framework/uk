"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const IOServer = require("socket.io");
const log_1 = require("@uk/log");
const controller_1 = require("./controller");
class Application {
    static start(opts) {
        const appMap = new Map();
        const log = new log_1.Log('UK-API:APP');
        log.info('Starting APP', {
            port: 'port' in opts ? opts.port : 'external'
        });
        this.ioServer = IOServer();
        this.ioServer.listen(opts.httpServer);
        this.ioServer.on('connection', (socket) => __awaiter(this, void 0, void 0, function* () {
            const app = new this();
            yield app.$init(socket, log);
            appMap.set(socket.id, app);
            socket.on('disconnect', () => {
                appMap.delete(socket.id);
            });
        }));
        return appMap;
    }
    $init(socket, log) {
        return __awaiter(this, void 0, void 0, function* () {
            this.$connectedPeer = new Proxy({}, {
                get: (target, propName) => {
                    return (...args) => {
                        log.debug('Call client ' + propName, args);
                        socket.emit('clientcall', {
                            method: propName,
                            args: args
                        });
                    };
                }
            });
            log.info('Connection accepted', {
                from: socket.handshake.address
            });
            this.$socket = socket;
            for (const ctrlName in this) {
                const ctrl = this[ctrlName];
                if (ctrl instanceof controller_1.Controller) {
                    const methods = ctrl.constructor[controller_1.controllerMethods];
                    if (!methods) {
                        log.warn('Controller has no one method.', {
                            ctrl: ctrlName
                        });
                        continue;
                    }
                    for (const method of methods) {
                        const fullmethod = ctrlName + "." + method;
                        socket.on(fullmethod, (args, ack) => __awaiter(this, void 0, void 0, function* () {
                            try {
                                let dt = Date.now();
                                let rv = yield ctrl[method].apply(ctrl, args);
                                ack({ rv: rv });
                                log.debug(`Call '${fullmethod}'`, {
                                    args: args,
                                    time: Date.now() - dt
                                });
                            }
                            catch (err) {
                                log.error(`Call '${fullmethod}' failure`, err);
                                ack({ err: err.message || err });
                            }
                        }));
                    }
                }
            }
            yield this.init();
        });
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.Application = Application;
//# sourceMappingURL=application.js.map