import { Log } from "@uk/log"
import * as socketio from "socket.io-client"

export var socket: SocketIOClient.Socket;
var clientCount = 0;
var reqCount = 0;

export interface ClientOpts {
    socket?: SocketIOClient.Socket
    timeout?: 10000
    url?: string
    onStateChange?: (client: IClient) => void
    onError?: (err: any) => void
}

export class Client {
    static create<TServerApp, TClientOpts extends ClientOpts = ClientOpts>(opts?: TClientOpts): TServerApp & IClient<TClientOpts> {
        opts = opts || {} as TClientOpts;
        opts.timeout = opts.timeout || 10000;
        const log = new Log('UK-API:' + clientCount++)
        if (!opts.socket) {
            if (!socket) {
                socket = socketio.connect(opts.url);
                socket.on('connect', () => {
                    log.info('Connected');
                });
                socket.on('disconnect', () => {
                    log.warn('Disconnected');
                });
            }
            opts.socket = socket;
        }
        const client: TServerApp & IClient<TClientOpts> = new Proxy({
            ...Client.proto,
            $opts: opts,
            $inProgress: 0,
            $log: log
        }, {
                get: function (target, name: string) {
                    if (name in target) return target[name];
                    else return target[name] = createApi(name, client);
                }
            }) as any;
        socket.on('clientcall', (args: any) => {
            const method = this.prototype[args['method']];
            log.debug('Server call', { args, method });
            if (typeof method === 'function') {
                method.apply(client, args.args);
            }
        });
        return client;
    }

    private static proto: IClient = {
        $call: async function (this: IClient, apiName: string, methodName: string, ...args: any[]) {
            const reqidx = reqCount++;
            return new Promise<any>((resolve, reject) => {
                this.$inProgress++;
                if (this.$opts.onStateChange) this.$opts.onStateChange(this);
                const fullName = apiName + '.' + methodName;
                const dt = Date.now();
                this.$opts.socket.emit(fullName, args, (resp: ApiResponse) => {
                    let err: Error;
                    if (!resp) err = new Error(`API call to ${fullName} returned ${resp}`);
                    else if (resp.err) err = new Error(resp.err);
                    this.$inProgress--;
                    if (err) {
                        this.$log.error(`Call ${fullName}`, {
                            req: reqidx,
                            args: args,
                            error: err
                        });
                        if (this.$opts.onError) this.$opts.onError(err);
                        reject(err);
                    } else {
                        this.$log.debug(`Call '${fullName}'`, {
                            req: reqidx,
                            args: args,
                            rv: resp.rv,
                            time: Date.now() - dt
                        });
                        resolve(resp.rv);
                    }
                    if (this.$opts.onStateChange) this.$opts.onStateChange(this);
                });
            });
        }
    }
}

export interface IClient<TOpts = ClientOpts> {
    $call(ctrlName: string, methodName: string, ...args: any[]): Promise<any>
    $inProgress?: number
    $opts?: TOpts
    $log?: Log
}

export class Controller {
}

export interface ApiResponse {
    err?: any
    rv?: any
}

function createApi(apiName: string, client: IClient): any {
    return new Proxy({
    } as any, {
            get: function (target, name: string) {
                const fullName = apiName + '.' + name;
                if (name in target) return target[name];
                else return target[name] = client.$call.bind(client, apiName, name);
            }
        });
}
