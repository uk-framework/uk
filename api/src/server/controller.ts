import { Application } from "./application"

export const controllerMethods = Symbol();

export function expose(target: any, key: string) {
    const statics = target.constructor;
    let methods = statics[controllerMethods] as string[];
    if (!statics.hasOwnProperty(controllerMethods)) {
        methods = statics[controllerMethods] = [...(methods || [])];
    }
    if (methods.indexOf(key) < 0) methods.push(key);
}

export class Controller<TApp extends Application<TApp['$connectedUser'], TApp['$connectedPeer']>> {
    protected get $peer() { return this.$app.$connectedPeer; }
    protected get $user() { return this.$app.$connectedUser; }

    constructor(protected $app: TApp, protected readonly $nsname: string = undefined) {
    }
}
