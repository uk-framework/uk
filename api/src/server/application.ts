import * as IOServer from "socket.io"
import { Log } from "@uk/log"
import { Controller, controllerMethods } from "./controller"
import { ApiResponse } from "../client"
import * as http from "http"

export class Application<TUser = {}, TClient = {}> {
    $connectedPeer: TClient
    $connectedUser: TUser
    $socket: SocketIO.Socket

    static start(opts: Application.Options) {
        const appMap = new Map<string, Application>();
        const log = new Log('UK-API:APP');
        log.info('Starting APP', {
            port: 'port' in opts ? opts.port : 'external'
        });
        this.ioServer = IOServer();
        this.ioServer.listen(opts.httpServer);
        this.ioServer.on('connection', async socket => {
            const app = new this();
            await app.$init(socket, log);
            appMap.set(socket.id, app);
            socket.on('disconnect', ()=> {
                appMap.delete(socket.id);
            });
        });
        return appMap;
    }

    private async $init(socket: SocketIO.Socket, log: Log) {
        this.$connectedPeer = new Proxy({}, {
            get: (target, propName: string)=> {
                return (...args: any[])=> {
                    log.debug('Call client ' + propName, args);
                    socket.emit('clientcall', {
                        method: propName,
                        args: args
                    });
                }
            }
        }) as any;
        log.info('Connection accepted', {
            from: socket.handshake.address
        });
        this.$socket = socket;
        for (const ctrlName in this) {
            const ctrl: any = this[ctrlName];
            if (ctrl instanceof Controller) {
                const methods = ctrl.constructor[controllerMethods] as string[];
                if (!methods) {
                    log.warn('Controller has no one method.', {
                        ctrl: ctrlName
                    });
                    continue;
                }
                for (const method of methods) {
                    const fullmethod = ctrlName + "." + method;
                    socket.on(fullmethod, async (args: any[], ack: (rv: ApiResponse)=>void) => {
                        try {
                            let dt = Date.now();
                            let rv = await ctrl[method].apply(ctrl, args);
                            ack({ rv: rv });
                            log.debug(`Call '${fullmethod}'`, {
                                args: args,
                                time: Date.now() - dt
                            });
                        } catch (err) {
                            log.error(`Call '${fullmethod}' failure`, err);
                            ack({ err: err.message || err });
                        }
                    });
                }
            }
        }
        await this.init();
    }

    protected async init() {
    }

    private static ioServer: SocketIO.Server
}

export namespace Application {
    export interface BaseOptions {
    }
    export interface PortOptions extends BaseOptions {
        port: number
    }
    export interface HttpServerOptions extends BaseOptions {
        httpServer: http.Server
    }
    export type Options = PortOptions & HttpServerOptions;
}
