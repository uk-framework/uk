/// <reference types="socket.io-client" />
import { Log } from "@uk/log";
export declare var socket: SocketIOClient.Socket;
export interface ClientOpts {
    socket?: SocketIOClient.Socket;
    timeout?: 10000;
    url?: string;
    onStateChange?: (client: IClient) => void;
    onError?: (err: any) => void;
}
export declare class Client {
    static create<TServerApp, TClientOpts extends ClientOpts = ClientOpts>(opts?: TClientOpts): TServerApp & IClient<TClientOpts>;
    private static proto;
}
export interface IClient<TOpts = ClientOpts> {
    $call(ctrlName: string, methodName: string, ...args: any[]): Promise<any>;
    $inProgress?: number;
    $opts?: TOpts;
    $log?: Log;
}
export declare class Controller {
}
export interface ApiResponse {
    err?: any;
    rv?: any;
}
