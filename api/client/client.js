"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("@uk/log");
const socketio = require("socket.io-client");
var clientCount = 0;
var reqCount = 0;
class Client {
    static create(opts) {
        opts = opts || {};
        opts.timeout = opts.timeout || 10000;
        const log = new log_1.Log('UK-API:' + clientCount++);
        if (!opts.socket) {
            if (!exports.socket) {
                exports.socket = socketio.connect(opts.url);
                exports.socket.on('connect', () => {
                    log.info('Connected');
                });
                exports.socket.on('disconnect', () => {
                    log.warn('Disconnected');
                });
            }
            opts.socket = exports.socket;
        }
        const client = new Proxy(Object.assign(Object.assign({}, Client.proto), { $opts: opts, $inProgress: 0, $log: log }), {
            get: function (target, name) {
                if (name in target)
                    return target[name];
                else
                    return target[name] = createApi(name, client);
            }
        });
        exports.socket.on('clientcall', (args) => {
            const method = this.prototype[args['method']];
            log.debug('Server call', { args, method });
            if (typeof method === 'function') {
                method.apply(client, args.args);
            }
        });
        return client;
    }
}
exports.Client = Client;
Client.proto = {
    $call: function (apiName, methodName, ...args) {
        return __awaiter(this, void 0, void 0, function* () {
            const reqidx = reqCount++;
            return new Promise((resolve, reject) => {
                this.$inProgress++;
                if (this.$opts.onStateChange)
                    this.$opts.onStateChange(this);
                const fullName = apiName + '.' + methodName;
                const dt = Date.now();
                this.$opts.socket.emit(fullName, args, (resp) => {
                    let err;
                    if (!resp)
                        err = new Error(`API call to ${fullName} returned ${resp}`);
                    else if (resp.err)
                        err = new Error(resp.err);
                    this.$inProgress--;
                    if (err) {
                        this.$log.error(`Call ${fullName}`, {
                            req: reqidx,
                            args: args,
                            error: err
                        });
                        if (this.$opts.onError)
                            this.$opts.onError(err);
                        reject(err);
                    }
                    else {
                        this.$log.debug(`Call '${fullName}'`, {
                            req: reqidx,
                            args: args,
                            rv: resp.rv,
                            time: Date.now() - dt
                        });
                        resolve(resp.rv);
                    }
                    if (this.$opts.onStateChange)
                        this.$opts.onStateChange(this);
                });
            });
        });
    }
};
class Controller {
}
exports.Controller = Controller;
function createApi(apiName, client) {
    return new Proxy({}, {
        get: function (target, name) {
            const fullName = apiName + '.' + name;
            if (name in target)
                return target[name];
            else
                return target[name] = client.$call.bind(client, apiName, name);
        }
    });
}
//# sourceMappingURL=client.js.map