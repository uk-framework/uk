"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("@uk/db");
const mongo = require("mongodb");
const log_1 = require("@uk/log");
let drvCount = 0;
class MongoDriver extends db_1.Driver {
    constructor(connString, connOpts) {
        super();
        this.connString = connString;
        this.connOpts = connOpts;
        this.log = new log_1.Log('UK-MONGO:' + drvCount++);
        this.connect();
    }
    static pool(connString, opts) {
        const drv = new MongoDriver(connString, opts);
        const rv = function () {
            return Promise.resolve(drv);
        };
        rv['ObjectID'] = mongo.ObjectID;
        return rv;
    }
    insert(model, docs) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.db)
                yield this.connect();
            this.log.info('Inserting to', { name: model.$.name, docs: docs });
            return new Promise((resolve, reject) => {
                this.db.collection(model.$.name).insertMany(docs, (err, rv) => {
                    if (err) {
                        this.log.error(err);
                        reject(err);
                    }
                    else {
                        this.log.info('Inserted', { docId: rv.insertedIds });
                        resolve({
                            pk: rv.insertedIds
                        });
                    }
                });
            });
        });
    }
    update(model, where, doc) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.db)
                yield this.connect();
            return new Promise((resolve, reject) => {
                this.db.collection(model.$.name).update(this.convertWhere(where), { $set: doc }, (err, rv) => {
                    if (err) {
                        this.log.error(err);
                        reject(err);
                    }
                    else {
                        this.log.info('Updated', { doc: doc });
                        resolve(doc['_id']);
                    }
                });
            });
        });
    }
    remove(model, doc) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.db)
                yield this.connect();
            return new Promise((resolve, reject) => {
                this.db.collection(model.$.name).remove(doc, (err, rv) => {
                    if (err) {
                        this.log.error(err);
                        reject(err);
                    }
                    else {
                        this.log.info('Removed', { _id: doc['_id'] });
                        resolve(doc['_id']);
                    }
                });
            });
        });
    }
    count(query) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.db)
                yield this.connect();
            const where = this.convertWhere(query.where());
            const rv = yield this.db.collection(query.model.$.name).countDocuments(where);
            this.log.info('Count', {
                where,
                rv
            });
            return rv;
        });
    }
    exists(query) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.db)
                yield this.connect();
            const where = this.convertWhere(query.where());
            const rv = (yield this.db.collection(query.model.$.name).count(where, { limit: 1 })) > 0;
            this.log.info('Exists', {
                where,
                rv
            });
            return rv;
        });
    }
    select(query, stream) {
        return __awaiter(this, void 0, void 0, function* () {
            const where = this.convertWhere(query.where());
            let rv;
            try {
                if (!this.db)
                    yield this.connect();
                const joins = query['joins'];
                if (joins.length > 0) {
                    const pipeline = joins.reduce((rv, fk) => {
                        rv.push({
                            $match: where
                        });
                        if (fk instanceof db_1.field.One.OneField) {
                            this.joinOnePipeline(fk, rv);
                        }
                        else {
                            this.joinManyPipeline(fk, rv);
                        }
                        return rv;
                    }, []);
                    rv = yield this.db.collection(query.model.$.name).aggregate(pipeline).toArray();
                }
                else {
                    const cursor = this.db.collection(query.model.$.name).find(where);
                    rv = (yield cursor.toArray()) || [];
                }
            }
            finally {
                this.log.info(`Select ${query.model.$.name}`, { where, join: query['joins'].map((j) => j.name), count: rv ? rv.length : 0 });
            }
            this.convertResultArray(rv);
            return {
                total: rv.length,
                rows: rv
            };
        });
    }
    release() {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    convertResultArray(r) {
        for (let i = 0; i < r.length; i++) {
            r[i] = this.convertResult(r[i]);
        }
    }
    convertResult(r) {
        const type = typeof r;
        if (type === 'string' || type === 'number' || type === 'boolean')
            return r;
        if (r instanceof Date)
            return r;
        if (Array.isArray(r)) {
            this.convertResultArray(r);
        }
        else if (mongo.ObjectId.isValid(r)) {
            return r.toString();
        }
        else if (type === 'object') {
            for (const n in r) {
                r[n] = this.convertResult(r[n]);
            }
        }
        return r;
    }
    joinOnePipeline(fk, rv) {
        rv.push({
            $lookup: {
                from: fk.target.$.name,
                localField: fk.name + '.' + fk.target.$.pk.name,
                foreignField: fk.target.$.pk.name,
                as: fk.name
            },
        });
        rv.push({
            $unwind: {
                path: '$' + fk.name,
                preserveNullAndEmptyArrays: true,
            }
        });
    }
    joinManyPipeline(fk, rv) {
        rv.push({
            $lookup: {
                from: fk.target.$.name,
                localField: fk.name + '.' + fk.target.$.pk.name,
                foreignField: fk.target.$.pk.name,
                as: fk.name
            },
        });
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.connectPromise)
                return this.connectPromise;
            if (this.client)
                this.disconnect();
            this.log.info('Connecting');
            return this.connectPromise = new Promise((resolve, reject) => {
                mongo.MongoClient.connect(this.connString, Object.assign({ useNewUrlParser: true }, this.connOpts), (err, client) => {
                    if (err) {
                        this.log.error(err);
                        reject(err);
                    }
                    else {
                        this.log.info('Connected');
                        this.client = client;
                        this.db = client.db();
                        resolve(this);
                    }
                    this.connectPromise = null;
                });
            });
        });
    }
    disconnect(force = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client) {
                const client = this.client;
                this.db = null;
                this.client = null;
                this.log.info('Disconnecting');
                return new Promise((resolve, reject) => {
                    client.close(force, (err) => {
                        if (err) {
                            this.log.error(err);
                            reject(err);
                        }
                        else {
                            this.log.info('Disconnected');
                            resolve();
                        }
                    });
                });
            }
        });
    }
    convertObject(fieldValue, fieldName, path, rv) {
        if (fieldName === '_id' && typeof fieldValue === 'string' && fieldValue.length === 24) {
            const id = mongo.ObjectId.createFromHexString(fieldValue);
            if (mongo.ObjectID.isValid(id)) {
                return id;
            }
            else {
                return fieldValue;
            }
        }
        else if (mongo.ObjectID.isValid(fieldValue)) {
            return fieldValue;
        }
        else if (fieldValue instanceof Date) {
            return fieldValue;
        }
        else if (Array.isArray(fieldValue)) {
            return fieldValue.map(o => {
                const r = {};
                this.convertObject(o, "", "", r);
                return r;
            });
        }
        else if (typeof fieldValue === 'object') {
            this.convertWhere(fieldValue, path ? path + "." : "", rv);
        }
        else {
            return fieldValue;
        }
    }
    convertWhere(doc, parentName = "", rv = {}) {
        for (const fieldName in doc) {
            if (fieldName.startsWith('$')) {
                const path = parentName.replace(/\.$/, '');
                const obj = this.convertObject(doc[fieldName], fieldName, path, rv);
                if (path) {
                    rv[path] = rv[path] || {};
                    rv[path][fieldName] = obj;
                }
                else {
                    rv[fieldName] = obj;
                }
            }
            else {
                const path = parentName + fieldName;
                const obj = this.convertObject(doc[fieldName], fieldName, path, rv);
                if (obj !== undefined)
                    rv[path] = obj;
            }
        }
        return rv;
    }
}
exports.MongoDriver = MongoDriver;
//# sourceMappingURL=mongodriver.js.map