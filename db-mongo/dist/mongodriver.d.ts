import { Driver, Model, DriverFactory, Query, Schema, Row, WhereCondition } from "@uk/db";
import * as mongo from "mongodb";
export declare class MongoDriver<TSchema extends Schema<TSchema>> extends Driver {
    private connString;
    private connOpts;
    private db;
    private client;
    private connectPromise;
    private log;
    static pool(connString: string, opts?: mongo.MongoClientOptions): DriverFactory;
    constructor(connString: string, connOpts: mongo.MongoClientOptions);
    insert(model: Model.Any, docs: Row<TSchema>[]): Promise<unknown>;
    update(model: Model<TSchema>, where: WhereCondition<TSchema>, doc: Row<TSchema>): Promise<unknown>;
    remove(model: Model<TSchema>, doc: Row<TSchema>): Promise<unknown>;
    count(query: Query<TSchema>): Promise<number>;
    exists(query: Query<TSchema>): Promise<boolean>;
    select(query: Query<TSchema>, stream?: Driver.SelectStreamHandler<TSchema>): Promise<{
        total: number;
        rows: Model.Row<TSchema>[];
    }>;
    release(): Promise<void>;
    private convertResultArray;
    private convertResult;
    private joinOnePipeline;
    private joinManyPipeline;
    private connect;
    private disconnect;
    private convertObject;
    private convertWhere;
}
