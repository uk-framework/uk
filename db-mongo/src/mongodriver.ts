import { Driver, Model, DriverFactory, Query, Schema, Row, WhereCondition, field, Field } from "@uk/db"
import * as mongo from "mongodb"
import { Log } from "@uk/log"

let drvCount = 0;

export class MongoDriver<TSchema extends Schema<TSchema>> extends Driver {
    private db: mongo.Db;
    private client: mongo.MongoClient;
    private connectPromise: Promise<any>
    private log = new Log('UK-MONGO:' + drvCount++);

    static pool(connString: string, opts?: mongo.MongoClientOptions): DriverFactory {
        const drv = new MongoDriver(connString, opts);
        const rv = function () {
            return Promise.resolve(drv);
        };
        rv['ObjectID'] = mongo.ObjectID;
        return rv;
    }

    constructor(private connString: string, private connOpts: mongo.MongoClientOptions) {
        super();
        this.connect();
    }

    async insert(model: Model.Any, docs: Row<TSchema>[]) {
        if (!this.db) await this.connect();
        //        for (let i = 0; i < docs.length; i++) {
        //            docs[i] = this.convertDocument(docs[i]);
        //        }
        this.log.info('Inserting to', { name: model.$.name, docs: docs });
        return new Promise((resolve, reject) => {
            this.db.collection(model.$.name).insertMany(docs, (err, rv) => {
                if (err) {
                    this.log.error(err);
                    reject(err);
                } else {
                    this.log.info('Inserted', { docId: rv.insertedIds });
                    resolve({
                        pk: rv.insertedIds
                    });
                }
            });
        });
    }

    async update(model: Model<TSchema>, where: WhereCondition<TSchema>, doc: Row<TSchema>) {
        if (!this.db) await this.connect();
        return new Promise((resolve, reject) => {
            //            doc = this.convertDocument(doc);
            this.db.collection(model.$.name).update(this.convertWhere(where), { $set: doc }, (err, rv) => {
                if (err) {
                    this.log.error(err);
                    reject(err);
                } else {
                    this.log.info('Updated', { doc: doc });
                    resolve(doc['_id']);
                }
            })
        })
    }

    async remove(model: Model<TSchema>, doc: Row<TSchema>) {
        // doc = this.convertDocument(doc);
        if (!this.db) await this.connect();
        return new Promise((resolve, reject) => {
            this.db.collection(model.$.name).remove(doc, (err, rv) => {
                if (err) {
                    this.log.error(err);
                    reject(err);
                } else {
                    this.log.info('Removed', { _id: doc['_id'] });
                    resolve(doc['_id'])
                }
            })
        })
    }

    async count(query: Query<TSchema>): Promise<number> {
        if (!this.db) await this.connect();
        const where = this.convertWhere(query.where());
        const rv = await this.db.collection(query.model.$.name).countDocuments(where);
        this.log.info('Count', {
            where,
            rv
        });
        return rv;
    }

    async exists(query: Query<TSchema>): Promise<boolean> {
        if (!this.db) await this.connect();
        const where = this.convertWhere(query.where());
        const rv = await this.db.collection(query.model.$.name).count(where, { limit: 1 }) > 0;
        this.log.info('Exists', {
            where,
            rv
        });
        return rv;
    }

    async select(query: Query<TSchema>, stream?: Driver.SelectStreamHandler<TSchema>) {
        const where = this.convertWhere(query.where());
        let rv: Row<TSchema>[];
        try {
            if (!this.db) await this.connect();
            const joins = query['joins'] as field.One<any>[];
            if (joins.length > 0) {
                const pipeline = joins.reduce((rv, fk) => {
                    rv.push({
                        $match: where
                    });
                    if (fk instanceof field.One.OneField) {
                        this.joinOnePipeline(fk, rv);
                    } else {
                        this.joinManyPipeline(fk, rv);
                    }
                    return rv;
                }, [] as any[]);

                rv = await this.db.collection(query.model.$.name).aggregate(pipeline).toArray();
            } else {
                const cursor = this.db.collection(query.model.$.name).find(where);
                rv = await cursor.toArray() || [];
            }
        } finally {
            this.log.info(`Select ${query.model.$.name}`, { where, join: query['joins'].map((j: field.One<any>) => j.name), count: rv ? rv.length : 0 });
        }
        this.convertResultArray(rv);
        return {
            total: rv.length,
            rows: rv
        }
    }

    async release() {
    }

    private convertResultArray(r: any[]) {
        for (let i = 0; i < r.length; i++) {
            r[i] = this.convertResult(r[i]);
        }
    }

    private convertResult(r: any) {
        const type = typeof r;
        if (type === 'string' || type === 'number' || type === 'boolean') return r;
        if (r instanceof Date) return r;
        if (Array.isArray(r)) {
            this.convertResultArray(r);
        } else if (mongo.ObjectId.isValid(r)) {
            return r.toString();
        } else if (type === 'object') {
            for (const n in r) {
                r[n] = this.convertResult(r[n]);
            }
        }
        return r;
    }

    private joinOnePipeline(fk: field.One<any>, rv: any[]) {
        rv.push({
            $lookup: {
                from: fk.target.$.name,
                localField: fk.name + '.' + fk.target.$.pk.name,
                foreignField: fk.target.$.pk.name,
                as: fk.name
            },
        });
        rv.push({
            $unwind: {
                path: '$' + fk.name,
                preserveNullAndEmptyArrays: true,
            }
        });
    }

    private joinManyPipeline(fk: field.One<any>, rv: any[]) {
        rv.push({
            $lookup: {
                from: fk.target.$.name,
                localField: fk.name + '.' + fk.target.$.pk.name,
                foreignField: fk.target.$.pk.name,
                as: fk.name
            },
        });
    }

    private async connect() {
        if (this.connectPromise) return this.connectPromise
        if (this.client) this.disconnect();
        this.log.info('Connecting');
        return this.connectPromise = new Promise((resolve, reject) => {
            mongo.MongoClient.connect(this.connString, { useNewUrlParser: true, ...this.connOpts }, (err, client) => {
                if (err) {
                    this.log.error(err);
                    reject(err);
                } else {
                    this.log.info('Connected');
                    this.client = client;
                    this.db = client.db();
                    resolve(this);
                }
                this.connectPromise = null;
            })
        });
    }

    private async disconnect(force = false) {
        if (this.client) {
            const client = this.client;
            this.db = null;
            this.client = null;
            this.log.info('Disconnecting');
            return new Promise((resolve, reject) => {
                client.close(force, (err) => {
                    if (err) {
                        this.log.error(err);
                        reject(err);
                    } else {
                        this.log.info('Disconnected');
                        resolve();
                    }
                });
            });
        }
    }
    /*
        private convertDocument(doc: any) {
            let rv = undefined as any;
            if (doc instanceof Array) {
                rv = doc.map((item: any) => {
                    return this.convertDocument(item);
                })
            } else if (doc instanceof Date) {
                rv = doc;
            } else if (typeof doc === 'object') {
                rv = {} as any;
                for (const fieldName in doc) {
                    rv[fieldName] = doc[fieldName];
                    const fieldValue = rv[fieldName];
                    if (fieldName === '_id' && typeof fieldValue === 'string' && fieldValue.length === 24) {
                        const id = mongo.ObjectId.createFromHexString(fieldValue);
                        if (mongo.ObjectID.isValid(id)) {
                            rv[fieldName] = id as any;
                        }
                    } else if (typeof fieldValue === 'object' && !mongo.ObjectID.isValid(fieldValue as any)) {
                        rv[fieldName] = this.convertDocument(fieldValue) as any;
                    }
                }
            } else {
                rv = doc;
            }
            return rv;
        }
    */
    private convertObject(fieldValue: any, fieldName: string, path: string, rv: any): any {
        if (fieldName === '_id' && typeof fieldValue === 'string' && fieldValue.length === 24) {
            const id = mongo.ObjectId.createFromHexString(fieldValue);
            if (mongo.ObjectID.isValid(id)) {
                return id as any;
            } else {
                return fieldValue;
            }
        } else if (mongo.ObjectID.isValid(fieldValue)) {
            return fieldValue;
        } else if (fieldValue instanceof Date) {
            return fieldValue;
        } else if (Array.isArray(fieldValue)) {
            return fieldValue.map(o => {
                //                if (typeof o !== 'object') return o;
                const r = {};
                this.convertObject(o, "", "", r);
                return r;
            });
        } else if (typeof fieldValue === 'object') {
            this.convertWhere(fieldValue, path ? path + "." : "", rv) as any;
        } else {
            return fieldValue;
        }
    }

    private convertWhere<T>(doc: T, parentName = "", rv = {} as any): object {
        for (const fieldName in doc) {
            if (fieldName.startsWith('$')) {
                const path = parentName.replace(/\.$/, '');
                const obj = this.convertObject(doc[fieldName], fieldName, path, rv);
                if (path) {
                    rv[path] = rv[path] || {};
                    rv[path][fieldName] = obj;
                } else {
                    rv[fieldName] = obj;
                }
            } else {
                const path = parentName + fieldName;
                const obj = this.convertObject(doc[fieldName], fieldName, path, rv);
                if (obj !== undefined) rv[path] = obj;
            }
        }
        return rv;
    }
}
