import { Model, Schema, Query, Row, WhereCondition } from ".";
export declare abstract class Driver<TSchema extends Schema<TSchema> = any> {
    abstract insert(model: Model<TSchema>, doc: Row<TSchema>[]): Promise<Driver.InsertResult>;
    abstract update(model: Model<TSchema>, where: WhereCondition<TSchema>, doc: Partial<Row<TSchema>>): Promise<Driver.UpdateResult>;
    abstract remove(model: Model<TSchema>, where: WhereCondition<TSchema>): Promise<Driver.RemoveResult>;
    abstract select(query: Query<TSchema>, stream?: Driver.SelectStreamHandler<TSchema>): Promise<Driver.SelectResult<Row<TSchema>>>;
    abstract count(query: Query<TSchema>): Promise<number>;
    abstract exists(query: Query<TSchema>): Promise<boolean>;
    abstract release(): void;
}
export declare module Driver {
    interface InsertResult {
        autoincrements?: {
            [field: string]: number;
        }[];
        pk?: any[];
    }
    interface UpdateResult {
    }
    interface RemoveResult {
    }
    interface SelectResult<TSchema> {
        total: number;
        rows?: Row<TSchema>[];
    }
    type SelectStreamHandler<TSchema extends Schema<TSchema>> = (row: Row<TSchema>, index: number, total: number) => boolean;
}
