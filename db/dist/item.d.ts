import { Schema, Query, Field } from ".";
export declare class Item<TSchema extends Schema<TSchema>> {
    readonly query: Query<TSchema>;
    constructor(query: Query<TSchema>);
    get(field: Field.Any): any;
    set(field: Field.Any, value: any): this;
    save(): Promise<void>;
    subscribe(func: Item.ChangeHandler<TSchema>): void;
    unsubscribe(func: Item.ChangeHandler<TSchema>): void;
    protected triggerChange(): void;
    private onChange?;
    private row;
}
export declare namespace Item {
    type Any = Item<any>;
    type ChangeHandler<TSchema extends Schema<TSchema>> = (item: Item<TSchema>) => void;
}
