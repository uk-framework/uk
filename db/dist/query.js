"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
class Query {
    constructor(model, fields) {
        this.model = model;
        this.fields = fields;
        this._offset = 0;
        this._limit = 0;
        this.condition = {};
        this.joins = [];
    }
    join(fk) {
        this.joins.push(fk.$);
        return this;
    }
    where(condition) {
        if (condition === undefined)
            return this.condition;
        for (const f of this.model.$.fields.values()) {
            if (f instanceof _1.field.One.OneField) {
                const value = f.value(condition);
                if (value) {
                    if (value['_id']) {
                        f.value(condition, { _id: value['_id'] });
                    }
                    else if (value['$in']) {
                        f.value(condition, value['$in'].map((r) => {
                            return { _id: r['_id'] };
                        }));
                    }
                }
            }
        }
        this.condition = condition;
        if (this.dataSet)
            this.dataSet.reload();
        return this;
    }
    clause() {
    }
    dataset() {
        if (!this.dataSet) {
            this.dataSet = new _1.DataSet(this);
            this.dataSet.reload();
        }
        return this.dataSet;
    }
    limit(limit, offset) {
        if (limit > 0)
            this._limit = limit;
        if (offset > 0)
            this._offset = offset;
        return this;
    }
    offset(value) {
        if (value > 0)
            this._offset = value;
        return this;
    }
    async notExists() {
        return !(await this.exists());
    }
    async exists() {
        return this.driver(async (drv) => {
            return drv.exists(this);
        });
    }
    async count() {
        return this.driver(async (drv) => {
            return drv.count(this);
        });
    }
    async one(id) {
        this.limit(1, 0);
        return this.driver(async (drv) => {
            const rv = await drv.select(this);
            return rv ? rv.rows[0] : null;
        });
    }
    async array() {
        return this.driver(async (drv) => {
            return await drv.select(this);
        });
    }
    async stream(onRow) {
        return this.driver(async (drv) => {
            const rv = await drv.select(this, onRow);
            return rv.total;
        });
    }
    get db() {
        return this.model.$.db;
    }
    driver(cb) {
        return this.db.driver(cb);
    }
    static serialize(query) {
        return {
            condition: query.condition,
            fields: query.fields,
            limit: query._limit,
            offset: query._offset,
            joins: query.joins.map(j => j.name)
        };
    }
    static deserialize(query, model) {
        const rv = new Query(model, query.fields).where(query.condition);
        if (query.joins && query.joins.length > 0) {
            query.joins.forEach(fldName => rv.join({ $: rv.model.$.fields.get(fldName) }));
        }
        rv._limit = query.limit;
        rv._offset = query.offset;
        return rv;
    }
}
exports.Query = Query;
//# sourceMappingURL=query.js.map