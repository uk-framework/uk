import { Schema as DbSchema, Model, Row } from ".";
export declare function Validator<TSchema extends DbSchema<TSchema>>(factory: Validator.Factory<TSchema>): Validator.Factory<TSchema>;
export declare namespace Validator {
    type Factory<TSchema extends DbSchema<TSchema>> = () => Validator.Schema<TSchema>;
    type Schema<TSchema extends DbSchema<TSchema>, TRow = Row<TSchema>> = {
        [P in keyof TRow]?: TRow[P] extends string ? CheckFunction<Checker.String<TSchema>> : TRow[P] extends Date ? CheckFunction<Checker.DateTime<TSchema>> : TRow[P] extends number ? CheckFunction<Checker.Number<TSchema>> : TRow[P] extends object ? Schema<TSchema, TRow[P]> : CheckFunction<Checker<TRow[P], TSchema>>;
    };
    class CheckResult {
        readonly text: string;
        constructor(text: string);
    }
    type Result = (CheckResult | undefined | null)[];
    type CheckFunction<TChecker extends Checker<any, any>> = (chk: TChecker) => Result | Promise<Result>;
    class Checker<TVal, TSchema extends DbSchema<TSchema>> {
        readonly value: TVal;
        readonly row: Row<TSchema>;
        readonly model: Model<TSchema>;
    }
    namespace Checker {
        class String<TSchema extends DbSchema<TSchema>> extends Checker<string, TSchema> {
            regex(regex: RegExp, message?: string): CheckResult;
            size(min: number, max: number, message?: string): CheckResult;
        }
        class Number<TSchema extends DbSchema<TSchema>> extends Checker<number, TSchema> {
            between(min: number, max: number, message?: string): CheckResult;
        }
        class DateTime<TSchema extends DbSchema<TSchema>> extends Checker<Date, TSchema> {
            between(min: Date, max: Date, message?: string): CheckResult;
        }
    }
}
