"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
const tool_1 = require("@uk/tool");
class Model {
    constructor(db, schemaCtor) {
        this.$$ = {
            name: "",
            fields: new Map(),
            fieldsArray: [],
            db: undefined,
            pk: undefined,
            schema: undefined,
            validate: new tool_1.ActionEvent(),
            pre: {
                select: new tool_1.ActionEvent(),
                insert: new tool_1.ActionEvent(),
                update: new tool_1.ActionEvent(),
                remove: new tool_1.ActionEvent(),
            },
            post: {
                select: new tool_1.ActionEvent(),
                insert: new tool_1.ActionEvent(),
                update: new tool_1.ActionEvent(),
                remove: new tool_1.ActionEvent(),
            },
            inspect: () => {
                let rv = `Inspecting model ${this.$.name}\n`;
                for (const fld of this.$.fields.values()) {
                    rv += fld.toString('\t') + '\n';
                }
                return rv;
            }
        };
        this.$ = this.$$;
        this.$$.db = db;
        this.$$.name = schemaCtor.name;
        this.$$.schema = schemaCtor;
    }
    create(from) {
        from = from || {};
        const rv = {};
        this.$.fields.forEach(fld => {
            rv[fld.name] = fld.convert(from[fld.name], { setDefault: true, truncate: false });
        });
        return rv;
    }
    async save(row, opts) {
        const pk = this.$.pk;
        const id = pk.value(row);
        if (id !== undefined)
            return this.update(row);
        else
            return this.insert(row);
    }
    async insert(rows, opts) {
        let isarray = true;
        if (!Array.isArray(rows)) {
            rows = [rows];
            isarray = false;
        }
        const toInsertArr = [];
        for (const row of rows) {
            const toInsert = {};
            this.$.fields.forEach(fld => {
                toInsert[fld.name] = fld.convert(row[fld.name], { setDefault: true, truncate: true });
            });
            await this.validate(toInsert);
            await this.$.pre.insert.fire({
                row: toInsert,
                opts: opts,
            });
            toInsertArr.push(toInsert);
        }
        const r = rows;
        return this.__driver(async (drv) => {
            const rv = await drv.insert(this, toInsertArr);
            const pkname = this.$.pk.name;
            for (let i = 0; i < r.length; i++) {
                if (r[i][pkname] === undefined || r[i][pkname] === null)
                    r[i][pkname] = rv.pk[i];
                this.$.post.insert.fire({
                    row: r[i],
                    opts: opts,
                });
            }
            return isarray ? r : r[0];
        });
    }
    async update(row, update, opts) {
        const src = update || row;
        const toUpdate = {};
        this.$.fields.forEach(fld => {
            if (fld.name in src) {
                toUpdate[fld.name] = fld.convert(src[fld.name], { setDefault: false, truncate: true });
            }
        });
        await this.validate(toUpdate, "partial");
        const change = {
            patch: toUpdate,
            row: row,
            opts: opts,
        };
        await this.$.pre.update.fire(change);
        return this.__driver(async (drv) => {
            const pk = this.$.pk;
            const id = pk.value(row);
            Object.assign(row, update);
            const rv = await drv.update(this, { [pk.name]: id }, toUpdate);
            this.$.post.update.fire(change);
            return row;
        });
    }
    async remove(rowOrId, opts) {
        const row = typeof rowOrId === 'object' ? rowOrId : { [this.$.pk.name]: rowOrId };
        this.$.fields.forEach(fld => {
            if (fld.name in row) {
                row[fld.name] = fld.convert(row[fld.name], { setDefault: false, truncate: true });
            }
        });
        if (this.$.pre.remove.hasHandlers) {
            await this.$.pre.remove.fire({
                row: row,
                opts: opts
            });
        }
        return this.__driver(async (drv) => {
            const rv = await drv.remove(this, row);
            if (this.$.post.remove.hasHandlers) {
                this.$.post.remove.fire({
                    row: row,
                    opts: opts
                });
            }
            return rv;
        });
    }
    select(...fields) {
        return new _1.Query(this, fields);
    }
    async findOne(_id) {
        if (!_id)
            throw new Error(`${this.$.name}.findOne requires _id argument, got "${_id}"`);
        return this.select().where({ [this.$.pk.name]: _id }).one();
    }
    async validate(row, partial) {
        this.$$.validate.fire(null);
    }
    async validateField() {
    }
    async validateRecursive(row, partial, fields, validator) {
    }
    __driver(cb) {
        return this.$.db.driver(cb);
    }
    __initialize() {
        const schema = new this.$.schema();
        for (const fieldName of Object.getOwnPropertyNames(schema)) {
            const field = schema[fieldName];
            if (field instanceof _1.Field) {
                field['init'](fieldName, this);
                this[fieldName] = { $: field };
                field.fillSchema(this[fieldName]);
                this.$.fields.set(field.name, field);
                this.$.fieldsArray.push(field);
                if (field.options.primary) {
                    this.$$.pk = field;
                }
            }
        }
        if (!this.$.pk)
            throw new Error(`Model ${this.$.name} has no primary key.`);
    }
}
exports.Model = Model;
//# sourceMappingURL=model.js.map