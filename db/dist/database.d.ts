import { Schema, Model, DriverFactory, Driver } from ".";
export declare type ModelConst<TSchema extends Schema<TSchema>, TStatic> = {
    [P in keyof TSchema]: (TSchema[P]['subtype'] & {
        readonly $: TSchema[P];
    });
} & Model.IModel<TSchema> & {
    [P in keyof TStatic]: TStatic[P];
};
export declare class ArrayMap<TKey, TVal> extends Map<TKey, TVal> {
    find(cb: (val: TVal, key: TKey) => boolean): TVal;
}
export declare class Database {
    static readonly default: Database;
    readonly models: ArrayMap<string, Model.Any>;
    register<TSchema extends Schema<TSchema>, TStatic>(SchemaCtor: {
        new (): TSchema;
    }, statics?: TStatic, modelName?: string): ModelConst<TSchema, TStatic>;
    init(factory: DriverFactory): Promise<void>;
    migrate(): Promise<void>;
    driver<TSchema extends Schema<TSchema>, TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult>;
    readonly drvFactory: DriverFactory;
}
