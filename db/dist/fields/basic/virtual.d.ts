import { Field } from "../field";
import { Schema, Model } from "../..";
export declare function Virtual<ThisSchema extends Schema<ThisSchema>, T>(getter: (this: Model.Row<ThisSchema>) => T, setter?: (val: T) => void): Virtual.VirtualField<ThisSchema, T>;
export declare namespace Virtual {
    class VirtualField<ThisSchema extends Schema<ThisSchema>, T> extends Field<T> {
        protected getter: (this: Model.Row<ThisSchema>) => T;
        protected setter?: (val: T) => void;
        constructor(getter: (this: Model.Row<ThisSchema>) => T, setter?: (val: T) => void);
        convert(value: any, opts: Field.ConvertOpts): any;
        value(row: Field.AnyRow): T;
        value(row: Field.AnyRow, val: T): this;
    }
}
export declare type Virtual<ThisSchema extends Schema<ThisSchema>, T> = Virtual.VirtualField<ThisSchema, T>;
