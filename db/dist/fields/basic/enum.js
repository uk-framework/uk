"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Enum(enumeration, opts) {
    return new Enum.EnumField(enumeration, opts);
}
exports.Enum = Enum;
(function (Enum) {
    let Dummy;
    (function (Dummy) {
    })(Dummy = Enum.Dummy || (Enum.Dummy = {}));
    class EnumField extends field_1.Field {
        constructor(enumeration, opts) {
            super(opts);
            this.enumeration = enumeration;
        }
    }
    Enum.EnumField = EnumField;
})(Enum = exports.Enum || (exports.Enum = {}));
//# sourceMappingURL=enum.js.map