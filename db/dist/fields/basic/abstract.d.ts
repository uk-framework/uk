import { Field } from "../field";
export declare function Abstract<T>(opts?: Abstract.Options<T>): Abstract.AbstractField<T>;
export declare namespace Abstract {
    class AbstractField<T> extends Field<T, Options<T>> {
        convert(value: any, opts: Field.ConvertOpts): any;
    }
    interface Options<T> extends Field.Options<T> {
    }
}
export declare type Abstract<T> = Abstract.AbstractField<T>;
