import { Field } from "../field";
export declare function Number(opts?: Number.Options): Number.NumberField;
export declare namespace Number {
    class NumberField extends Field<number, Number.Options> {
        convert(value: any, opts: Field.ConvertOpts): number;
    }
    interface Options extends Field.Options<number> {
        size?: 8 | 16 | 32 | 64;
    }
}
export declare type Number = Number.NumberField;
