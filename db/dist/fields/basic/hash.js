"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Hash(protoField, opts) {
    return new Hash.HashField(protoField, opts);
}
exports.Hash = Hash;
(function (Hash) {
    class HashField extends field_1.Field {
        constructor(protoField, opts) {
            super(opts);
            this.protoField = protoField;
        }
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            const rv = {};
            for (const key in value) {
                rv[key] = this.protoField.convert(value[key], opts);
            }
            return rv;
        }
    }
    Hash.HashField = HashField;
})(Hash = exports.Hash || (exports.Hash = {}));
//# sourceMappingURL=hash.js.map