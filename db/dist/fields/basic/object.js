"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
const tool_1 = require("@uk/tool");
function Obj(fields, opts) {
    return new Obj.ObjectField(fields, opts);
}
exports.Obj = Obj;
(function (Obj) {
    class ObjectField extends field_1.Field {
        constructor(fields, opts) {
            super(opts);
            this.fields = fields;
            this.fieldsArray = [];
            for (const n in this.fields) {
                const fld = this.fields[n];
                this.fieldsArray.push(fld);
            }
        }
        init(name, model) {
            super.init(name, model);
            for (const n in this.fields) {
                const fld = this.fields[n];
                fld.init(n, model);
                tool_1.noReadOnly(fld).parent = this;
            }
        }
        fillSchema(schema) {
            for (const n in this.fields) {
                const field = this.fields[n];
                field.fillSchema(schema[n] = { $: field });
            }
        }
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            const rv = {};
            for (const fld of this.fieldsArray) {
                rv[fld.name] = fld.convert(value[fld.name], opts);
            }
            return rv;
        }
        toString(tab) {
            let txt = super.toString(tab) + ":";
            tab += '\t';
            for (const name in this.fields) {
                txt += '\n' + this.fields[name].toString(tab);
            }
            return txt;
        }
    }
    Obj.ObjectField = ObjectField;
})(Obj = exports.Obj || (exports.Obj = {}));
//# sourceMappingURL=object.js.map