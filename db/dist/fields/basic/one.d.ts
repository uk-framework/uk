import { Field } from "../field";
import { Schema, Model, Ctor } from "../..";
export declare function One<ToSchema extends Schema<ToSchema>>(schemaCtor: Ctor<ToSchema>, opts?: One.Options): One.OneField<ToSchema>;
export declare namespace One {
    interface Options extends Field.Options<any> {
    }
    class OneField<ToSchema extends Schema<ToSchema>> extends Field<Model.Row<ToSchema>> {
        protected schema: Ctor<ToSchema>;
        readonly target: Model.Any;
        readonly subtype: {
            [P in keyof ToSchema]: (ToSchema[P]['subtype'] & {
                readonly $: ToSchema[P];
            });
        };
        constructor(schema: Ctor<ToSchema>, opts?: Options);
        toString(tab?: string): string;
        init(name: string, model: Model.Any): void;
        convert(value: any, opts: Field.ConvertOpts): any;
    }
}
export declare type One<ToSchema extends Schema<ToSchema>> = One.OneField<ToSchema>;
