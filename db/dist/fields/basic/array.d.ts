import { Field } from "../field";
import { Model } from "../..";
export declare function Array<TValue, TField extends Field<TValue>>(protoField: TField, opts?: Array.Options): Array.ArrayField<unknown, TField>;
export declare namespace Array {
    class ArrayField<TValue, TField extends Field<TValue>> extends Field<TField['defaultValue'][]> {
        readonly protoField: TField;
        readonly subtype: TField['subtype'];
        constructor(protoField: TField, opts?: Options);
        init(name: string, model: Model.Any): void;
        convert(value: any, opts: Field.ConvertOpts): TField["defaultValue"][];
    }
    interface Options extends Field.Options<any[]> {
    }
}
export declare type Array<TValue, TField extends Field<TValue>> = Array.ArrayField<TValue, TField>;
