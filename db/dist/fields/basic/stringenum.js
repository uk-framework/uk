"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function StringEnum(...args) {
    return new StringEnum.StringEnumField(args);
}
exports.StringEnum = StringEnum;
(function (StringEnum) {
    class StringEnumField extends field_1.Field {
        constructor(values, opts) {
            super();
            this.values = values;
        }
    }
    StringEnum.StringEnumField = StringEnumField;
})(StringEnum = exports.StringEnum || (exports.StringEnum = {}));
//# sourceMappingURL=stringenum.js.map