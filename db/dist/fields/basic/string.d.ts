import { Field } from "../field";
export declare function String<T = string>(opts?: String.Options): String.StringField<T>;
export declare namespace String {
    class StringField<T = string> extends Field<T, Options> {
        convert(value: any, opts: Field.ConvertOpts): any;
    }
    interface Options extends Field.Options<string> {
        min?: number;
        max?: number;
        trim?: boolean;
    }
}
