import { Field } from "../field";
import { Schema, Model } from "../..";
export declare function Obj<TVal extends Schema<TVal>>(fields: TVal, opts?: Obj.Options): Obj.ObjectField<TVal>;
export declare namespace Obj {
    interface Options extends Field.Options<any> {
    }
    class ObjectField<TVal extends Schema<TVal>> extends Field<{
        [P in keyof TVal]: TVal[P]['defaultValue'];
    }> {
        readonly fields: TVal;
        readonly conditionValue: {
            [P in keyof TVal]?: TVal[P]['conditionValue'];
        };
        readonly subtype: {
            [P in keyof TVal]: (TVal[P]['subtype'] & {
                readonly $: TVal[P];
            });
        };
        readonly fieldsArray: Field.Any[];
        constructor(fields: TVal, opts?: Options);
        init(name: string, model: Model.Any): void;
        fillSchema(schema: any): void;
        convert(value: any, opts: Field.ConvertOpts): { [P in keyof TVal]: TVal[P]["defaultValue"]; };
        toString(tab?: string): string;
    }
}
export declare type Obj<TVal extends Schema<TVal>> = Obj.ObjectField<TVal>;
