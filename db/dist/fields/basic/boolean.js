"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Boolean(opts) {
    return new Boolean.BooleanField(opts);
}
exports.Boolean = Boolean;
(function (Boolean) {
    class BooleanField extends field_1.Field {
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            return !!value;
        }
    }
    Boolean.BooleanField = BooleanField;
})(Boolean = exports.Boolean || (exports.Boolean = {}));
//# sourceMappingURL=boolean.js.map