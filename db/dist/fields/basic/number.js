"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Number(opts) {
    return new Number.NumberField(opts);
}
exports.Number = Number;
(function (Number) {
    class NumberField extends field_1.Field {
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            return +value;
        }
    }
    Number.NumberField = NumberField;
})(Number = exports.Number || (exports.Number = {}));
//# sourceMappingURL=number.js.map