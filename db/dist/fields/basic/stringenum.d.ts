import { Field } from "../field";
export declare function StringEnum<T extends string[]>(...args: T): StringEnum.StringEnumField<T>;
export declare namespace StringEnum {
    class StringEnumField<T extends string[]> extends Field<T[number]> {
        readonly values: T;
        constructor(values: T, opts?: Field.Options<T>);
    }
}
export declare type StringEnum<T extends string[]> = StringEnum.StringEnumField<T>;
