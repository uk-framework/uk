"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Abstract(opts) {
    return new Abstract.AbstractField(opts);
}
exports.Abstract = Abstract;
(function (Abstract) {
    class AbstractField extends field_1.Field {
        convert(value, opts) {
            return undefined;
        }
    }
    Abstract.AbstractField = AbstractField;
})(Abstract = exports.Abstract || (exports.Abstract = {}));
//# sourceMappingURL=abstract.js.map