import { Field } from "../field";
export declare function Enum<T>(enumeration: Enum.DummyType, opts?: Enum.Options<T>): Enum.EnumField<T>;
export declare namespace Enum {
    enum Dummy {
    }
    type DummyType = typeof Dummy;
    class EnumField<T> extends Field<T> {
        protected enumeration: DummyType;
        constructor(enumeration: DummyType, opts?: Field.Options<T>);
    }
    interface Options<T> extends Field.Options<T> {
    }
}
export declare type Enum<T> = Enum.EnumField<T>;
