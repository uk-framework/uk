"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Many(schemaCtor, toField) {
    return new Many.ManyField(schemaCtor, toField);
}
exports.Many = Many;
(function (Many) {
    class ManyField extends field_1.Field {
        constructor(schema, toField) {
            super();
            this.schema = schema;
            this.toField = toField;
        }
        toString(tab) {
            return super.toString(tab) + " -> " + this.target.$.name;
        }
        init(name, model) {
            super.init(name, model);
            this.target = this.model.$.db.models.find(mdl => mdl.$.schema === this.schema);
            if (!this.target)
                throw new Error(`Foreign model with schema '${this.schema.name}' for field '${this.model.$.name}.${name}' not found!`);
        }
        convert(value, opts) {
            if (!value)
                return super.convert(value, opts);
            const pkname = this.target.$.pk.name;
            return value.map((i) => {
                return opts.truncate ? {
                    [pkname]: this.target.$.pk.convert(i[pkname], {}),
                } : i;
            });
        }
    }
    Many.ManyField = ManyField;
})(Many = exports.Many || (exports.Many = {}));
//# sourceMappingURL=many.js.map