import { Field } from "../field";
export declare function Boolean(opts?: Boolean.Options): Boolean.BooleanField;
export declare namespace Boolean {
    class BooleanField extends Field<boolean> {
        convert(value: any, opts: Field.ConvertOpts): boolean;
    }
    interface Options extends Field.Options<boolean> {
    }
}
export declare type Boolean = Boolean.BooleanField;
