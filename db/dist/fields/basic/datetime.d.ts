import { Field } from "../field";
export declare function DateTime(opts?: DateTime.Options): DateTime.DateTimeField;
export declare function OnlyDate(opts?: DateTime.Options): DateTime.DateField;
export declare namespace DateTime {
    interface Options extends Field.Options<Date> {
        min?: Date;
        max?: Date;
    }
    class DateTimeField extends Field<Date, Options> {
        minMax(min: Date, max?: Date): this;
        convert(value: any, opts: Field.ConvertOpts): Date;
    }
    class DateField extends Field<Date, Options> {
        minMax(min: Date, max?: Date): this;
        convert(value: any, opts: Field.ConvertOpts): Date;
    }
}
export declare type DateTime = DateTime.DateTimeField;
export declare type OnlyDate = DateTime.DateField;
