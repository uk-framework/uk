"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function One(schemaCtor, opts) {
    return new One.OneField(schemaCtor, opts);
}
exports.One = One;
(function (One) {
    class OneField extends field_1.Field {
        constructor(schema, opts) {
            super(opts);
            this.schema = schema;
        }
        toString(tab) {
            return super.toString(tab) + " -> " + this.target.$.name;
        }
        init(name, model) {
            super.init(name, model);
            this.target = this.model.$.db.models.find(mdl => mdl.$.schema === this.schema);
            if (!this.target)
                throw new Error(`Foreign model with schema '${this.schema.name}' for field '${this.model.$.name}.${name}' not found!`);
        }
        convert(value, opts) {
            if (!value)
                return super.convert(value, opts);
            const pkname = this.target.$.pk.name;
            return opts.truncate ? {
                [pkname]: this.target.$.pk.convert(value[pkname], {}),
            } : value;
        }
    }
    One.OneField = OneField;
})(One = exports.One || (exports.One = {}));
//# sourceMappingURL=one.js.map