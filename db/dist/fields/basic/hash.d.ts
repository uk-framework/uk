import { Field } from "../field";
export declare function Hash<TValue, TField extends Field<TValue>>(protoField: TField, opts?: Hash.Options): Hash.HashField<TField, TValue>;
export declare namespace Hash {
    class HashField<TField extends Field<TValue>, TValue> extends Field<{
        [key: string]: TField['defaultValue'];
    }> {
        readonly protoField: TField;
        readonly subtype: TField['subtype'];
        constructor(protoField: TField, opts?: Options);
        convert(value: any, opts: Field.ConvertOpts): {
            [key: string]: TField["defaultValue"];
        };
    }
    interface Options extends Field.Options<any> {
    }
}
export declare type Hash<TValue, TField extends Field<TValue>> = Hash.HashField<TField, TValue>;
