"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Virtual(getter, setter) {
    return new Virtual.VirtualField(getter, setter);
}
exports.Virtual = Virtual;
(function (Virtual) {
    class VirtualField extends field_1.Field {
        constructor(getter, setter) {
            super();
            this.getter = getter;
            this.setter = setter;
        }
        convert(value, opts) {
            return undefined;
        }
        value(row, val) {
            if (val === undefined) {
                return this.getter.call(row);
            }
            else
                return this;
        }
    }
    Virtual.VirtualField = VirtualField;
})(Virtual = exports.Virtual || (exports.Virtual = {}));
//# sourceMappingURL=virtual.js.map