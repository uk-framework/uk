import { Field } from "../field";
import { Schema, Model, Ctor } from "../..";
export declare function Many<ToSchema extends Schema<ToSchema>>(schemaCtor: Ctor<ToSchema>, toField?: keyof ToSchema): Many.ManyField<ToSchema>;
export declare namespace Many {
    interface Options extends Field.Options<any> {
    }
    class ManyField<ToSchema extends Schema<ToSchema>> extends Field<Model.Row<ToSchema>[]> {
        private schema;
        private toField?;
        target: Model.Any;
        readonly subtype: {
            [P in keyof ToSchema]: (ToSchema[P]['subtype'] & {
                readonly $: ToSchema[P];
            });
        };
        constructor(schema: Ctor<ToSchema>, toField?: keyof ToSchema);
        toString(tab?: string): string;
        init(name: string, model: Model.Any): void;
        convert(value: any, opts: Field.ConvertOpts): any;
    }
}
export declare type Many<ToSchema extends Schema<ToSchema>> = Many.ManyField<ToSchema>;
