"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function String(opts) {
    return new String.StringField(opts);
}
exports.String = String;
(function (String) {
    class StringField extends field_1.Field {
        convert(value, opts) {
            if (value === undefined || value === null)
                return super.convert(value, opts);
            return this.options.trim ? value.toString().trim() : value.toString();
        }
    }
    String.StringField = StringField;
})(String = exports.String || (exports.String = {}));
//# sourceMappingURL=string.js.map