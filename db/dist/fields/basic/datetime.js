"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function DateTime(opts) {
    return new DateTime.DateTimeField(opts);
}
exports.DateTime = DateTime;
function OnlyDate(opts) {
    return new DateTime.DateField(opts);
}
exports.OnlyDate = OnlyDate;
(function (DateTime) {
    class DateTimeField extends field_1.Field {
        minMax(min, max) {
            return this;
        }
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            return new Date(value);
        }
    }
    DateTime.DateTimeField = DateTimeField;
    class DateField extends field_1.Field {
        minMax(min, max) {
            return this;
        }
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            return new Date(value);
        }
    }
    DateTime.DateField = DateField;
})(DateTime = exports.DateTime || (exports.DateTime = {}));
//# sourceMappingURL=datetime.js.map