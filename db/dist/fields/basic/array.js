"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field_1 = require("../field");
function Array(protoField, opts) {
    return new Array.ArrayField(protoField, opts);
}
exports.Array = Array;
(function (Array) {
    class ArrayField extends field_1.Field {
        constructor(protoField, opts) {
            super(opts);
            this.protoField = protoField;
        }
        init(name, model) {
            super.init(name, model);
            this.protoField.init("$$$", model);
        }
        convert(value, opts) {
            if (!value)
                return super.convert(value, opts);
            const rv = [];
            for (const item of value) {
                const converted = this.protoField.convert(item, opts);
                rv.push(converted);
            }
            return rv;
        }
    }
    Array.ArrayField = ArrayField;
})(Array = exports.Array || (exports.Array = {}));
//# sourceMappingURL=array.js.map