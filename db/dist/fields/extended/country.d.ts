import { String } from "..";
export declare function Country(opts?: Country.Options): Country.CountryField;
export declare namespace Country {
    class CountryField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Country = Country.CountryField;
