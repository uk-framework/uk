import { Object, String, Number } from "..";
import { ZipCode } from "./zipcode";
import { Country } from "./country";
import { Field } from "../field";
export declare function Address(opts?: Address.Options): Address.AddressField;
export declare namespace Address {
    class Schema {
        zip: ZipCode.ZipCodeField;
        country: Country.CountryField;
        state: String.StringField<string>;
        city: String.StringField<string>;
        floor: Number.NumberField;
        address: Object.ObjectField<{
            line1: String.StringField<any>;
            line2: String.StringField<any>;
        }>;
    }
    class AddressField extends Object.ObjectField<Schema> {
        constructor(opts?: Options);
    }
    interface Options extends Field.Options<Schema> {
    }
}
export declare type Address = Address.AddressField;
