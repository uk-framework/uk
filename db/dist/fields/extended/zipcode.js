"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function ZipCode(opts) {
    return new ZipCode.ZipCodeField(opts);
}
exports.ZipCode = ZipCode;
(function (ZipCode) {
    class ZipCodeField extends __1.String.StringField {
    }
    ZipCode.ZipCodeField = ZipCodeField;
})(ZipCode = exports.ZipCode || (exports.ZipCode = {}));
//# sourceMappingURL=zipcode.js.map