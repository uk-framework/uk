import { String } from "..";
export declare function Url(opts?: Url.Options): Url.UrlField;
export declare namespace Url {
    class UrlField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Url = Url.UrlField;
