"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function MongoID(opts) {
    return new MongoID.MongoIDField(opts);
}
exports.MongoID = MongoID;
(function (MongoID) {
    class MongoIDField extends __1.String.StringField {
        fromString(id) {
            const StringToObjectID = this.model.$.db.drvFactory['ObjectID'];
            return typeof StringToObjectID === 'function' ? StringToObjectID(id) : id;
        }
        convert(value, opts) {
            if (value === undefined)
                return super.convert(value, opts);
            return this.fromString(value);
        }
    }
    MongoID.MongoIDField = MongoIDField;
})(MongoID = exports.MongoID || (exports.MongoID = {}));
//# sourceMappingURL=mongoid.js.map