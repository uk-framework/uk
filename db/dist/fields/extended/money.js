"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field = require("..");
function Money(opts) {
    return new Money.MoneyField(opts);
}
exports.Money = Money;
(function (Money) {
    class Schema {
        constructor() {
            this.amount = field.Number();
            this.currency = field.String();
        }
    }
    Money.Schema = Schema;
    class MoneyField extends field.Object.ObjectField {
        constructor(opts) {
            super(new Schema, opts);
        }
    }
    Money.MoneyField = MoneyField;
})(Money = exports.Money || (exports.Money = {}));
//# sourceMappingURL=money.js.map