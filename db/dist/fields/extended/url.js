"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Url(opts) {
    return new Url.UrlField(opts);
}
exports.Url = Url;
(function (Url) {
    class UrlField extends __1.String.StringField {
    }
    Url.UrlField = UrlField;
})(Url = exports.Url || (exports.Url = {}));
//# sourceMappingURL=url.js.map