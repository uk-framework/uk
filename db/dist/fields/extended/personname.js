"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field = require("..");
function PersonName(opts) {
    return new PersonName.PersonNameField(opts);
}
exports.PersonName = PersonName;
(function (PersonName) {
    class Schema {
        constructor() {
            this.first = field.String();
            this.last = field.String();
            this.mid = field.String();
            this.alias = field.String();
            this.full = field.String();
        }
    }
    PersonName.Schema = Schema;
    class PersonNameField extends field.Object.ObjectField {
        constructor(opts) {
            super(new Schema, opts);
        }
    }
    PersonName.PersonNameField = PersonNameField;
})(PersonName = exports.PersonName || (exports.PersonName = {}));
//# sourceMappingURL=personname.js.map