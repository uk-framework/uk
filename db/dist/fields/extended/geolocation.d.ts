import * as field from "..";
export declare function GeoLocation(opts?: GeoLocation.Options): GeoLocation.GeoLocationField;
export declare namespace GeoLocation {
    class Schema {
        lat: field.Number.NumberField;
        lng: field.Number.NumberField;
        alt?: field.Number.NumberField;
    }
    class GeoLocationField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options);
    }
    interface Options extends field.Object.Options {
    }
}
export declare type GeoLocation = GeoLocation.GeoLocationField;
