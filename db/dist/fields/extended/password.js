"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Password(opts) {
    return new Password.PasswordField(opts);
}
exports.Password = Password;
(function (Password) {
    class PasswordField extends __1.String.StringField {
    }
    Password.PasswordField = PasswordField;
})(Password = exports.Password || (exports.Password = {}));
//# sourceMappingURL=password.js.map