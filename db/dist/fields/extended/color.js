"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Color(opts) {
    return new Color.ColorField(opts);
}
exports.Color = Color;
(function (Color) {
    class ColorField extends __1.String.StringField {
    }
    Color.ColorField = ColorField;
})(Color = exports.Color || (exports.Color = {}));
//# sourceMappingURL=color.js.map