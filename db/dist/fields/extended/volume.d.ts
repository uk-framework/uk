import { Number } from "..";
export declare function Volume(opts?: Volume.Options): Volume.VolumeField;
export declare namespace Volume {
    class VolumeField extends Number.NumberField {
    }
    interface Options extends Number.Options {
    }
}
export declare type Volume = Volume.VolumeField;
