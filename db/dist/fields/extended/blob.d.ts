import { String } from "..";
export declare function Blob(opts?: Blob.Options): Blob.BlobField;
export declare namespace Blob {
    class BlobField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Blob = Blob.BlobField;
