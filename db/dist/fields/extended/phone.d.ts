import { String } from "..";
export declare function Phone(opts?: Phone.Options): Phone.PhoneField;
export declare namespace Phone {
    class PhoneField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Phone = Phone.PhoneField;
