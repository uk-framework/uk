"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Blob(opts) {
    return new Blob.BlobField(opts);
}
exports.Blob = Blob;
(function (Blob) {
    class BlobField extends __1.String.StringField {
    }
    Blob.BlobField = BlobField;
})(Blob = exports.Blob || (exports.Blob = {}));
//# sourceMappingURL=blob.js.map