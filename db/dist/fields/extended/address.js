"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
const zipcode_1 = require("./zipcode");
const country_1 = require("./country");
function Address(opts) {
    return new Address.AddressField(opts);
}
exports.Address = Address;
(function (Address) {
    class Schema {
        constructor() {
            this.zip = zipcode_1.ZipCode();
            this.country = country_1.Country();
            this.state = __1.String();
            this.city = __1.String();
            this.floor = __1.Number();
            this.address = __1.Object({
                line1: __1.String(),
                line2: __1.String()
            });
        }
    }
    Address.Schema = Schema;
    class AddressField extends __1.Object.ObjectField {
        constructor(opts) {
            super(new Schema);
        }
    }
    Address.AddressField = AddressField;
})(Address = exports.Address || (exports.Address = {}));
//# sourceMappingURL=address.js.map