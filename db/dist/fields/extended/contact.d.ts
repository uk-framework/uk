import * as field from "..";
import { Phone } from "./phone";
import { Email } from "./email";
import { Address } from "./address";
export declare function Contact(opts?: Contact.Options): Contact.ContactField;
export declare namespace Contact {
    class Schema {
        phones?: field.Object.ObjectField<{
            mobile: field.Array.ArrayField<unknown, Phone.PhoneField>;
            home: field.Array.ArrayField<unknown, Phone.PhoneField>;
            other: field.Array.ArrayField<unknown, Phone.PhoneField>;
        }>;
        emails?: field.Array.ArrayField<unknown, Email.EmailField>;
        address?: field.Array.ArrayField<unknown, Address.AddressField>;
        employment?: field.Array.ArrayField<unknown, field.Object.ObjectField<{
            company: field.String.StringField<any>;
            address: Address.AddressField;
            department: field.String.StringField<any>;
            position: field.String.StringField<any>;
            phones: field.Array.ArrayField<unknown, Phone.PhoneField>;
            emails: field.Array.ArrayField<unknown, Email.EmailField>;
            fax: field.Array.ArrayField<unknown, Phone.PhoneField>;
        }>>;
    }
    class ContactField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options);
    }
    interface Options extends field.Object.Options {
    }
}
