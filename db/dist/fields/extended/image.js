"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Image(opts) {
    return new Image.ImageField(opts);
}
exports.Image = Image;
(function (Image) {
    class ImageField extends __1.String.StringField {
    }
    Image.ImageField = ImageField;
})(Image = exports.Image || (exports.Image = {}));
//# sourceMappingURL=image.js.map