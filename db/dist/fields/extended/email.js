"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Email(opts) {
    return new Email.EmailField(opts);
}
exports.Email = Email;
(function (Email) {
    class EmailField extends __1.String.StringField {
    }
    Email.EmailField = EmailField;
})(Email = exports.Email || (exports.Email = {}));
//# sourceMappingURL=email.js.map