"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Country(opts) {
    return new Country.CountryField(opts);
}
exports.Country = Country;
(function (Country) {
    class CountryField extends __1.String.StringField {
    }
    Country.CountryField = CountryField;
})(Country = exports.Country || (exports.Country = {}));
//# sourceMappingURL=country.js.map