import { String } from "..";
export declare function Color(opts?: Color.Options): Color.ColorField;
export declare namespace Color {
    class ColorField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Color = Color.ColorField;
