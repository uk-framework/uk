import * as field from "..";
export declare function PersonName(opts?: PersonName.Options): PersonName.PersonNameField;
export declare namespace PersonName {
    class Schema {
        first: field.String.StringField<string>;
        last?: field.String.StringField<string>;
        mid?: field.String.StringField<string>;
        alias?: field.String.StringField<string>;
        full?: field.String.StringField<string>;
    }
    class PersonNameField extends field.Object.ObjectField<Schema> {
        constructor(opts?: Options);
    }
    interface Options extends field.Object.Options {
    }
}
export declare type PersonName = PersonName.PersonNameField;
