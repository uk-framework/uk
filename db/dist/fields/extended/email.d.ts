import { String } from "..";
export declare function Email(opts?: Email.Options): Email.EmailField;
export declare namespace Email {
    class EmailField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Email = Email.EmailField;
