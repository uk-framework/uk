"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function RichText(opts) {
    return new RichText.RichTextField(opts);
}
exports.RichText = RichText;
(function (RichText) {
    class RichTextField extends __1.String.StringField {
    }
    RichText.RichTextField = RichTextField;
})(RichText = exports.RichText || (exports.RichText = {}));
//# sourceMappingURL=richtext.js.map