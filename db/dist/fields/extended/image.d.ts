import { String } from "..";
export declare function Image(opts?: Image.Options): Image.ImageField;
export declare namespace Image {
    class ImageField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Image = Image.ImageField;
