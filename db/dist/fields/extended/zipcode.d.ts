import { String } from "..";
export declare function ZipCode(opts?: ZipCode.Options): ZipCode.ZipCodeField;
export declare namespace ZipCode {
    class ZipCodeField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type ZipCode = ZipCode.ZipCodeField;
