"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field = require("..");
function GeoLocation(opts) {
    return new GeoLocation.GeoLocationField(opts);
}
exports.GeoLocation = GeoLocation;
(function (GeoLocation) {
    class Schema {
        constructor() {
            this.lat = field.Number({});
            this.lng = field.Number({});
            this.alt = field.Number();
        }
    }
    GeoLocation.Schema = Schema;
    class GeoLocationField extends field.Object.ObjectField {
        constructor(opts) {
            super(new Schema, opts);
        }
    }
    GeoLocation.GeoLocationField = GeoLocationField;
})(GeoLocation = exports.GeoLocation || (exports.GeoLocation = {}));
//# sourceMappingURL=geolocation.js.map