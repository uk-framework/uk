import { String } from "..";
import { Field } from "../field";
export declare function MongoID(opts?: MongoID.Options): MongoID.MongoIDField;
export declare namespace MongoID {
    class MongoIDField extends String.StringField {
        fromString(id: string): any;
        convert(value: any, opts: Field.ConvertOpts): any;
    }
    interface Options extends String.Options {
    }
}
export declare type MongoID = MongoID.MongoIDField;
