import { String } from "..";
export declare function Login(opts?: Login.Options): Login.LoginField;
export declare namespace Login {
    class LoginField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Login = Login.LoginField;
