"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const field = require("..");
const phone_1 = require("./phone");
const email_1 = require("./email");
const address_1 = require("./address");
function Contact(opts) {
    return new Contact.ContactField(opts);
}
exports.Contact = Contact;
(function (Contact) {
    class Schema {
        constructor() {
            this.phones = field.Object({
                mobile: field.Array(phone_1.Phone()),
                home: field.Array(phone_1.Phone()),
                other: field.Array(phone_1.Phone())
            });
            this.emails = field.Array(email_1.Email());
            this.address = field.Array(address_1.Address());
            this.employment = field.Array(field.Object({
                company: field.String(),
                address: address_1.Address(),
                department: field.String(),
                position: field.String(),
                phones: field.Array(phone_1.Phone()),
                emails: field.Array(email_1.Email()),
                fax: field.Array(phone_1.Phone()),
            }));
        }
    }
    Contact.Schema = Schema;
    class ContactField extends field.Object.ObjectField {
        constructor(opts) {
            super(new Schema, opts);
        }
    }
    Contact.ContactField = ContactField;
})(Contact = exports.Contact || (exports.Contact = {}));
//# sourceMappingURL=contact.js.map