import { String } from "..";
export declare function Password(opts?: Password.Options): Password.PasswordField;
export declare namespace Password {
    class PasswordField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Password = Password.PasswordField;
