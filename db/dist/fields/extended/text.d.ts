import { String } from "..";
export declare function Text(opts?: Text.Options): Text.TextField;
export declare namespace Text {
    class TextField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type Text = Text.TextField;
