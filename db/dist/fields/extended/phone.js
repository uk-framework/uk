"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Phone(opts) {
    return new Phone.PhoneField(opts);
}
exports.Phone = Phone;
(function (Phone) {
    class PhoneField extends __1.String.StringField {
    }
    Phone.PhoneField = PhoneField;
})(Phone = exports.Phone || (exports.Phone = {}));
//# sourceMappingURL=phone.js.map