import { String } from "..";
export declare function RichText(opts?: RichText.Options): RichText.RichTextField;
export declare namespace RichText {
    class RichTextField extends String.StringField {
    }
    interface Options extends String.Options {
    }
}
export declare type RichText = RichText.RichTextField;
