import * as field from "..";
export declare function Money(opts?: Money.Options): Money.MoneyField;
export declare namespace Money {
    class Schema {
        amount: field.Number.NumberField;
        currency?: field.String.StringField<string>;
    }
    class MoneyField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options);
    }
    interface Options extends field.Object.Options {
    }
}
export declare type Money = Money.MoneyField;
