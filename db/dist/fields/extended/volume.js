"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
function Volume(opts) {
    return new Volume.VolumeField(opts);
}
exports.Volume = Volume;
(function (Volume) {
    class VolumeField extends __1.Number.NumberField {
    }
    Volume.VolumeField = VolumeField;
})(Volume = exports.Volume || (exports.Volume = {}));
//# sourceMappingURL=volume.js.map