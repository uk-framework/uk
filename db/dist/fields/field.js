"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Field {
    constructor(options) {
        this.options = options;
        this.self = this;
        if (!options)
            options = this.options = {};
        if (!('required' in options))
            options.required = true;
    }
    get typeName() { return this.constructor.name; }
    hasValue(row) {
        return !!this.value(row);
    }
    value(row, val) {
        if (!row)
            return row;
        row = this.parent ? this.parent.parentValue(row) : row;
        if (val === undefined)
            return row[this.name];
        row[this.name] = val;
        return this;
    }
    parentValue(row) {
        if (this.parent)
            row = this.parent.parentValue(row);
        if (row[this.name])
            return row[this.name];
        return row[this.name] = {};
    }
    toString(tab) {
        tab = tab || "";
        return `${tab}${this.name}: ${this.typeName}${this.options.primary ? ', primary' : ''}${this.options.index ? ', index' : ''}`;
    }
    init(name, model) {
        this.self.model = model;
        this.self.name = name;
    }
    convert(value, opts) {
        return (value === undefined || value === null) && opts.setDefault ? this.defaultValue : value;
    }
    fillSchema(schema) {
    }
}
exports.Field = Field;
//# sourceMappingURL=field.js.map