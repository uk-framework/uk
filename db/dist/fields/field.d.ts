import { Model } from "..";
export declare class Field<TVal, TOpts extends Field.Options<TVal> = Field.Options<TVal>> {
    readonly options?: TOpts;
    readonly defaultValue: TVal;
    readonly conditionValue: Field.Condition<TVal>;
    readonly subtype: {};
    readonly model: Model.Any;
    readonly name: string;
    readonly path: string;
    readonly parent: Field.Any;
    get typeName(): string;
    protected self: {
        -readonly [P in keyof this]: this[P];
    };
    constructor(options?: TOpts);
    hasValue(row: any): boolean;
    value(row: Field.AnyRow): TVal;
    value(row: Field.AnyRow, val: TVal): this;
    protected parentValue(row: Field.AnyRow): TVal;
    toString(tab?: string): string;
    init(name: string, model: Model.Any): void;
    convert(value: any, opts: Field.ConvertOpts): TVal;
    fillSchema(schema: any): void;
}
export declare namespace Field {
    type Any = Field<any>;
    type AnyRow = any;
    interface ConvertOpts {
        setDefault?: boolean;
        truncate?: boolean;
    }
    interface Options<TVal> {
        primary?: boolean;
        index?: boolean;
        unique?: boolean;
        required?: boolean;
    }
    type Condition<TVal> = TVal | {
        $eq?: TVal | undefined | null;
        $ne?: TVal | undefined | null;
        $lt?: TVal;
        $gt?: TVal;
        $nin?: TVal[];
        $in?: TVal[];
    };
}
