"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Item {
    constructor(query) {
        this.query = query;
        this.row = {};
        query.one().then(row => {
            this.row = row;
        });
    }
    get(field) {
        return field.value(this.row);
    }
    set(field, value) {
        field.value(this.row, value);
        this.triggerChange();
        return this;
    }
    save() {
        return this.query.model.save(this.row);
    }
    subscribe(func) {
        this.onChange = func;
    }
    unsubscribe(func) {
        this.onChange = undefined;
    }
    triggerChange() {
        this.onChange && this.onChange(this);
    }
}
exports.Item = Item;
//# sourceMappingURL=item.js.map