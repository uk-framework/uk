import { Field, Query, Schema, Database, Ctor, Driver, Row } from ".";
import { ActionEvent } from "@uk/tool";
export declare namespace Model {
    type Row<TSchema extends Schema<TSchema>> = {
        [P in keyof TSchema]: TSchema[P]['defaultValue'];
    };
    interface IModel<TSchema extends Schema<TSchema>, TRow = Model.Row<TSchema>> extends Model<TSchema> {
    }
    interface ValidationOptions {
        partial?: boolean;
    }
    interface SaveOptions {
        skipValidation?: boolean;
        cascade?: boolean;
        sync?: boolean;
        user?: any;
    }
    namespace PreHook {
        interface Base {
            opts: SaveOptions;
        }
        interface Validate<TSchema extends Schema<TSchema>> extends Base {
            row: Row<TSchema>;
        }
        interface Select<TSchema extends Schema<TSchema>> extends Base {
            patch: Partial<Row<TSchema>>;
        }
        interface Insert<TSchema extends Schema<TSchema>> extends Base {
            row: Row<TSchema>;
        }
        interface Update<TSchema extends Schema<TSchema>> extends Base {
            row?: Row<TSchema>;
            patch: Partial<Row<TSchema>>;
        }
        interface Remove<TSchema extends Schema<TSchema>> extends Base {
            patch: Partial<Row<TSchema>>;
        }
    }
    type Any = Model<any>;
}
export declare class Model<TSchema extends Schema<TSchema>> {
    readonly $: Readonly<Model<TSchema>['$$']>;
    constructor(db: Database, schemaCtor: Ctor<TSchema>);
    create(from?: Partial<Row<TSchema>>): Row<TSchema>;
    save(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<any>;
    insert(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<Row<TSchema>>;
    insert(rows: Row<TSchema>[], opts?: Model.SaveOptions): Promise<Row<TSchema>[]>;
    update(row: Row<TSchema>, update?: Partial<Row<TSchema>>, opts?: Model.SaveOptions): Promise<Driver.UpdateResult>;
    remove(id: string, opts?: Model.SaveOptions): Promise<Driver.RemoveResult>;
    remove(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<Driver.RemoveResult>;
    select(...fields: (Field.Any | {
        $: Field.Any;
    })[]): Query<TSchema>;
    findOne(_id: string): Promise<Model.Row<TSchema>>;
    validate(row: Partial<Row<TSchema>>, partial: "partial"): Promise<any>;
    validate(row: Row<TSchema>): Promise<any>;
    validateField(): Promise<void>;
    protected validateRecursive(row: Row<TSchema>, partial: "partial" | undefined, fields: Field.Any[], validator: object): Promise<void>;
    protected __driver<TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult>;
    protected __initialize(): void;
    protected readonly $$: {
        name: string;
        fields: Map<string, Field.Any>;
        fieldsArray: Field.Any[];
        db: Database;
        pk: Field.Any;
        schema: new () => TSchema;
        validate: ActionEvent<(ctx: Model.PreHook.Validate<TSchema>) => any>;
        pre: {
            select: ActionEvent<(ctx: Model.PreHook.Select<TSchema>) => any>;
            insert: ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>;
            update: ActionEvent<(ctx: Model.PreHook.Update<TSchema>) => any>;
            remove: ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>;
        };
        post: {
            select: ActionEvent<(ctx: Model.PreHook.Select<TSchema>) => any>;
            insert: ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>;
            update: ActionEvent<(ctx: Model.PreHook.Update<TSchema>) => any>;
            remove: ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>;
        };
        inspect: () => string;
    };
}
