"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const log_1 = require("@uk/log");
class DataSet {
    constructor(query) {
        this.query = query;
        this.totalCount = 0;
        this.loading = false;
        this.error = undefined;
        this.data = [];
        this.log = new log_1.Log("UK-DB:DS-" + this.query.model.$.name.toUpperCase());
    }
    get first() { return this.data[0]; }
    get last() { return this[this.data.length - 1]; }
    map(cb) {
        return this.data.map(cb);
    }
    reload() {
        if (this.loading)
            return;
        this['loading'] = true;
        this['error'] = undefined;
        this.log.debug("reload");
        setTimeout(async () => {
            try {
                const arr = await this.query.array();
                this['totalCount'] = arr.total;
                this.data = arr.rows;
            }
            finally {
                this['loading'] = false;
                this.log.debug("reload finally");
                if (this.onChangeHandler)
                    this.onChangeHandler();
            }
        }, 0);
        return this;
    }
    get(index) {
        return this.data[index] || {};
    }
    onUpdate(handler) {
        this.onChangeHandler = handler;
        return this;
    }
}
exports.DataSet = DataSet;
//# sourceMappingURL=dataset.js.map