export { Model } from "./model";
export { Query, WhereCondition } from "./query";
export { Field } from "./fields/field";
export { Database } from "./database";
export { Driver } from "./driver";
export { Item } from "./item";
export { DataSet } from "./dataset";
export * from "./validator";
import { Field } from "./fields/field";
import { Model } from "./model";
import { Driver } from "./driver";
export declare type Row<TSchema extends Schema<TSchema>> = Model.Row<TSchema>;
export declare type Schema<T> = {
    [P in keyof T]?: Field.Any;
};
export declare type Ctor<T> = {
    new (): T;
};
export declare type DriverFactory = () => Promise<Driver>;
import * as field from "./fields";
export { field };
