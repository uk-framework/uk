import { Schema, Model, Field, Driver, DataSet, Row, field } from ".";
export declare type WhereCondition<TSchema extends Schema<TSchema>> = {
    [P in keyof TSchema]?: TSchema[P]['conditionValue'];
} & {
    $and?: WhereCondition<TSchema>[];
    $or?: WhereCondition<TSchema>[];
};
export declare class Query<TSchema extends Schema<TSchema>> {
    model: Model<TSchema>;
    fields: Field.Any[];
    constructor(model: Model<TSchema>, fields: Field.Any[]);
    join<T>(fk: {
        $: field.One<T> | field.Many<T>;
    }): this;
    where(): WhereCondition<TSchema>;
    where(condition: WhereCondition<TSchema>): this;
    clause(): void;
    dataset(): DataSet<TSchema>;
    limit(limit: number, offset?: number): this;
    offset(value: number): this;
    notExists(): Promise<boolean>;
    exists(): Promise<boolean>;
    count(): Promise<number>;
    one(id?: string | WhereCondition<TSchema>): Promise<Row<TSchema>>;
    array(): Promise<Driver.SelectResult<TSchema>>;
    stream(onRow: (row: Row<TSchema>, index: number, totalCount: number) => boolean): Promise<number>;
    get db(): import("./database").Database;
    protected driver<TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult>;
    static serialize(query: Query.Any): Query.Serialized;
    static deserialize(query: Query.Serialized, model: Model.Any): Query<any>;
    private _offset;
    private _limit;
    private condition;
    private joins;
    private dataSet;
}
export declare namespace Query {
    type Any = Query<any>;
    interface Serialized {
        condition: WhereCondition<any>;
        fields: Field.Any[];
        joins: string[];
        limit: number;
        offset: number;
    }
}
