import { Query, Model, Schema } from ".";
export declare class DataSet<TSchema extends Schema<TSchema>> {
    readonly query: Query<TSchema>;
    get first(): DataSet.QueryRow<TSchema>;
    get last(): any;
    readonly totalCount = 0;
    readonly loading = false;
    readonly error: Error;
    constructor(query: Query<TSchema>);
    map<R>(cb: (el: DataSet.QueryRow<TSchema>, index: number, thiz: any) => R): R[];
    reload(): this;
    get(index: number): {};
    onUpdate(handler: DataSet.UpdateHandler): this;
    private onChangeHandler;
    private data;
    private log;
}
export declare module DataSet {
    type QueryRow<TSchema extends Schema<TSchema>> = Model.Row<TSchema> & {
        $loading?: boolean;
    };
    type UpdateHandler = null | (() => void);
}
