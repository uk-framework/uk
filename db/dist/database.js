"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
class ArrayMap extends Map {
    find(cb) {
        for (const pair of this) {
            if (cb(pair[1], pair[0]))
                return pair[1];
        }
    }
}
exports.ArrayMap = ArrayMap;
class Database {
    constructor() {
        this.models = new ArrayMap();
    }
    register(SchemaCtor, statics, modelName) {
        const rv = new _1.Model(this, SchemaCtor);
        if (this.models.has(rv.$.name))
            throw new Error(`Database already have model with name '${rv.$.name}'. You have to change model name`);
        if (this.models.find(m => m.$.schema === SchemaCtor))
            throw new Error(`Database already have model with Schema '${SchemaCtor.name}'. You have to inherit schema if you want to use same Schema in multiple models`);
        this.models.set(rv.$.name, rv);
        if (statics) {
            for (const n of Object.getOwnPropertyNames(statics)) {
                const fn = statics[n];
                rv[n] = fn;
            }
        }
        return rv;
    }
    async init(factory) {
        if (this.models.size === 0)
            throw new Error('Trying to init Database without models. Did you forget to import your model prior to Database.init()?');
        this.drvFactory = factory;
        for (const model of this.models.values()) {
            model['__initialize']();
        }
    }
    async migrate() {
        throw new Error('NOT IMPLEMENTED');
    }
    async driver(cb) {
        const driver = await this.drvFactory();
        const userpromise = cb(driver);
        const release = driver.release.bind(driver);
        if (userpromise) {
            userpromise.then(release, release);
            return userpromise;
        }
        else {
            const err = `Database.driver callback returns ${userpromise}, when Promise expected. Driver released`;
            console.warn(err);
            release();
            return Promise.reject(new Error(err));
        }
    }
}
exports.Database = Database;
Database.default = new Database();
//# sourceMappingURL=database.js.map