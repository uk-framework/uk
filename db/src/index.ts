export { Model } from "./model"
export { Query, WhereCondition } from "./query"
export { Field } from "./fields/field"
export { Database } from "./database"
export { Driver } from "./driver"
export { Item } from "./item"
export { DataSet } from "./dataset"
export * from "./validator"

import { Field } from "./fields/field"
import { Model } from "./model"
import { Driver } from "./driver"
import { DataSet } from "./dataset"

export type Row<TSchema extends Schema<TSchema>> = Model.Row<TSchema>
export type Schema<T> = {[P in keyof T]?: Field.Any }
export type Ctor<T> = { new(): T }
export type DriverFactory = () => Promise<Driver>;

import * as field from "./fields"
export { field }
/*
export function connectReact<TSchema extends Schema<TSchema>>(component: { forceUpdate: () => any, componentWillUnmount?: () => any }, ds: DataSet<TSchema>) {
    const unmount = component.componentWillUnmount;
    component.componentWillUnmount = function () {
        if (unmount) unmount.apply(this);
        ds.onUpdate(null);
    }
    ds.onUpdate(component.forceUpdate.bind(component));
    return ds;
}
*/
