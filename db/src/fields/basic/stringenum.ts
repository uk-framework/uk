import { Field } from "../field"

export function StringEnum<T extends string[]>(...args: T) {
    return new StringEnum.StringEnumField(args);
}

export namespace StringEnum {
    export class StringEnumField<T extends string[]> extends Field<T[number]> {
        constructor(readonly values: T, opts?: Field.Options<T>) {
            super();
        }
    }
}

export type StringEnum<T extends string[]> = StringEnum.StringEnumField<T>;
