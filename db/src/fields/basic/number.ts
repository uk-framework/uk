import { Field } from "../field"

export function Number(opts?: Number.Options) {
    return new Number.NumberField(opts);
}

export namespace Number {
    export class NumberField extends Field<number, Number.Options> {
        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            return +value;
        }
    }

    export interface Options extends Field.Options<number> {
        size?: 8|16|32|64,
    }
}

export type Number = Number.NumberField;
