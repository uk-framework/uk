import { Field } from "../field"
import { Schema, Model } from "../.."
import { noReadOnly } from "@uk/tool";

export function Obj<TVal extends Schema<TVal>>(fields: TVal, opts?: Obj.Options) {
    return new Obj.ObjectField(fields, opts);
}

export namespace Obj {
    export interface Options extends Field.Options<any> {
    }

    export class ObjectField<TVal extends Schema<TVal>> extends Field<{ [P in keyof TVal]: TVal[P]['defaultValue'] }> {
        readonly conditionValue: { [P in keyof TVal]?: TVal[P]['conditionValue'] };
        readonly subtype: { [P in keyof TVal]: (TVal[P]['subtype'] & { readonly $: TVal[P] }) };
        readonly fieldsArray = [] as Field.Any[];

        constructor(public readonly fields: TVal, opts?: Options) {
            super(opts);
            for (const n in this.fields) {
                const fld = this.fields[n];
                this.fieldsArray.push(fld);
            }
        }

        init(name: string, model: Model.Any) {
            super.init(name, model);
            for (const n in this.fields) {
                const fld = this.fields[n] as Field.Any;
                fld.init(n, model);
                noReadOnly(fld).parent = this;
            }
        }

        fillSchema(schema: any) {
            for (const n in this.fields) {
                const field = this.fields[n];
                field.fillSchema(schema[n] = { $: field })
            }
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            const rv = {} as TVal;
            for (const fld of this.fieldsArray) {
                rv[fld.name] = fld.convert(value[fld.name], opts) as any;
            }
            return rv;
        }

        toString(tab?: string) {
            let txt = super.toString(tab) + ":";
            tab += '\t';
            for (const name in this.fields) {
                txt += '\n' + this.fields[name].toString(tab);
            }
            return txt;
        }
    }
}

export type Obj<TVal extends Schema<TVal>> = Obj.ObjectField<TVal>;
