import { Field } from "../field"
import { Model } from "../.."

export function String<T = string>(opts?: String.Options) {
    return new String.StringField<T>(opts);
}

export namespace String {
    export class StringField<T = string> extends Field<T, Options> {
        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined || value === null) return super.convert(value, opts);
            return this.options.trim ? value.toString().trim() : value.toString();
        }
    }

    export interface Options extends Field.Options<string> {
        min?: number,
        max?: number,
        trim?: boolean,
    }
}
