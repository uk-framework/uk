import { Field } from "../field"
import { Model } from "../.."

export function Array<TValue, TField extends Field<TValue>>(protoField: TField, opts?: Array.Options) {
    return new Array.ArrayField(protoField, opts);
}

export namespace Array {
    export class ArrayField<TValue, TField extends Field<TValue>> extends Field<TField['defaultValue'][]> {
        readonly subtype: TField['subtype'];

        constructor(readonly protoField: TField, opts?: Options) {
            super(opts);
        }

        init(name: string, model: Model.Any) {
            super.init(name, model);
            this.protoField.init("$$$", model);
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (!value) return super.convert(value, opts);
            const rv = [] as TField['defaultValue'][];
            for (const item of value) {
                const converted = this.protoField.convert(item, opts) as any;
                // console.log('Array.convert', value.length, item, converted);
                rv.push(converted);
            }
            return rv;
        }
    }
    export interface Options extends Field.Options<any[]> {
    }
}

export type Array<TValue, TField extends Field<TValue>> = Array.ArrayField<TValue, TField>;
