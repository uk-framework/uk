import { Field } from "../field"
import { Schema, Model, Ctor } from "../.."

export function Virtual<ThisSchema extends Schema<ThisSchema>, T>(getter: (this: Model.Row<ThisSchema>) => T, setter?: (val: T) => void) {
    return new Virtual.VirtualField<ThisSchema, T>(getter, setter);
}

export namespace Virtual {
    export class VirtualField<ThisSchema extends Schema<ThisSchema>, T> extends Field<T> {
        constructor(protected getter: (this: Model.Row<ThisSchema>) => T,
            protected setter?: (val: T) => void) {
            super();
        }

        convert(value: any, opts: Field.ConvertOpts) {
            return undefined as any;
        }

        value(row: Field.AnyRow): T;
        value(row: Field.AnyRow, val: T): this;
        value(row: Field.AnyRow, val?: T): T | this {
            if (val === undefined) {
                return this.getter.call(row);
            } else return this;
        }
    }
}

export type Virtual<ThisSchema extends Schema<ThisSchema>, T> = Virtual.VirtualField<ThisSchema, T>;
