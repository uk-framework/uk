import { Field } from "../field"
import { Schema } from "../.."

export function Abstract<T>(opts?: Abstract.Options<T>) {
    return new Abstract.AbstractField<T>(opts);
}

export namespace Abstract {
    export class AbstractField<T> extends Field<T, Options<T>> {
        convert(value: any, opts: Field.ConvertOpts) {
            return undefined as any;
        }
    }

    export interface Options<T> extends Field.Options<T> {
    }
}

export type Abstract<T> = Abstract.AbstractField<T>;
