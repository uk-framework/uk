import { Field } from "../field"
import { Schema, Model, Ctor } from "../.."

export function Many<ToSchema extends Schema<ToSchema>>(schemaCtor: Ctor<ToSchema>, toField?: keyof ToSchema) {
    return new Many.ManyField(schemaCtor,toField);
}

export namespace Many {
    export interface Options extends Field.Options<any> {
    }

    export class ManyField<ToSchema extends Schema<ToSchema>> extends Field<Model.Row<ToSchema>[]> {
        target: Model.Any;
        readonly subtype: { [P in keyof ToSchema]: (ToSchema[P]['subtype'] & { readonly $: ToSchema[P] }) };

        constructor(private schema: Ctor<ToSchema>, private toField?: keyof ToSchema) {
            super();
        }

        toString(tab?: string) {
            return super.toString(tab) + " -> " + this.target.$.name;
        }

        init(name: string, model: Model.Any) {
            super.init(name, model);
            (<any>this).target = this.model.$.db.models.find(mdl=> mdl.$.schema === this.schema);
            if (!this.target) throw new Error(`Foreign model with schema '${this.schema.name}' for field '${this.model.$.name}.${name}' not found!`);
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (!value) return super.convert(value, opts);
            const pkname = this.target.$.pk.name;
            return value.map((i: any)=> {
                return opts.truncate ? {
                    [pkname]: this.target.$.pk.convert(i[pkname], {}),
                } : i;
            });
        }
    }
}

export type Many<ToSchema extends Schema<ToSchema>> = Many.ManyField<ToSchema>;
