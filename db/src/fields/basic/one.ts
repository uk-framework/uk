import { Field } from "../field"
import { Schema, Model, Ctor } from "../.."

export function One<ToSchema extends Schema<ToSchema>>(schemaCtor: Ctor<ToSchema>, opts?: One.Options) {
    return new One.OneField(schemaCtor, opts);
}

export namespace One {
    export interface Options extends Field.Options<any> {        
    }

    export class OneField<ToSchema extends Schema<ToSchema>> extends Field<Model.Row<ToSchema>> {
        readonly target: Model.Any;
        readonly subtype: { [P in keyof ToSchema]: (ToSchema[P]['subtype'] & { readonly $: ToSchema[P] }) };

        constructor(protected schema: Ctor<ToSchema>, opts?: Options) {
            super(opts);
        }

        toString(tab?: string) {
            return super.toString(tab) + " -> " + this.target.$.name;
        }

        init(name: string, model: Model.Any) {
            super.init(name, model);
            (<any>this).target = this.model.$.db.models.find(mdl=> mdl.$.schema === this.schema);
            if (!this.target) throw new Error(`Foreign model with schema '${this.schema.name}' for field '${this.model.$.name}.${name}' not found!`);
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (!value) return super.convert(value, opts);
            const pkname = this.target.$.pk.name;
            return opts.truncate ? {
                [pkname]: this.target.$.pk.convert(value[pkname], {}),
            } : value;
        }
    }
}

export type One<ToSchema extends Schema<ToSchema>> = One.OneField<ToSchema>;
