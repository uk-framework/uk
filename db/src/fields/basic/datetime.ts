import { Field } from "../field"

export function DateTime(opts?: DateTime.Options) {
    return new DateTime.DateTimeField(opts);
}
export function OnlyDate(opts?: DateTime.Options) {
    return new DateTime.DateField(opts);
}
export namespace DateTime {
    export interface Options extends Field.Options<Date> {
        min?: Date,
        max?: Date,
    }

    export class DateTimeField extends Field<Date, Options> {
        minMax(min: Date, max?: Date) {
            return this;
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            return new Date(value);
        }
    }
    export class DateField extends Field<Date, Options> {
        minMax(min: Date, max?: Date) {
            return this;
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            return new Date(value);
        }
    }
}

export type DateTime = DateTime.DateTimeField;
export type OnlyDate = DateTime.DateField;
