import { Field } from "../field"

export function Boolean(opts?: Boolean.Options) {
    return new Boolean.BooleanField(opts);
}

export namespace Boolean {
    export class BooleanField extends Field<boolean> {
        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            return !!value;
        }
    }
    export interface Options extends Field.Options<boolean> {
    }
}

export type Boolean = Boolean.BooleanField;
