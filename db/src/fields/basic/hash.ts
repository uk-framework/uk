import { Field } from "../field"
import * as field from ".."
import { Model } from "../.."

export function Hash<TValue, TField extends Field<TValue>>(protoField: TField, opts?: Hash.Options) {
    return new Hash.HashField<TField, TValue>(protoField, opts);
}

export namespace Hash {
    export class HashField<TField extends Field<TValue>, TValue> extends Field<{ [key: string]: TField['defaultValue'] }> {
        readonly subtype: TField['subtype'];

        constructor(readonly protoField: TField, opts?: Options) {
            super(opts);
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            const rv = {} as { [key: string]: TField['defaultValue'] };
            for (const key in value) {
                rv[key] = this.protoField.convert(value[key], opts) as any;
            }
            return rv;
        }
    }

    export interface Options extends Field.Options<any> {
    }
}

export type Hash<TValue, TField extends Field<TValue>> = Hash.HashField<TField, TValue>;
