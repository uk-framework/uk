import { Field } from "../field"

export function Enum<T>(enumeration: Enum.DummyType, opts?: Enum.Options<T>) {
    return new Enum.EnumField<T>(enumeration, opts);
}

export namespace Enum {

    export enum Dummy {
    }
    export type DummyType = typeof Dummy;

    export class EnumField<T> extends Field<T> {
        constructor(protected enumeration: DummyType, opts?: Field.Options<T>) {
            super(opts);
        }
    }

    export interface Options<T> extends Field.Options<T> {
    }
}

export type Enum<T> = Enum.EnumField<T>;
