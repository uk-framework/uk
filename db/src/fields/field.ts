import { Model } from ".."

export class Field<TVal, TOpts extends Field.Options<TVal> = Field.Options<TVal>> {
    readonly defaultValue: TVal;
    readonly conditionValue: Field.Condition<TVal>;
    readonly subtype: {};

    readonly model: Model.Any
    readonly name: string
    readonly path: string
    readonly parent: Field.Any

    get typeName() { return this.constructor.name; }

    protected self: { -readonly [P in keyof this]: this[P] } = this;

    constructor(readonly options?: TOpts) {
        if (!options) options = this.options = {} as any;
        if (!('required' in options)) options.required = true;
    }

    hasValue(row: any) {
        return !!this.value(row);
    }

    value(row: Field.AnyRow): TVal;
    value(row: Field.AnyRow, val: TVal): this;
    value(row: Field.AnyRow, val?: TVal): TVal | this {
        if (!row) return row;
        row = this.parent ? this.parent.parentValue(row) : row;
        if (val === undefined) return row[this.name];
        row[this.name] = val;
        return this;
    }

    protected parentValue(row: Field.AnyRow): TVal {
        if (this.parent) row = this.parent.parentValue(row);
        if (row[this.name]) return row[this.name];
        return row[this.name] = {} as TVal;
    }

    toString(tab?: string) {
        tab = tab || "";
        return `${tab}${this.name}: ${this.typeName}${this.options.primary ? ', primary' : ''}${this.options.index ? ', index' : ''}`;
    }

    init(name: string, model: Model.Any) {
        this.self.model = model;
        this.self.name = name;
    }

    convert(value: any, opts: Field.ConvertOpts): TVal {
        return (value === undefined || value === null) && opts.setDefault ? this.defaultValue : value;
    }

    fillSchema(schema: any) {
    }
}

export namespace Field {
    export type Any = Field<any>;
    export type AnyRow = any;

    export interface ConvertOpts {
        setDefault?: boolean
        truncate?: boolean
    }

    export interface Options<TVal> {
        primary?: boolean
        index?: boolean
        unique?: boolean
        required?: boolean
    }

    export type Condition<TVal> = TVal | {
        $eq?: TVal | undefined | null
        $ne?: TVal | undefined | null
        $lt?: TVal
        $gt?: TVal
        $nin?: TVal[]
        $in?: TVal[]
    }
}
