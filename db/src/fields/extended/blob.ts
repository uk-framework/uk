import { String } from ".."

export function Blob(opts?: Blob.Options) {
    return new Blob.BlobField(opts);
}

export namespace Blob {
    export class BlobField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Blob = Blob.BlobField;
