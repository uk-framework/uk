import { Object, String, Number } from ".."
import { ZipCode } from "./zipcode"
import { Country } from "./country"
import { Field } from "../field";

export function Address(opts?: Address.Options) {
    return new Address.AddressField(opts);
}

export namespace Address {
    export class Schema {
        zip = ZipCode();
        country = Country();
        state = String();
        city = String();
        floor = Number();
        address = Object({
            line1: String(),
            line2: String()
        });
    }

    export class AddressField extends Object.ObjectField<Schema> {
        constructor(opts?: Options) {
            super(new Schema);
        }
    }

    export interface Options extends Field.Options<Schema> {
    }
}

export type Address = Address.AddressField;
