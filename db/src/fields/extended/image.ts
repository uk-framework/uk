import { String } from ".."

export function Image(opts?: Image.Options) {
    return new Image.ImageField(opts);
}

export namespace Image {
    export class ImageField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Image = Image.ImageField;
