import * as field from ".."
import { Phone } from "./phone"
import { Email } from "./email"
import { Address } from "./address"

export function Contact(opts?: Contact.Options) {
    return new Contact.ContactField(opts);
}

export namespace Contact {
    export class Schema {
        phones?= field.Object({
            mobile: field.Array(Phone()),
            home: field.Array(Phone()),
            other: field.Array(Phone())
        });
        emails?= field.Array(Email())
        address?= field.Array(Address());
        employment?= field.Array(field.Object({
            company: field.String(),
            address: Address(),
            department: field.String(),
            position: field.String(),
            phones: field.Array(Phone()),
            emails: field.Array(Email()),
            fax: field.Array(Phone()),
        }));
    }

    export class ContactField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options) {
            super(new Schema, opts);
        }
    }

    export interface Options extends field.Object.Options {
    }
}
