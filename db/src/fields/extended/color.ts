import { String } from ".."

export function Color(opts?: Color.Options) {
    return new Color.ColorField(opts);
}

export namespace Color {
    export class ColorField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Color = Color.ColorField;
