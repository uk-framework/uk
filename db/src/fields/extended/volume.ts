import { Number } from ".."

export function Volume(opts?: Volume.Options) {
    return new Volume.VolumeField(opts);
}

export namespace Volume {
    export class VolumeField extends Number.NumberField {
    }
    export interface Options extends Number.Options {
    }
}

export type Volume = Volume.VolumeField;
