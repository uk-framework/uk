import { String } from ".."

export function Phone(opts?: Phone.Options) {
    return new Phone.PhoneField(opts);
}

export namespace Phone {
    export class PhoneField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Phone = Phone.PhoneField;
