import { String } from ".."

export function Login(opts?: Login.Options) {
    return new Login.LoginField(opts);
}

export namespace Login {
    export class LoginField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Login = Login.LoginField;
