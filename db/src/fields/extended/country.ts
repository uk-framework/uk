import { String } from ".."

export function Country(opts?: Country.Options) {
    return new Country.CountryField(opts);
}

export namespace Country {
    export class CountryField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Country = Country.CountryField;
