import { String } from ".."

export function Url(opts?: Url.Options) {
    return new Url.UrlField(opts);
}

export namespace Url {
    export class UrlField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Url = Url.UrlField;
