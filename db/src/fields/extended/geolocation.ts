import * as field from ".."

export function GeoLocation(opts?: GeoLocation.Options) {
    return new GeoLocation.GeoLocationField(opts);
}

export namespace GeoLocation {
    export class Schema {
        lat = field.Number({ /*min: -180, max: 180*/ });
        lng = field.Number({ /*min: -90,  max: 90*/ });
        alt?= field.Number();
    }

    export class GeoLocationField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options) {
            super(new Schema, opts);
        }
    }

    export interface Options extends field.Object.Options {
    }
}

export type GeoLocation = GeoLocation.GeoLocationField;
