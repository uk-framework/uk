import { String } from ".."

export function RichText(opts?: RichText.Options) {
    return new RichText.RichTextField(opts);
}

export namespace RichText {
    export class RichTextField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type RichText = RichText.RichTextField;
