import { String } from ".."

export function Password(opts?: Password.Options) {
    return new Password.PasswordField(opts);
}

export namespace Password {
    export class PasswordField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Password = Password.PasswordField;
