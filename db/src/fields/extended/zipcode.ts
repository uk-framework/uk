import { String } from ".."

export function ZipCode(opts?: ZipCode.Options) {
    return new ZipCode.ZipCodeField(opts);
}

export namespace ZipCode {
    export class ZipCodeField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type ZipCode = ZipCode.ZipCodeField;
