import { String } from ".."

export function Email(opts?: Email.Options) {
    return new Email.EmailField(opts);
}

export namespace Email {
    export class EmailField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Email = Email.EmailField;
