import { String } from ".."

export function Text(opts?: Text.Options) {
    return new Text.TextField(opts);
}

export namespace Text {
    export class TextField extends String.StringField {
    }
    export interface Options extends String.Options {
    }
}

export type Text = Text.TextField;
