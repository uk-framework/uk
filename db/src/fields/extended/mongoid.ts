import { String } from ".."
import { Field } from "../field"

export function MongoID(opts?: MongoID.Options) {
    return new MongoID.MongoIDField(opts);
}

export namespace MongoID {
    export class MongoIDField extends String.StringField {
        fromString(id: string): any {
            const StringToObjectID = this.model.$.db.drvFactory['ObjectID'];
            //console.log(StringToObjectID);
            return typeof StringToObjectID === 'function' ? StringToObjectID(id) : id;
        }

        convert(value: any, opts: Field.ConvertOpts) {
            if (value === undefined) return super.convert(value, opts);
            return this.fromString(value);
        }
    }
    export interface Options extends String.Options {
    }
}

export type MongoID = MongoID.MongoIDField;
