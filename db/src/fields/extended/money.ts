import * as field from ".."

export function Money(opts?: Money.Options) {
    return new Money.MoneyField(opts);
}

export namespace Money {
    export class Schema {
        amount = field.Number();
        currency? = field.String();
    }
    
    export class MoneyField extends field.Object.ObjectField<Schema> {
        constructor(opts: Options) {
            super(new Schema, opts);
        }
    }

    export interface Options extends field.Object.Options {
    }
}

export type Money = Money.MoneyField;
