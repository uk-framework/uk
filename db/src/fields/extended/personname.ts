import * as field from ".."

export function PersonName(opts?: PersonName.Options) {
    return new PersonName.PersonNameField(opts);
}

export namespace PersonName {
    export class Schema {
        first = field.String();
        last? = field.String();
        mid? = field.String();
        alias? = field.String();
        full? = field.String();
    }

    export class PersonNameField extends field.Object.ObjectField<Schema> {
        constructor(opts?: Options) {
            super(new Schema, opts);
        }
    }

    export interface Options extends field.Object.Options {
    }
}

export type PersonName = PersonName.PersonNameField;
