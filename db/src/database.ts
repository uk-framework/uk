import { Schema, Model, Field, DriverFactory, Driver } from "."

export type ModelConst<TSchema extends Schema<TSchema>, TStatic> =
    { [P in keyof TSchema]: (TSchema[P]['subtype'] & { readonly $: TSchema[P] }) } &
    Model.IModel<TSchema> &
    { [P in keyof TStatic]: TStatic[P] };

export class ArrayMap<TKey, TVal> extends Map<TKey, TVal> {
    find(cb: (val: TVal, key: TKey)=> boolean): TVal {
        for (const pair of this) {
            if (cb(pair[1], pair[0])) return pair[1];
        }
    }
}

export class Database {
    static readonly default = new Database();
    readonly models = new ArrayMap<string, Model.Any>();

    register<TSchema extends Schema<TSchema>, TStatic>(SchemaCtor: { new(): TSchema }, statics?: TStatic, modelName?: string):
        ModelConst<TSchema, TStatic> {

        const rv = new Model(this, SchemaCtor) as TSchema & Model.IModel<TSchema>;
        if (this.models.has(rv.$.name)) throw new Error(`Database already have model with name '${rv.$.name}'. You have to change model name`);
        if (this.models.find(m=> m.$.schema === SchemaCtor)) throw new Error(`Database already have model with Schema '${SchemaCtor.name}'. You have to inherit schema if you want to use same Schema in multiple models`);
        this.models.set(rv.$.name, rv);
        if (statics) {
            for (const n of Object.getOwnPropertyNames(statics)) {
                const fn = statics[n];
                rv[n] = fn;
            }
        }
        return rv as any;
    }

    async init(factory: DriverFactory) {
        if (this.models.size === 0) throw new Error('Trying to init Database without models. Did you forget to import your model prior to Database.init()?');
        (<any>this).drvFactory = factory;
        for (const model of this.models.values()) {
            model['__initialize']();
        }
    }

    async migrate() {
        throw new Error('NOT IMPLEMENTED');
    }

    async driver<TSchema extends Schema<TSchema>, TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult> {
        const driver = await this.drvFactory() as Driver<TSchema>;
        const userpromise = cb(driver);
        const release = driver.release.bind(driver);
        if (userpromise) {
            userpromise.then(release, release);
            return userpromise;
        } else {
            const err = `Database.driver callback returns ${userpromise}, when Promise expected. Driver released`;
            console.warn(err);
            release();
            return Promise.reject(new Error(err));
        }
    }

    readonly drvFactory: DriverFactory;
}
