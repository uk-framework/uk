import { Schema, Model, Query, Field, Row } from "."

export class Item<TSchema extends Schema<TSchema>> {
    // onChange
    // Loading
    // validation

    constructor(readonly query: Query<TSchema>) {
        query.one().then(row=> {
            this.row = row;
        });
    }

    get(field: Field.Any): any {
        return field.value(this.row);
    }

    set(field: Field.Any, value: any): this {
        field.value(this.row, value);
        this.triggerChange();
        return this;
    }

    save(): Promise<void> {
        return this.query.model.save(this.row);
    }

    subscribe(func: Item.ChangeHandler<TSchema>) {
        this.onChange = func;
    }

    unsubscribe(func: Item.ChangeHandler<TSchema>) {
        this.onChange = undefined;
    }

    protected triggerChange() {
        this.onChange && this.onChange(this);
    }

    private onChange?: Item.ChangeHandler<TSchema>;
    private row = {} as Row<TSchema>;
}

export namespace Item {
    export type Any = Item<any>;
    export type ChangeHandler<TSchema extends Schema<TSchema>> = (item: Item<TSchema>)=>void;
}
