import { Field, Query, Schema, Database, Ctor, Driver, Row, field, WhereCondition } from "."
import { ActionEvent } from "@uk/tool"
import { Validator } from "./validator";

export namespace Model {
    export type Row<TSchema extends Schema<TSchema>> = { [P in keyof TSchema]: TSchema[P]['defaultValue'] };

    export interface IModel<TSchema extends Schema<TSchema>, TRow = Model.Row<TSchema>> extends Model<TSchema> {
    }

    export interface ValidationOptions {
        partial?: boolean
    }

    export interface SaveOptions {
        skipValidation?: boolean
        cascade?: boolean
        sync?: boolean
        user?: any
    }

    export namespace PreHook {
        export interface Base {
            opts: SaveOptions
        }
        export interface Validate<TSchema extends Schema<TSchema>> extends Base {
            row: Row<TSchema>
        }
        export interface Select<TSchema extends Schema<TSchema>> extends Base {
            patch: Partial<Row<TSchema>>
        }
        export interface Insert<TSchema extends Schema<TSchema>> extends Base {
            row: Row<TSchema>
        }
        export interface Update<TSchema extends Schema<TSchema>> extends Base {
            row?: Row<TSchema>
            patch: Partial<Row<TSchema>>
        }
        export interface Remove<TSchema extends Schema<TSchema>> extends Base {
            patch: Partial<Row<TSchema>>
        }
    }

    export type Any = Model<any>
}

export class Model<TSchema extends Schema<TSchema>> {
    readonly $: Readonly<Model<TSchema>['$$']>;

    constructor(db: Database, schemaCtor: Ctor<TSchema>) {
        this.$ = this.$$;
        this.$$.db = db;
        this.$$.name = schemaCtor.name;
        this.$$.schema = schemaCtor;
    }

    create(from?: Partial<Row<TSchema>>): Row<TSchema> {
        from = from || {} as any;
        const rv = {};
        this.$.fields.forEach(fld => {
            rv[fld.name] = fld.convert(from[fld.name], { setDefault: true, truncate: false });
        });
        return rv as Row<TSchema>;
    }

    async save(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<any> {
        const pk = this.$.pk;
        const id = pk.value(row);
        if (id !== undefined) return this.update(row);
        else return this.insert(row);
    }

    async insert(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<Row<TSchema>>;
    async insert(rows: Row<TSchema>[], opts?: Model.SaveOptions): Promise<Row<TSchema>[]>;
    async insert(rows: Row<TSchema> | Row<TSchema>[], opts?: Model.SaveOptions): Promise<any> {
        let isarray = true;
        if (!Array.isArray(rows)) {
            rows = [rows];
            isarray = false;
        }
        const toInsertArr = [] as Row<TSchema>[];
        for (const row of rows) {
            const toInsert = {} as Row<TSchema>;
            this.$.fields.forEach(fld => {
                toInsert[fld.name] = fld.convert(row[fld.name], { setDefault: true, truncate: true });
            });
            await this.validate(toInsert);
            await this.$.pre.insert.fire({
                row: toInsert,
                opts: opts,
            });
            toInsertArr.push(toInsert);
        }
        const r = rows;
        return this.__driver(async drv => {
            const rv = await drv.insert(this, toInsertArr);
            const pkname = this.$.pk.name;
            for (let i = 0; i < r.length; i++) {
                if (r[i][pkname] === undefined || r[i][pkname] === null) r[i][pkname] = rv.pk[i];
                this.$.post.insert.fire({
                    row: r[i],
                    opts: opts,
                });
            }
            return isarray ? r : r[0];
        });
    }

    async update(row: Row<TSchema>, update?: Partial<Row<TSchema>>, opts?: Model.SaveOptions): Promise<Driver.UpdateResult> {
        const src = update || row;
        const toUpdate = {};
        this.$.fields.forEach(fld => {
            if (fld.name in src) {
                toUpdate[fld.name] = fld.convert(src[fld.name], { setDefault: false, truncate: true });
            }
        });

        await this.validate(toUpdate, "partial");

        const change = {
            patch: toUpdate,
            row: row,
            opts: opts,
        };
        await this.$.pre.update.fire(change);
        return this.__driver(async drv => {
            const pk = this.$.pk;
            const id = pk.value(row);
            Object.assign(row, update);
            const rv = await drv.update(this, { [pk.name]: id } as any, toUpdate);
            this.$.post.update.fire(change);
            return row;
        });
    }

    remove(id: string, opts?: Model.SaveOptions): Promise<Driver.RemoveResult>;
    remove(row: Row<TSchema>, opts?: Model.SaveOptions): Promise<Driver.RemoveResult>;
    async remove(rowOrId: Row<TSchema> | string, opts?: Model.SaveOptions): Promise<Driver.RemoveResult> {
        const row = typeof rowOrId === 'object' ? rowOrId : { [this.$.pk.name]: rowOrId } as Row<TSchema>;
        this.$.fields.forEach(fld => {
            if (fld.name in row) {
                row[fld.name] = fld.convert(row[fld.name], { setDefault: false, truncate: true });
            }
        });

        if (this.$.pre.remove.hasHandlers) {
            await this.$.pre.remove.fire({
                row: row,
                opts: opts
            });
        }
        return this.__driver(async drv => {
            //const rv = await drv.remove(this, {});    GET AFTER PULL, DELETE ALL ROWS IN DATABASE
            const rv = await drv.remove(this, row);  //WORKING CORRECTLY
            if (this.$.post.remove.hasHandlers) {
                this.$.post.remove.fire({
                    row: row,
                    opts: opts
                });
            }
            return rv;
        });
    }

    select(...fields: (Field.Any | { $: Field.Any })[]): Query<TSchema>
    select(...fields: (WhereCondition<TSchema> | (Field.Any | { $: Field.Any }))[]) {
        return new Query<TSchema>(this, fields as any);
    }

    async findOne(_id: string) {
        if (!_id) throw new Error(`${this.$.name}.findOne requires _id argument, got "${_id}"`);
        return this.select().where({ [this.$.pk.name]: _id } as any).one();
    }

    async validate(row: Partial<Row<TSchema>>, partial: "partial"): Promise<any>;
    async validate(row: Row<TSchema>): Promise<any>;
    async validate(row: Row<TSchema>, partial?: "partial"): Promise<any> {
        this.$$.validate.fire(null);
//        if (!this.$.schema.validator) return;
//        if (!this.$.validator) this.$$.validator = this.$.schema.validator();
//        return this.validateRecursive(row, partial, this.$.fieldsArray, this.$.validator);
    }

    async validateField() {

    }

    protected async validateRecursive(row: Row<TSchema>, partial: "partial" | undefined, fields: Field.Any[], validator: object) {
//        return Promise.all(fields.map(fld=> this.validateField(fld, row, validator)));
    }

    protected __driver<TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult> {
        return this.$.db.driver<TSchema, TResult>(cb);
    }

    protected __initialize() {
        const schema = new this.$.schema();
        for (const fieldName of Object.getOwnPropertyNames(schema)) {
            const field = schema[fieldName];
            if (field instanceof Field) {
                field['init'](fieldName, this);
                this[fieldName] = { $: field };
                field.fillSchema(this[fieldName]);
                this.$.fields.set(field.name, field);
                this.$.fieldsArray.push(field);
                if (field.options.primary) {
                    this.$$.pk = field;
                }
            }
        }
        if (!this.$.pk) throw new Error(`Model ${this.$.name} has no primary key.`);
    }

    protected readonly $$ = {
        name: "",
        fields: new Map<string, Field.Any>(),
        fieldsArray: [] as Field.Any[],
        db: undefined as Database,
        pk: undefined as Field.Any,
        schema: undefined as { new(): TSchema },
        validate: new ActionEvent<(ctx: Model.PreHook.Validate<TSchema>) => any>(),
//        validator: undefined as TSchema extends void ? any : Validator.Schema<TSchema>,
        pre: {
            select: new ActionEvent<(ctx: Model.PreHook.Select<TSchema>) => any>(),
            insert: new ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>(),
            update: new ActionEvent<(ctx: Model.PreHook.Update<TSchema>) => any>(),
            remove: new ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>(),
        },
        post: {
            select: new ActionEvent<(ctx: Model.PreHook.Select<TSchema>) => any>(),
            insert: new ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>(),
            update: new ActionEvent<(ctx: Model.PreHook.Update<TSchema>) => any>(),
            remove: new ActionEvent<(ctx: Model.PreHook.Insert<TSchema>) => any>(),
        },
        inspect: () => {
            let rv = `Inspecting model ${this.$.name}\n`;
            for (const fld of this.$.fields.values()) {
                rv += fld.toString('\t') + '\n';
            }
            return rv;
        }
    }
}
