import { Schema, Model, Field, Driver, DataSet, Row, field } from "."

export type WhereCondition<TSchema extends Schema<TSchema>> = {
    [P in keyof TSchema]?: TSchema[P]['conditionValue']
} & {
    $and?: WhereCondition<TSchema>[]
    $or?: WhereCondition<TSchema>[]
}

export class Query<TSchema extends Schema<TSchema>> {
    constructor(public model: Model<TSchema>, public fields: Field.Any[]) {
    }

    join<T>(fk: { $: field.One<T> | field.Many<T> }) {
        this.joins.push(fk.$);
        return this;
    }

    where(): WhereCondition<TSchema>;
    where(condition: WhereCondition<TSchema>): this;
    where(condition?: WhereCondition<TSchema>): this | WhereCondition<TSchema> {
        if (condition === undefined) return this.condition;
        for (const f of this.model.$.fields.values()) {
            if (f instanceof field.One.OneField) {
                const value = f.value(condition);
                if (value) {
                    if (value['_id']) {
                        f.value(condition, { _id: value['_id'] });
                    } else if (value['$in']) {
                        f.value(condition, value['$in'].map((r: any)=> {
                            return { _id: r['_id'] };
                        }));
                    }
                }
            }
        }
        this.condition = condition;
        if (this.dataSet) this.dataSet.reload();
        return this;
    }

    clause() {
    }

    dataset(): DataSet<TSchema> {
        if (!this.dataSet) {
            this.dataSet = new DataSet(this);
            this.dataSet.reload();
        }
        return this.dataSet;
    }

    limit(limit: number, offset?: number) {
        if (limit > 0) this._limit = limit;
        if (offset > 0) this._offset = offset;
        return this;
    }

    offset(value: number) {
        if (value > 0) this._offset = value;
        return this;
    }

    async notExists() {
        return !(await this.exists());
    }

    async exists() {
        return this.driver(async (drv) => {
            return drv.exists(this);
        });
    }

    async count() {
        return this.driver(async (drv) => {
            return drv.count(this);
        });
    }

    async one(id?: string | WhereCondition<TSchema>): Promise<Row<TSchema>> {
        this.limit(1, 0);
        return this.driver(async (drv) => {
            const rv = await drv.select(this);
            return rv ? rv.rows[0] : null;
        });
    }

    async array(): Promise<Driver.SelectResult<TSchema>> {
        return this.driver(async (drv) => {
            return await drv.select(this);
        });
    }

    async stream(onRow: (row: Row<TSchema>, index: number, totalCount: number) => boolean): Promise<number> {
        return this.driver(async (drv) => {
            const rv = await drv.select(this, onRow);
            return rv.total;
        });
    }

    get db() {
        return this.model.$.db;
    }

    protected driver<TResult>(cb: (drv: Driver<TSchema>) => Promise<TResult>): Promise<TResult> {
        return this.db.driver<TSchema, TResult>(cb);
    }

    static serialize(query: Query.Any): Query.Serialized {
        return {
            condition: query.condition,
            fields: query.fields,
            limit: query._limit,
            offset: query._offset,
            joins: query.joins.map(j => j.name) // !TODO
        }
    }

    static deserialize(query: Query.Serialized, model: Model.Any) {
        const rv = new Query(model, query.fields).where(query.condition);
        if (query.joins && query.joins.length > 0) {
            query.joins.forEach(fldName => rv.join({ $: rv.model.$.fields.get(fldName) as any }));
        }
        rv._limit = query.limit;
        rv._offset = query.offset;
        return rv;
    }

    private _offset = 0;
    private _limit = 0;
    private condition = {} as WhereCondition<TSchema>;
    private joins = [] as (field.One<any> | field.Many<any>)[];
    private dataSet: DataSet<TSchema>;
}

export namespace Query {
    export type Any = Query<any>
    export interface Serialized {
        condition: WhereCondition<any>
        fields: Field.Any[]
        joins: string[]
        limit: number
        offset: number
    }
}
