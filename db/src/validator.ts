import { Schema as DbSchema, Model, Row } from "."

export function Validator<TSchema extends DbSchema<TSchema>>(factory: Validator.Factory<TSchema>) {
    return factory;
}

export namespace Validator {
    export type Factory<TSchema extends DbSchema<TSchema>> = () => Validator.Schema<TSchema>

    export type Schema<TSchema extends DbSchema<TSchema>, TRow = Row<TSchema>> = {
        [P in keyof TRow]?:
        TRow[P] extends string ? CheckFunction<Checker.String<TSchema>> :
        TRow[P] extends Date ? CheckFunction<Checker.DateTime<TSchema>> :
        TRow[P] extends number ? CheckFunction<Checker.Number<TSchema>> :
        TRow[P] extends object ? Schema<TSchema, TRow[P]> :
        CheckFunction<Checker<TRow[P], TSchema>>
    }

    export class CheckResult {
        constructor(readonly text: string) {
        }
    }

    export type Result = (CheckResult | undefined | null)[]

    export type CheckFunction<TChecker extends Checker<any, any>> = (chk: TChecker) => Result | Promise<Result>

    export class Checker<TVal, TSchema extends DbSchema<TSchema>> {
        readonly value: TVal
        readonly row: Row<TSchema>
        readonly model: Model<TSchema>
    }

    export namespace Checker {
        export class String<TSchema extends DbSchema<TSchema>> extends Checker<string, TSchema> {
            regex(regex: RegExp, message?: string) {
                return new CheckResult("fuck");
            }

            size(min: number, max: number, message?: string) {
                return new CheckResult("fuck");
            }
        }
        export class Number<TSchema extends DbSchema<TSchema>> extends Checker<number, TSchema> {
            between(min: number, max: number, message?: string) {
                return new CheckResult("fuck");
            }
        }
        export class DateTime<TSchema extends DbSchema<TSchema>> extends Checker<Date, TSchema> {
            between(min: Date, max: Date, message?: string) {
                return new CheckResult("fuck");
            }
        }
    }
}
