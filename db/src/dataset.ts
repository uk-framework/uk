import { Query, Model, Schema } from "."
import { Log } from "@uk/log"

export class DataSet<TSchema extends Schema<TSchema>> {
    get first() { return this.data[0]; }
    get last() { return this[this.data.length - 1]; }
    readonly totalCount = 0;
    readonly loading = false;
    readonly error = undefined as Error;

    constructor(readonly query: Query<TSchema>) {
    }

    map<R>(cb: (el: DataSet.QueryRow<TSchema>, index: number, thiz: any) => R) {
        return this.data.map(cb);
    }

    reload() {
        if (this.loading) return;
        (<any>this)['loading'] = true;
        (<any>this)['error'] = undefined;
        this.log.debug("reload");
        setTimeout(async () => {
            try {
                const arr = await this.query.array();
                (<any>this)['totalCount'] = arr.total;
                this.data = arr.rows;
/*            } catch (err) {
                this.log.error(err);
                (<any>this)['error'] = err;*/
            } finally {
                (<any>this)['loading'] = false;
                this.log.debug("reload finally");
                if (this.onChangeHandler) this.onChangeHandler();
            }
        }, 0);
        return this;
    }

    get(index: number) {
        return this.data[index] || {};
    }

    onUpdate(handler: DataSet.UpdateHandler) {
        this.onChangeHandler = handler;
        return this;
    }

    private onChangeHandler: DataSet.UpdateHandler
    private data = [] as DataSet.QueryRow<TSchema>[];
    private log = new Log("UK-DB:DS-" + this.query.model.$.name.toUpperCase());
}

export module DataSet {
    export type QueryRow<TSchema extends Schema<TSchema>> = Model.Row<TSchema> & { $loading?: boolean }
    export type UpdateHandler = null | (() => void);
}
