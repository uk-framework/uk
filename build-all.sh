#!/bin/bash

set -e

function build {
    pushd $1
    rm -fr node_modules
    #ncu -u
    npm i
    npm i -D typescript@3.8
    npm run build
    popd
}

build tool
build log
build tr
build db
build api
build db-api
build db-mongo
build bot
build bot-telegram
build bot-viber
