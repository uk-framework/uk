import { LogItem } from "./types";
import { Logger } from "./logger";
import { Level } from "./types";
export declare function push(item: LogItem): void;
export declare function route(tag: RegExp, minLevel: Level, logger: Logger): void;
export declare function tagMinLevel(tag: string): Level;
