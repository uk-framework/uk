import { Level as LogLevel, Payload, HttpLogOptions } from "./types";
import { TryPayload } from "./trypayload";
export declare class Log {
    minLevel: LogLevel;
    readonly tag: string;
    instance: number;
    protected indexPayload: Payload;
    protected opts: Log.Options;
    constructor(tagOrOptions: string | Log.Options | Function | Log);
    index(): Payload;
    index(set: Payload): this;
    trace(msg: string, unindexed?: Payload, indexed?: Payload): void;
    debug(msg: string, unindexed?: Payload, indexed?: Payload): void;
    info(msg: string, unindexed?: Payload, indexed?: Payload): void;
    warn(msg: string, unindexed?: Payload, indexed?: Payload): void;
    error(msg: Error): void;
    error(msg: string, unindexed?: Payload, indexed?: Payload): void;
    fatal(msg: Error): void;
    fatal(msg: string, unindexed?: Payload, indexed?: Payload): void;
    append(level: LogLevel, msg: string, unindexed?: Payload, indexed?: Payload, error?: Error): void;
    try<T>(msg: string, tryfunc: TryFunc<T>): T;
    try<T>(level: LogLevel, msg: string, tryfunc: TryFunc<T>): T;
    now(): number;
    httpMiddleware(opts?: HttpLogOptions): (req: any, resp: any, next: () => void) => void;
}
declare type TryFunc<T> = (payload: TryPayload) => T;
export declare namespace Log {
    interface Options {
        tag: string;
        minLevel?: LogLevel;
    }
    const Level: typeof LogLevel;
    type Level = LogLevel;
}
export {};
