"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Level;
(function (Level) {
    Level[Level["TRACE"] = 10] = "TRACE";
    Level[Level["DEBUG"] = 20] = "DEBUG";
    Level[Level["INFO"] = 30] = "INFO";
    Level[Level["WARN"] = 40] = "WARN";
    Level[Level["ERROR"] = 50] = "ERROR";
    Level[Level["FATAL"] = 60] = "FATAL";
})(Level = exports.Level || (exports.Level = {}));
//# sourceMappingURL=types.js.map