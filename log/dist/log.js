"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("./types");
const router = require("./router");
const trypayload_1 = require("./trypayload");
const instances = {};
class Log {
    constructor(tagOrOptions) {
        if (typeof tagOrOptions === 'string') {
            this.tag = tagOrOptions;
        }
        else if (typeof tagOrOptions === "function") {
            this.tag = tagOrOptions.name.toUpperCase();
        }
        else if (tagOrOptions instanceof Log) {
            this.tag = tagOrOptions.tag;
            this.opts = tagOrOptions.opts;
        }
        else {
            this.tag = tagOrOptions.tag;
            this.opts = tagOrOptions;
        }
        this.tag = this.tag.endsWith(".ts") || this.tag.endsWith(".js")
            ? this.tag.substring(this.tag.lastIndexOf('/') + 1).slice(0, -3).toUpperCase()
            : this.tag;
        this.minLevel = router.tagMinLevel(this.tag);
        this.instance = instances[this.tag] || 0;
        instances[this.tag] = this.instance + 1;
    }
    index(set) {
        if (set === undefined)
            return this.indexPayload;
        this.indexPayload = set;
        return this;
    }
    trace(msg, unindexed, indexed) {
        this.append(types_1.Level.TRACE, msg, unindexed, indexed);
    }
    debug(msg, unindexed, indexed) {
        this.append(types_1.Level.DEBUG, msg, unindexed, indexed);
    }
    info(msg, unindexed, indexed) {
        this.append(types_1.Level.INFO, msg, unindexed, indexed);
    }
    warn(msg, unindexed, indexed) {
        this.append(types_1.Level.WARN, msg, unindexed, indexed);
    }
    error(msg, unindexed, indexed) {
        if (msg instanceof Error) {
            this.append(types_1.Level.ERROR, msg.message, unindexed, indexed, msg);
        }
        else {
            this.append(types_1.Level.ERROR, msg, unindexed, indexed);
        }
    }
    fatal(msg, unindexed, indexed) {
        if (msg instanceof Error) {
            this.append(types_1.Level.ERROR, msg.message, unindexed, indexed, msg);
        }
        else {
            this.append(types_1.Level.ERROR, msg, unindexed, indexed);
        }
    }
    append(level, msg, unindexed, indexed, error) {
        if (this.minLevel && level >= this.minLevel) {
            router.push({
                d: new Date,
                l: level,
                m: msg,
                e: error,
                u: unindexed,
                p: this.indexPayload ? Object.assign(Object.assign({}, this.indexPayload), { indexed }) : indexed,
                t: this.tag,
                i: this.instance
            });
        }
    }
    try(levelOrMsg, msgOrFunc, tryfunc) {
        let func;
        let msg;
        let level = types_1.Level.DEBUG;
        switch (typeof levelOrMsg) {
            case 'string':
                msg = levelOrMsg;
                func = msgOrFunc;
                break;
            case 'number':
                level = levelOrMsg;
                msg = msgOrFunc;
                func = tryfunc;
                break;
        }
        const start = this.now();
        let rv;
        const payload = new trypayload_1.TryPayload();
        try {
            rv = func(payload);
        }
        catch (err) {
            this.append(types_1.Level.ERROR, msg, payload['uError'], Object.assign(Object.assign({}, payload['iError']), { execTime: this.now() - start }), err);
            throw err;
        }
        const execTime = this.now() - start;
        if (rv instanceof Promise) {
            rv = rv.then((val) => {
                this.append(level, msg, payload['uSuccess'], Object.assign(Object.assign({}, payload['iSuccess']), { execTime, asyncTime: this.now() - start }));
                return val;
            }, (err) => {
                this.append(types_1.Level.ERROR, msg, payload['uError'], Object.assign(Object.assign({}, payload['iError']), { execTime, asyncTime: this.now() - start }), err);
                return err;
            });
        }
        else {
            this.append(level, msg, payload['uSuccess'], Object.assign(Object.assign({}, payload['iSuccess']), { execTime }));
        }
        return rv;
    }
    now() {
        if (process && process.hrtime) {
            const hr = process.hrtime();
            return hr[0] * 1000 + hr[1] / 1000000;
        }
        else {
            return Date.now();
        }
    }
    httpMiddleware(opts) {
        const prod = process.env.NODE_ENV === 'production';
        opts = opts || {
            level: types_1.Level.INFO,
            body: !prod,
            params: !prod,
        };
        opts.level = opts.level || types_1.Level.INFO;
        return (req, resp, next) => {
            const time = Date.now();
            resp.on('finish', () => {
                let level = opts.level;
                if (resp.status >= 500) {
                    level = types_1.Level.ERROR;
                }
                else if (resp.status >= 400) {
                    level = types_1.Level.WARN;
                }
                this.append(level, `${req.method} ${req.originalUrl} => ${resp.statusCode}`, {
                    time: Date.now() - time,
                    from: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                    fresh: opts.fresh ? req.fresh : undefined,
                    body: opts.body ? req.body : undefined,
                    cookies: opts.cookies ? req.cookies : undefined,
                    params: opts.params ? req.params : undefined,
                    xhr: opts.xhr ? req.xhr : undefined,
                });
            });
            next();
        };
    }
}
exports.Log = Log;
(function (Log) {
    Log.Level = types_1.Level;
})(Log = exports.Log || (exports.Log = {}));
//# sourceMappingURL=log.js.map