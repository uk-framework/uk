export interface LogItem {
    d: Date;
    t: string;
    l: Level;
    e: Error;
    m: string;
    p: Payload;
    u: Payload;
    i: number;
}
export declare enum Level {
    TRACE = 10,
    DEBUG = 20,
    INFO = 30,
    WARN = 40,
    ERROR = 50,
    FATAL = 60
}
export declare type Payload = object;
export interface HttpLogOptions {
    level?: Level;
    body?: boolean;
    cookies?: boolean;
    fresh?: boolean;
    params?: boolean;
    xhr?: boolean;
}
