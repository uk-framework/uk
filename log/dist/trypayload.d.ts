export declare class TryPayload {
    private iError;
    private uError;
    private iSuccess;
    private uSuccess;
    error(unindexed: object | null | undefined, indexed?: object): this;
    success(unindexed: object | null | undefined, indexed?: object): this;
    finally(unindexed: object | null | undefined, indexed?: object): this;
}
