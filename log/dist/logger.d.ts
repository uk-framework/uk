import { LogItem } from "./types";
export declare type Logger = Logger.Json | Logger.Item;
export declare namespace Logger {
    interface Item {
        appendItem(item: LogItem): void;
    }
    interface Json {
        appendJson(item: string): void;
    }
}
