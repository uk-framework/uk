"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const readline = require("readline");
const types_1 = require("../types");
const moment = require("moment");
const util = require("util");
const chalk_1 = require("chalk");
const tool_1 = require("@uk/tool");
let maxTag = 0;
const tagOffset = {};
const rl = readline.createInterface(process.stdin);
const config = tool_1.envConfig({
    ARRAY_DEPTH: 3,
    OBJ_DEPTH: 3,
    COLOR: true,
    STRING_WRAP_LEN: 80,
    TIME_FORMAT: "HH:mm:ss.SSS",
});
rl.on("line", line => {
    let item;
    try {
        item = JSON.parse(line);
    }
    catch (_a) {
        writeItem({
            d: new Date(),
            i: 0,
            l: types_1.Level.INFO,
            m: line,
            p: null,
            e: undefined,
            u: undefined,
            t: "STDOUT",
        });
        return;
    }
    if (item && item.d && item.l && typeof item.m === 'string' && item.t) {
        writeItem(item);
    }
});
function writeItem(item) {
    if (!(item.t in tagOffset)) {
        tagOffset[item.t] = "";
        if (item.t.length > maxTag)
            maxTag = item.t.length;
        for (const tag in tagOffset) {
            let off = "  ";
            let len = tag.length;
            while (len++ < maxTag)
                off += " ";
            tagOffset[tag] = off;
        }
    }
    let err = "";
    if (item.e) {
        err = chalk_1.default.red(item.e.name + ': ' + item.e.message) + '\n' + chalk_1.default.gray(item.e.stack) + '\n';
    }
    let payload = "";
    if (item.p) {
        payload = chalk_1.default.gray(inspect(item.p));
    }
    if (item.u) {
        if (payload)
            payload += '\n';
        payload += chalk_1.default.gray(inspect(item.u));
    }
    console.log(chalk_1.default.white(moment(item.d).format(config.TIME_FORMAT) + "  ") +
        levelColor(item.l) +
        "\t" +
        tagColor(item.t, item.i) +
        chalk_1.default.white(item.m) +
        "\t" +
        err +
        payload);
}
function inspect(obj) {
    if (obj && typeof obj['execTime'] === 'number') {
        obj['execTime'] = obj['execTime'].toFixed(3);
    }
    if (obj && typeof obj['asyncTime'] === 'number') {
        obj['asyncTime'] = obj['asyncTime'].toFixed(3);
    }
    let rv = util.inspect(obj, {
        maxArrayLength: config.ARRAY_DEPTH,
        depth: config.OBJ_DEPTH,
        colors: config.COLOR
    });
    if (rv.length > config.STRING_WRAP_LEN) {
        rv = "\n" + rv;
    }
    return rv;
}
function tagColor(tag, instance) {
    const inst = instance ? "/" + instance : "";
    return tag + inst + tagOffset[tag].substr(inst.length);
}
function levelColor(level) {
    const lvl = types_1.Level[level];
    switch (level) {
        case types_1.Level.TRACE:
            return chalk_1.default.gray(lvl);
        case types_1.Level.INFO:
            return chalk_1.default.yellow(lvl);
        case types_1.Level.WARN:
            return chalk_1.default.red(lvl);
        case types_1.Level.ERROR:
        case types_1.Level.FATAL:
            return chalk_1.default.bgRed(lvl);
        default:
            return chalk_1.default.white(lvl);
    }
}
//# sourceMappingURL=reader.js.map