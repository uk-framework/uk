import { Logger } from "../logger";
import { LogItem } from "../types";
export declare class FluentdLogger implements Logger.Item {
    appendItem(item: LogItem): Promise<any>;
}
