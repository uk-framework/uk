"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class StreamLogger {
    constructor(stream) {
        this.stream = stream;
    }
    appendJson(item) {
        this.stream.write(item);
        this.stream.write('\n');
    }
}
exports.StreamLogger = StreamLogger;
//# sourceMappingURL=stream.js.map