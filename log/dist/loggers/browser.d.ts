import { Logger } from "../logger";
import { LogItem } from "../types";
export declare class BrowserLogger implements Logger.Item {
    constructor();
    appendItem(item: LogItem): void;
}
