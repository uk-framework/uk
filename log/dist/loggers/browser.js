"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const colorMap = new Map();
class BrowserLogger {
    constructor() {
    }
    appendItem(item) {
        let func = console.log;
        let msgcolor = "";
        switch (item.l) {
            case types_1.Level.TRACE:
                msgcolor = "color: rgba(0, 0, 0, 0.1)";
                func = console.log;
                break;
            case types_1.Level.INFO:
                func = console.info;
                break;
            case types_1.Level.WARN:
                func = console.warn;
                break;
            case types_1.Level.ERROR:
            case types_1.Level.FATAL:
                func = console.error;
                break;
        }
        let tagcolor = colorMap.get(item.t);
        if (!tagcolor) {
            tagcolor = `color: hsla(${Math.random() * 360}, 100%, 50%, ${item.l === types_1.Level.TRACE ? 0.2 : 1})`;
            colorMap.set(item.t, tagcolor);
        }
        if (item.e) {
            console.error(item.e, { indexed: item.p, unindexed: item.u });
        }
        else {
            const args = [`%c${item.t} %c${item.m}`, tagcolor, msgcolor];
            if (item.u)
                args.push(item.u);
            if (item.p)
                args.push(item.p);
            func.apply(console, args);
        }
    }
}
exports.BrowserLogger = BrowserLogger;
//# sourceMappingURL=browser.js.map