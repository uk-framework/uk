import { Logger } from "../logger";
export interface WritableStream {
    write(data: string, encoding?: string, cb?: () => void): boolean;
    on(ev: 'drain', cb: () => void): void;
}
export declare class StreamLogger implements Logger.Json {
    protected stream: WritableStream;
    constructor(stream: WritableStream);
    appendJson(item: string): void;
}
