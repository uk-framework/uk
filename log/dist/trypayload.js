"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TryPayload {
    error(unindexed, indexed) {
        if (indexed) {
            this.iError = Object.assign(this.iError || {}, indexed);
        }
        if (unindexed) {
            this.uError = Object.assign(this.uError || {}, unindexed);
        }
        return this;
    }
    success(unindexed, indexed) {
        if (indexed) {
            this.iSuccess = Object.assign(this.iSuccess || {}, indexed);
        }
        if (unindexed) {
            this.uSuccess = Object.assign(this.uSuccess || {}, unindexed);
        }
        return this;
    }
    finally(unindexed, indexed) {
        if (indexed) {
            this.iSuccess = Object.assign(this.iSuccess || {}, indexed);
            this.iError = Object.assign(this.iError || {}, indexed);
        }
        if (unindexed) {
            this.uError = Object.assign(this.uError || {}, unindexed);
            this.uSuccess = Object.assign(this.uSuccess || {}, unindexed);
        }
        return this;
    }
}
exports.TryPayload = TryPayload;
//# sourceMappingURL=trypayload.js.map