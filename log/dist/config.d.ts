export declare const config: {
    FLUENTD_LOGGER_IP: string;
    FLUENTD_LOGGER_PORT: string;
};
