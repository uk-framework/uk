"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tool_1 = require("@uk/tool");
const stream_1 = require("./loggers/stream");
const browser_1 = require("./loggers/browser");
const types_1 = require("./types");
const routes = [];
function push(item) {
    if (item.e) {
        item.e = {
            message: item.e.message,
            name: item.e.name,
            stack: item.e.stack ? item.e.stack.toString() : "No stacktrace"
        };
    }
    const json = tool_1.Json.Safe.stringify(item);
    for (const route of routes) {
        if (item.l >= route.minLevel && route.tag.test(item.t)) {
            if (route.json)
                route.json.appendJson(json);
            else if (route.item)
                route.item.appendItem(item);
        }
    }
}
exports.push = push;
function route(tag, minLevel, logger) {
    routes.push({
        tag: tag,
        minLevel: minLevel,
        json: 'appendJson' in logger ? logger : undefined,
        item: 'appendItem' in logger ? logger : undefined,
    });
}
exports.route = route;
function tagMinLevel(tag) {
    return types_1.Level.TRACE;
}
exports.tagMinLevel = tagMinLevel;
if (tool_1.isNode) {
    route(/.*/, types_1.Level.TRACE, new stream_1.StreamLogger(process.stdout));
}
else {
    route(/.*/, types_1.Level.TRACE, new browser_1.BrowserLogger());
}
//# sourceMappingURL=router.js.map