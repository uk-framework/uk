import { LogItem } from "./types"
import { Logger } from "./logger"
import { Json, isNode } from "@uk/tool"
import { StreamLogger } from "./loggers/stream"
import { BrowserLogger } from "./loggers/browser"
import { Level } from "./types"

const routes = [] as {
    tag: RegExp,
    minLevel: Level
    json?: Logger.Json
    item?: Logger.Item
}[];

export function push(item: LogItem) {
    if (item.e) {
        item.e = {
            message: item.e.message,
            name: item.e.name,
            stack: item.e.stack ? item.e.stack.toString() : "No stacktrace"
        }
    }
    const json = Json.Safe.stringify(item);
    for (const route of routes) {
        if (item.l >= route.minLevel && route.tag.test(item.t)) {
            if (route.json) route.json.appendJson(json);
            else if (route.item) route.item.appendItem(item);
        }
    }
}

export function route(tag: RegExp, minLevel: Level, logger: Logger) {
    routes.push({
        tag: tag,
        minLevel: minLevel,
        json: 'appendJson' in logger ? logger : undefined,
        item: 'appendItem' in logger ? logger : undefined,
    });
}

export function tagMinLevel(tag: string): Level {
    return Level.TRACE;
}

if (isNode) {
    route(/.*/, Level.TRACE, new StreamLogger(process.stdout));
} else {
    route(/.*/, Level.TRACE, new BrowserLogger());
}
