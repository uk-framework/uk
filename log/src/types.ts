export interface LogItem {
    /** Event time */
    d: Date
    /** Tag */
    t: string
    /** Level */
    l: Level
    /** Error */
    e: Error
    /** Message */
    m: string
    /** Indexed payload */
    p: Payload
    /** Unindexed payload */
    u: Payload
    /** Instance number */
    i: number
}

export enum Level {
    TRACE = 10,
    DEBUG = 20,
    INFO = 30,
    WARN = 40,
    ERROR = 50,
    FATAL = 60,
}

export type Payload = object;

export interface HttpLogOptions {
    level?: Level
    body?: boolean
    cookies?: boolean
    fresh?: boolean
    params?: boolean
    xhr?: boolean
}
