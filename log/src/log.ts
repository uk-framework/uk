import { Level as LogLevel, Payload, HttpLogOptions } from "./types"
import * as router from "./router"
import { TryPayload } from "./trypayload"

const instances = {} as {
    [tag: string]: number
}

export class Log {
    minLevel: LogLevel
    readonly tag: string
    instance: number

    protected indexPayload: Payload
    protected opts: Log.Options

    constructor(tagOrOptions: string | Log.Options | Function | Log) {
        if (typeof tagOrOptions === 'string') {
            this.tag = tagOrOptions;
        } else if (typeof tagOrOptions === "function") {
            this.tag = tagOrOptions.name.toUpperCase();
        } else if (tagOrOptions instanceof Log) {
            this.tag = tagOrOptions.tag;
            this.opts = tagOrOptions.opts;
        } else {
            this.tag = tagOrOptions.tag;
            this.opts = tagOrOptions;
        }
        this.tag = this.tag.endsWith(".ts") || this.tag.endsWith(".js")
            ? this.tag.substring(this.tag.lastIndexOf('/') + 1).slice(0, -3).toUpperCase()
            : this.tag;
        this.minLevel = router.tagMinLevel(this.tag);
        this.instance = instances[this.tag] || 0;
        instances[this.tag] = this.instance + 1;
    }

    index(): Payload
    index(set: Payload): this;
    index(set?: Payload): this | Payload {
        if (set === undefined) return this.indexPayload;
        this.indexPayload = set;
        return this;
    }

    trace(msg: string, unindexed?: Payload, indexed?: Payload) {
        this.append(LogLevel.TRACE, msg, unindexed, indexed);
    }

    debug(msg: string, unindexed?: Payload, indexed?: Payload) {
        this.append(LogLevel.DEBUG, msg, unindexed, indexed);
    }

    info(msg: string, unindexed?: Payload, indexed?: Payload) {
        this.append(LogLevel.INFO, msg, unindexed, indexed);
    }

    warn(msg: string, unindexed?: Payload, indexed?: Payload) {
        this.append(LogLevel.WARN, msg, unindexed, indexed);
    }

    error(msg: Error): void;
    error(msg: string, unindexed?: Payload, indexed?: Payload): void;
    error(msg: string | Error, unindexed?: Payload, indexed?: Payload): void {
        if (msg instanceof Error) {
            this.append(LogLevel.ERROR, msg.message, unindexed, indexed, msg);
        } else {
            this.append(LogLevel.ERROR, msg, unindexed, indexed);
        }
    }

    fatal(msg: Error): void;
    fatal(msg: string, unindexed?: Payload, indexed?: Payload): void;
    fatal(msg: string | Error, unindexed?: Payload, indexed?: Payload): void {
        if (msg instanceof Error) {
            this.append(LogLevel.ERROR, msg.message, unindexed, indexed, msg);
        } else {
            this.append(LogLevel.ERROR, msg, unindexed, indexed);
        }
    }

    append(level: LogLevel, msg: string, unindexed?: Payload, indexed?: Payload, error?: Error) {
        if (this.minLevel && level >= this.minLevel) {
            router.push({
                d: new Date,
                l: level,
                m: msg,
                e: error,
                u: unindexed,
                p: this.indexPayload ? { ...this.indexPayload, indexed } : indexed,
                t: this.tag,
                i: this.instance
            });
        }
    }

    try<T>(msg: string, tryfunc: TryFunc<T>): T;
    try<T>(level: LogLevel, msg: string, tryfunc: TryFunc<T>): T;
    try<T>(levelOrMsg: LogLevel | string, msgOrFunc?: string | TryFunc<T>, tryfunc?: TryFunc<T>): T {
        let func: TryFunc<T>;
        let msg: string;
        let level = LogLevel.DEBUG;
        switch (typeof levelOrMsg) {
            case 'string':
                msg = levelOrMsg;
                func = msgOrFunc as TryFunc<T>;
                break;
            case 'number':
                level = levelOrMsg;
                msg = msgOrFunc as string;
                func = tryfunc as TryFunc<T>;
                break;
        }
        const start = this.now();
        let rv;
        const payload = new TryPayload();
        try {
            rv = func(payload);
        } catch (err) {
            this.append(LogLevel.ERROR, msg, payload['uError'], {
                ...payload['iError'],
                execTime: this.now() - start,
            }, err);
            throw err;
        }
        const execTime = this.now() - start;
        if (rv instanceof Promise) {
            rv = rv.then((val) => {
                this.append(level, msg, payload['uSuccess'], {
                    ...payload['iSuccess'],
                    execTime,
                    asyncTime: this.now() - start,
                });
                return val;
            }, (err) => {
                this.append(LogLevel.ERROR, msg, payload['uError'], {
                    ...payload['iError'],
                    execTime,
                    asyncTime: this.now() - start,
                }, err);
                return err;
            });
        } else {
            this.append(level, msg, payload['uSuccess'], {
                ...payload['iSuccess'],
                execTime,
            });
        }
        return rv as T;
    }

    now() {
        if (process && process.hrtime) {
            const hr = process.hrtime();
            return hr[0] * 1000 + hr[1] / 1000000;
        } else {
            return Date.now();
        }
    }

    httpMiddleware(opts?: HttpLogOptions) {
        const prod = process.env.NODE_ENV === 'production';
        opts = opts || {
            level: LogLevel.INFO,
            body: !prod,
            params: !prod,
        };
        opts.level = opts.level || LogLevel.INFO;
        return (req: any, resp: any, next: () => void) => {
            const time = Date.now();
            resp.on('finish', () => {
                let level = opts.level;
                if (resp.status >= 500) {
                    level = LogLevel.ERROR;
                } else if (resp.status >= 400) {
                    level = LogLevel.WARN;
                }
                this.append(level, `${req.method} ${req.originalUrl} => ${resp.statusCode}`, {
                    time: Date.now() - time,
                    from: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
                    fresh: opts.fresh ? req.fresh : undefined,
                    body: opts.body ? req.body : undefined,
                    cookies: opts.cookies ? req.cookies : undefined,
                    params: opts.params ? req.params : undefined,
                    xhr: opts.xhr ? req.xhr : undefined,
                });
            });
            next();
        }
    }
}

type TryFunc<T> = (payload: TryPayload) => T;

export namespace Log {
    export interface Options {
        tag: string
        minLevel?: LogLevel
    }
    export const Level = LogLevel;
    export type Level = LogLevel;
}
