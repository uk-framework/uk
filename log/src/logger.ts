import { LogItem } from "./types"

export type Logger = Logger.Json|Logger.Item;
export namespace Logger {
    export interface Item {
        appendItem(item: LogItem): void
    }
    export interface Json {
        appendJson(item: string): void
    }
}
