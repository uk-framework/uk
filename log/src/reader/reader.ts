import * as readline from "readline"
import { LogItem, Level } from "../types"
import * as moment from "moment"
import * as util from "util"
import chalk from "chalk"
import { envConfig } from "@uk/tool"

let maxTag = 0;
const tagOffset = {} as {
    [tag: string]: string
}

const rl = readline.createInterface(process.stdin);
const config = envConfig({
    ARRAY_DEPTH: 3,
    OBJ_DEPTH: 3,
    COLOR: true,
    STRING_WRAP_LEN: 80,
    TIME_FORMAT: "HH:mm:ss.SSS",
});

rl.on("line", line => {
    let item: LogItem;
    try {
        item = JSON.parse(line);
    } catch {
        writeItem({
            d: new Date(),
            i: 0,
            l: Level.INFO,
            m: line,
            p: null,
            e: undefined,
            u: undefined,
            t: "STDOUT",
        });
        return;
    }
    if (item && item.d && item.l && typeof item.m === 'string' && item.t) {
        writeItem(item);
    }
});

function writeItem(item: LogItem) {
    if (!(item.t in tagOffset)) {
        tagOffset[item.t] = "";
        if (item.t.length > maxTag) maxTag = item.t.length;
        for (const tag in tagOffset) {
            let off = "  ";
            let len = tag.length;
            while (len++ < maxTag) off += " ";
            tagOffset[tag] = off;
        }
    }
    let err = "";
    if (item.e) {
        err = chalk.red(item.e.name + ': ' + item.e.message) + '\n' + chalk.gray(item.e.stack) + '\n';
    }
    let payload = "";
    if (item.p) {
        payload = chalk.gray(inspect(item.p));
    }
    if (item.u) {
        if (payload) payload += '\n';
        payload += chalk.gray(inspect(item.u));
    }

    console.log(
        chalk.white(moment(item.d).format(config.TIME_FORMAT) + "  ") +
        levelColor(item.l) +
        "\t" +
        tagColor(item.t, item.i) +
        chalk.white(item.m) +
        "\t" +
        err +
        payload);
}

function inspect(obj: object) {
    if (obj && typeof obj['execTime'] === 'number') {
        obj['execTime'] = obj['execTime'].toFixed(3);
    }
    if (obj && typeof obj['asyncTime'] === 'number') {
        obj['asyncTime'] = obj['asyncTime'].toFixed(3);
    }
    let rv = util.inspect(obj, {
        maxArrayLength: config.ARRAY_DEPTH,
        depth: config.OBJ_DEPTH,
        colors: config.COLOR
    });
    if (rv.length > config.STRING_WRAP_LEN) {
        rv = "\n" + rv;
    }
    return rv;
}

function tagColor(tag: string, instance: number) {
    const inst = instance ? "/" + instance : "";
    return tag + inst + tagOffset[tag].substr(inst.length);
}

function levelColor(level: Level) {
    const lvl = Level[level];
    switch (level) {
        case Level.TRACE:
            return chalk.gray(lvl);
        case Level.INFO:
            return chalk.yellow(lvl);
        case Level.WARN:
            return chalk.red(lvl);
        case Level.ERROR:
        case Level.FATAL:
            return chalk.bgRed(lvl);
        default:
            return chalk.white(lvl);
    }
}
