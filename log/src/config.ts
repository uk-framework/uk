import { envConfig } from "@uk/tool"

export const config = envConfig({
    FLUENTD_LOGGER_IP: '',
    FLUENTD_LOGGER_PORT: ''
});
