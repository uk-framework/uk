import { Logger } from "../logger"
import { LogItem, Level } from "../types"

const colorMap = new Map<string, string>();

export class BrowserLogger implements Logger.Item {
    constructor() {
    }

    appendItem(item: LogItem) {
        let func = console.log;
        let msgcolor = "";
        switch (item.l) {
            case Level.TRACE:
                msgcolor = "color: rgba(0, 0, 0, 0.1)";
                func = console.log;
                break;
            case Level.INFO:
                func = console.info;
                break;
            case Level.WARN:
                func = console.warn;
                break;
            case Level.ERROR:
            case Level.FATAL:
                func = console.error;
                break;
        }
        let tagcolor = colorMap.get(item.t);
        if (!tagcolor) {
            tagcolor = `color: hsla(${Math.random() * 360}, 100%, 50%, ${item.l === Level.TRACE ? 0.2 : 1})`;
            colorMap.set(item.t, tagcolor);
        }
        if (item.e) {
            console.error(item.e, { indexed: item.p, unindexed: item.u });
        } else {
            const args: any[] = [`%c${item.t} %c${item.m}`, tagcolor, msgcolor];
            if (item.u) args.push(item.u);
            if (item.p) args.push(item.p);
            func.apply(console, args);
        }
    }
}
