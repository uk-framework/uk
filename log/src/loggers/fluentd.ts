import { Logger } from "../logger"
import { LogItem } from "../types"
import * as prequest from "request-promise"
import { config } from "../config"

export class FluentdLogger implements Logger.Item {
    async appendItem(item: LogItem) {
        return prequest.post(`http://${config.FLUENTD_LOGGER_IP}:${config.FLUENTD_LOGGER_PORT}/app.log`, {
            json: true,
            body: {
                logMessage: item.m,
                logPayload: item.p,
                logTag: item.t,
                logLevel: item.l
            }
        }).catch(err=> console.log('Fluentd call failed', err));
    }
}
