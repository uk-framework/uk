import { Logger } from "../logger"

export interface WritableStream {
    write(data: string, encoding?: string, cb?: () => void): boolean
    on(ev: 'drain', cb: () => void): void
}

export class StreamLogger implements Logger.Json {
    constructor(protected stream: WritableStream) {
    }

    appendJson(item: string) {
        this.stream.write(item);
        this.stream.write('\n');
    }
}
