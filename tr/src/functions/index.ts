export type TranslateFunction<TArg> = TArg extends void ? ()=> string  : (p: TArg)=> string;
// export type TranslateFunction<TArg> = TArg extends void ? string : (p: TArg)=> string;

export class Tr<TLocale extends string> {
    constructor(protected locale: TLocale) {
    }

    protected tr<TArg = void>(translates: { [P in TLocale]: TranslateFunction<TArg> }): TranslateFunction<TArg> {
        return translates[this.locale];
    }

    protected plural(num: number, variant: string, ...variants: string[]) {
        variants.splice(0, 0, variant);
        if (num >= variants.length) num = variants.length - 1;
        while (num >= 0) {
            if (variants[num]) return variants[num];
            num--;
        }
        return variant;
    }
}
