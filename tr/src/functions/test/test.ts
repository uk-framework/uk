import { Tr } from ".."

export const Locale = {
    "uk": "Ukrainian",
    "en": "English",
}
export type Locale = keyof typeof Locale;

export class Apples extends Tr<Locale> {
    // welcomeApple = this.tr({
    //     uk: `Ласкаво просимо до магазину Apple`,
    //     en: `Welcome Apple Store`,
    // });
    giveMeApple = this.tr<{ color: string, count: number }>({
        uk: p=> `Дайте мені ${p.count} ${p.color} ${this.apples(p.count)}`,
        en: p=> `Give me an ${p.count} ${p.color} apples`,
    });
    returnApple = this.tr<number>({
        uk: p=> `Поверніть будь-ласка ${p} ${this.apples(p)}`,
        en: p=> `Please return ${p} apples`
    });
    red = this.tr<number>({
        uk: p=> `${this.plural(p, "червоний", "червоних")}`,
        en: p=> `red`,
    });
    apples = this.tr<number>({
        uk: p=> `${this.plural(p, "яблук", "яблуко", "яблука", "", "", "яблук")}`,
        en: p=> `${this.plural(p, "apples", "apple", "apples")}`,
    })
    // test = this.tr({
    //     uk: `ТЕСТ`,
    //     en: 'TEST'
    // })
    test2 = this.tr<number>({
        uk: i=>`ТЕСТ ${i}`,
        en: i=>`TEST ${i}`
    })
}

export const texts = {
    apples: new Apples("uk"),
}

function assert(str1: string, str2: string) {
    if (str1 === str2) {
        console.log(`SUCCESS "${str1} === ${str2}"`);
    } else {
        console.error(`FAILURE "${str1} !== ${str2}"`);
    }
}

// assert(texts.apples.welcomeApple, "Ласкаво просимо до магазину Apple");
assert(texts.apples.giveMeApple({
    color: texts.apples.red(4),
    count: 4
}), "Дайте мені 4 червоних яблука");
assert(texts.apples.returnApple(0), "Поверніть будь-ласка 0 яблук");
assert(texts.apples.returnApple(1), "Поверніть будь-ласка 1 яблуко");
assert(texts.apples.returnApple(2), "Поверніть будь-ласка 2 яблука");
assert(texts.apples.returnApple(3), "Поверніть будь-ласка 3 яблука");
assert(texts.apples.returnApple(4), "Поверніть будь-ласка 4 яблука");
assert(texts.apples.returnApple(5), "Поверніть будь-ласка 5 яблук");
assert(texts.apples.returnApple(6), "Поверніть будь-ласка 6 яблук");
assert(texts.apples.returnApple(15), "Поверніть будь-ласка 15 яблук");
// assert(texts.apples.test, "ТЕСТ");
assert(texts.apples.test2(1), "ТЕСТ 1");
