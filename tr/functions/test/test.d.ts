import { Tr } from "..";
export declare const Locale: {
    uk: string;
    en: string;
};
export declare type Locale = keyof typeof Locale;
export declare class Apples extends Tr<Locale> {
    giveMeApple: (p: {
        color: string;
        count: number;
    }) => string;
    returnApple: (p: number) => string;
    red: (p: number) => string;
    apples: (p: number) => string;
    test2: (p: number) => string;
}
export declare const texts: {
    apples: Apples;
};
