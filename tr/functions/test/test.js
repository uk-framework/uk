"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const __1 = require("..");
exports.Locale = {
    "uk": "Ukrainian",
    "en": "English",
};
class Apples extends __1.Tr {
    constructor() {
        super(...arguments);
        this.giveMeApple = this.tr({
            uk: p => `Дайте мені ${p.count} ${p.color} ${this.apples(p.count)}`,
            en: p => `Give me an ${p.count} ${p.color} apples`,
        });
        this.returnApple = this.tr({
            uk: p => `Поверніть будь-ласка ${p} ${this.apples(p)}`,
            en: p => `Please return ${p} apples`
        });
        this.red = this.tr({
            uk: p => `${this.plural(p, "червоний", "червоних")}`,
            en: p => `red`,
        });
        this.apples = this.tr({
            uk: p => `${this.plural(p, "яблук", "яблуко", "яблука", "", "", "яблук")}`,
            en: p => `${this.plural(p, "apples", "apple", "apples")}`,
        });
        this.test2 = this.tr({
            uk: i => `ТЕСТ ${i}`,
            en: i => `TEST ${i}`
        });
    }
}
exports.Apples = Apples;
exports.texts = {
    apples: new Apples("uk"),
};
function assert(str1, str2) {
    if (str1 === str2) {
        console.log(`SUCCESS "${str1} === ${str2}"`);
    }
    else {
        console.error(`FAILURE "${str1} !== ${str2}"`);
    }
}
assert(exports.texts.apples.giveMeApple({
    color: exports.texts.apples.red(4),
    count: 4
}), "Дайте мені 4 червоних яблука");
assert(exports.texts.apples.returnApple(0), "Поверніть будь-ласка 0 яблук");
assert(exports.texts.apples.returnApple(1), "Поверніть будь-ласка 1 яблуко");
assert(exports.texts.apples.returnApple(2), "Поверніть будь-ласка 2 яблука");
assert(exports.texts.apples.returnApple(3), "Поверніть будь-ласка 3 яблука");
assert(exports.texts.apples.returnApple(4), "Поверніть будь-ласка 4 яблука");
assert(exports.texts.apples.returnApple(5), "Поверніть будь-ласка 5 яблук");
assert(exports.texts.apples.returnApple(6), "Поверніть будь-ласка 6 яблук");
assert(exports.texts.apples.returnApple(15), "Поверніть будь-ласка 15 яблук");
assert(exports.texts.apples.test2(1), "ТЕСТ 1");
//# sourceMappingURL=test.js.map