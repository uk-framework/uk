"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Tr {
    constructor(locale) {
        this.locale = locale;
    }
    tr(translates) {
        return translates[this.locale];
    }
    plural(num, variant, ...variants) {
        variants.splice(0, 0, variant);
        if (num >= variants.length)
            num = variants.length - 1;
        while (num >= 0) {
            if (variants[num])
                return variants[num];
            num--;
        }
        return variant;
    }
}
exports.Tr = Tr;
//# sourceMappingURL=index.js.map