export declare type TranslateFunction<TArg> = TArg extends void ? () => string : (p: TArg) => string;
export declare class Tr<TLocale extends string> {
    protected locale: TLocale;
    constructor(locale: TLocale);
    protected tr<TArg = void>(translates: {
        [P in TLocale]: TranslateFunction<TArg>;
    }): TranslateFunction<TArg>;
    protected plural(num: number, variant: string, ...variants: string[]): string;
}
