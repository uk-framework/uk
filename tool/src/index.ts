export const isNode = (typeof window === 'undefined');
export const isBrowser = !isNode;

export { envConfig } from "./envconfig"
export { route } from "./route"
export { BinBuffer } from "./binbuffer"
export { PromiseQueue } from "./promisequeue"
export { Constructor } from "./constructor"
export { Json } from "./json"
export { ActionEvent } from "./actionevent"
export { parsePath } from './parsepath'

type Writeable<T> = { -readonly [P in keyof T]: T[P] };

export function noReadOnly<T>(obj: T) {
    return obj as Writeable<T>
}
