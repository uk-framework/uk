export namespace Constructor {
    export type AnyArgs<T> = { new(...args: any[]): T }
    export type NoArgs<T> = { new(...args: any[]): T }
}
