export class PromiseQueue<Arg, Ret> implements Iterable<Arg> {
    get length() { return this.queue.length; }

    enqueue(arg: Arg): Promise<Ret> {
        const rv = new Promise<Ret>((resolve, reject)=> {
            this.promiseFuncs.push({ resolve: resolve, reject: reject });
            this.queue.push(arg);
        });
        rv.catch(()=> {});
        return rv;
    }

    resolve(ret?: Ret) {
        for (const p of this.promiseFuncs) {
            p.resolve(ret);
        }
        this.clean();
    }

    reject(err: Error) {
        for (const p of this.promiseFuncs) {
            p.reject(err);
        }
        this.clean();
    }

    clean() {
        this.queue.length = 0;
        this.promiseFuncs.length = 0;
    }

    [Symbol.iterator]() {
        let idx = 0;
        let queue = this.queue;

        return {
            next(): IteratorResult<Arg> {
                if (idx < queue.length) {
                    return {
                        done: false,
                        value: queue[idx++]
                    }
                } else {
                    return {
                        done: true,
                        value: undefined
                    }
                }
            }
        }
    }

    private queue = [] as Arg[];
    private promiseFuncs = [] as { resolve: (ret?: Ret)=> void, reject: (err: Error)=> void }[]
}
