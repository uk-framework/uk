import { isNode } from "."

if (isNode) {
    const dotenv = require('dotenv');
    dotenv.config();
}

export function envConfig<TEnv, TCustom>(envConf: TEnv, customConf?: TCustom): TEnv & TCustom {
    for (const name in envConf) {
        if (name in process.env) {
            let val: any = envConf[name];
            switch (typeof val) {
                case 'number':
                    const num = +process.env[name];
                    if (!isNaN(num)) val = num;
                    break;
                default:
                    val = process.env[name];
                    break;
            }
            envConf[name] = val as any;
        }
    }
    return Object.assign(envConf, customConf);
}
