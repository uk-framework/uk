export interface FilePathData {
    root?: string
    dir?: string
    base?: string
    ext?: string
    name?: string

    // aliases
    extname?: string
    basename?: string
    dirname?: string
    stem?: string

    // original path
    path: string

    // getters
    absolute: string,
    isAbsolute: boolean
}
export const parsePath = require('parse-filepath') as (path: string) => FilePathData;
