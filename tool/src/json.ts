import * as JsonSafe from "json-stringify-safe"

export namespace Json {
    export const parse = JSON.parse;
    export const stringify = JSON.stringify;
    export namespace Safe {
        export const stringify = JsonSafe;
        export function parse<T>(text: string, reviverOrDefault?: T|((key: any, value: any) => any), defaultVal?: T): T {
            const isfunc = typeof reviverOrDefault === "function";
            try {
                if (isfunc) {
                    return JSON.parse(text, reviverOrDefault as any);
                } else {
                    return JSON.parse(text);
                }
            } catch (err) {
                return isfunc ? defaultVal : reviverOrDefault as any;
            }
        }
    }
}
