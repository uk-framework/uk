export function route(): route.Route;
export function route<T extends Function>(sub: T): route.Route<T>;
export function route<T extends object>(sub: T): route.Route & T;
export function route<T>(arg?: object | Function): route.Route {
    if (arg instanceof Function) {
        return {
            $call: arg,
            $path: null as string,
        } as any
    } else {
        return {
            $call: function() { return this.$path },
            $path: null as string,
            ...arg
        } as any
    }
}

export namespace route {
    export interface Route<T = () => string> {
        $call: T
        $path: string
    }

    export function prepare<T>(routes: T): T {
        function recurse(path: string, routes: any) {
            for (const name in routes) {
                if (name.startsWith('$')) continue;
                const route = routes[name];
                route.$path = path + '/' + name;
                recurse(route.$path, route);
            }
        }
        recurse('', routes);
        return routes;
    }
}
