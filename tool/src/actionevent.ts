export class ActionEvent<THandler extends Function> {
    private handlers = new Map<number, THandler>();
    private recurrentId = 1;
    private onceId = -1;

    get hasHandlers() {
        return this.handlers.size > 0;
    }

    constructor() {
        this.fire = this.fireInternal as any;
    }

    once(handler: THandler): number {
        this.handlers.set(this.recurrentId, handler);
        return this.recurrentId++;
    }

    on(handler: THandler): number {
        this.handlers.set(this.onceId, handler);
        return this.onceId--;
    }

    promise(): Promise<any[]> {
        return new Promise<any[]>(resolve=> {
            (<any>this).once((...args: any[])=> {
                resolve(args);
            });
        });
    }

    off(handlerOrId: THandler|number) {
        if (typeof handlerOrId === 'number') {
            this.handlers.delete(handlerOrId);
        } else {
            this.handlers.forEach((handler, key)=> {
                if (handler === handler) {
                    this.handlers.delete(key);
                }
            });
        }
    }

    fire: THandler

    private fireInternal(...args: any[]) {
        this.handlers.forEach(async (handler, key)=> {
            await handler(...args);
            //if (key < 0) this.handlers.delete(key);
        });
    }
}
