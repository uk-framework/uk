/// <reference types="node" />
export interface BinBuffer {
    index: number;
    packedBitCount: number;
    source: Buffer;
    packetReadNext: boolean;
    readUInt8(offset?: number, noAssert?: boolean): number;
    readUInt16LE(offset?: number, noAssert?: boolean): number;
    readUInt16BE(offset?: number, noAssert?: boolean): number;
    readUInt32LE(offset?: number, noAssert?: boolean): number;
    readUInt32BE(offset?: number, noAssert?: boolean): number;
    readInt8(offset?: number, noAssert?: boolean): number;
    readInt16LE(offset?: number, noAssert?: boolean): number;
    readInt16BE(offset?: number, noAssert?: boolean): number;
    readInt32LE(offset?: number, noAssert?: boolean): number;
    readInt32BE(offset?: number, noAssert?: boolean): number;
    readPackedInt(offset?: number, noAssert?: boolean): number;
    writeUInt8(value: number, offset?: number, noAssert?: boolean): number;
    writeUInt16LE(value: number, offset?: number, noAssert?: boolean): number;
    writeUInt16BE(value: number, offset?: number, noAssert?: boolean): number;
    writeUInt32LE(value: number, offset?: number, noAssert?: boolean): number;
    writeUInt32BE(value: number, offset?: number, noAssert?: boolean): number;
    writeInt8(value: number, offset?: number, noAssert?: boolean): number;
    writeInt16LE(value: number, offset?: number, noAssert?: boolean): number;
    writeInt16BE(value: number, offset?: number, noAssert?: boolean): number;
    writeInt32LE(value: number, offset?: number, noAssert?: boolean): number;
    writeInt32BE(value: number, offset?: number, noAssert?: boolean): number;
    writeFloatLE(value: number, offset?: number, noAssert?: boolean): number;
    writeFloatBE(value: number, offset?: number, noAssert?: boolean): number;
    writeDoubleLE(value: number, offset?: number, noAssert?: boolean): number;
    writeDoubleBE(value: number, offset?: number, noAssert?: boolean): number;
}
export declare const BinBuffer: {
    new (buffer: Buffer, index?: number): BinBuffer;
};
