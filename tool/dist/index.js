"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNode = (typeof window === 'undefined');
exports.isBrowser = !exports.isNode;
var envconfig_1 = require("./envconfig");
exports.envConfig = envconfig_1.envConfig;
var route_1 = require("./route");
exports.route = route_1.route;
var binbuffer_1 = require("./binbuffer");
exports.BinBuffer = binbuffer_1.BinBuffer;
var promisequeue_1 = require("./promisequeue");
exports.PromiseQueue = promisequeue_1.PromiseQueue;
var json_1 = require("./json");
exports.Json = json_1.Json;
var actionevent_1 = require("./actionevent");
exports.ActionEvent = actionevent_1.ActionEvent;
var parsepath_1 = require("./parsepath");
exports.parsePath = parsepath_1.parsePath;
function noReadOnly(obj) {
    return obj;
}
exports.noReadOnly = noReadOnly;
//# sourceMappingURL=index.js.map