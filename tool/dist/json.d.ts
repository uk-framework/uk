import * as JsonSafe from "json-stringify-safe";
export declare namespace Json {
    const parse: (text: string, reviver?: (this: any, key: string, value: any) => any) => any;
    const stringify: {
        (value: any, replacer?: (this: any, key: string, value: any) => any, space?: string | number): string;
        (value: any, replacer?: (string | number)[], space?: string | number): string;
    };
    namespace Safe {
        const stringify: typeof JsonSafe;
        function parse<T>(text: string, reviverOrDefault?: T | ((key: any, value: any) => any), defaultVal?: T): T;
    }
}
