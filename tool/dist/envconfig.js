"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _1 = require(".");
if (_1.isNode) {
    const dotenv = require('dotenv');
    dotenv.config();
}
function envConfig(envConf, customConf) {
    for (const name in envConf) {
        if (name in process.env) {
            let val = envConf[name];
            switch (typeof val) {
                case 'number':
                    const num = +process.env[name];
                    if (!isNaN(num))
                        val = num;
                    break;
                default:
                    val = process.env[name];
                    break;
            }
            envConf[name] = val;
        }
    }
    return Object.assign(envConf, customConf);
}
exports.envConfig = envConfig;
//# sourceMappingURL=envconfig.js.map