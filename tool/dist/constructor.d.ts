export declare namespace Constructor {
    type AnyArgs<T> = {
        new (...args: any[]): T;
    };
    type NoArgs<T> = {
        new (...args: any[]): T;
    };
}
