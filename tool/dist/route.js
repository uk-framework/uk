"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function route(arg) {
    if (arg instanceof Function) {
        return {
            $call: arg,
            $path: null,
        };
    }
    else {
        return Object.assign({ $call: function () { return this.$path; }, $path: null }, arg);
    }
}
exports.route = route;
(function (route_1) {
    function prepare(routes) {
        function recurse(path, routes) {
            for (const name in routes) {
                if (name.startsWith('$'))
                    continue;
                const route = routes[name];
                route.$path = path + '/' + name;
                recurse(route.$path, route);
            }
        }
        recurse('', routes);
        return routes;
    }
    route_1.prepare = prepare;
})(route = exports.route || (exports.route = {}));
//# sourceMappingURL=route.js.map