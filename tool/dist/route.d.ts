export declare function route(): route.Route;
export declare function route<T extends Function>(sub: T): route.Route<T>;
export declare function route<T extends object>(sub: T): route.Route & T;
export declare namespace route {
    interface Route<T = () => string> {
        $call: T;
        $path: string;
    }
    function prepare<T>(routes: T): T;
}
