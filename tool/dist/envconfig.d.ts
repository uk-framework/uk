export declare function envConfig<TEnv, TCustom>(envConf: TEnv, customConf?: TCustom): TEnv & TCustom;
