export declare class PromiseQueue<Arg, Ret> implements Iterable<Arg> {
    get length(): number;
    enqueue(arg: Arg): Promise<Ret>;
    resolve(ret?: Ret): void;
    reject(err: Error): void;
    clean(): void;
    [Symbol.iterator](): {
        next(): IteratorResult<Arg, any>;
    };
    private queue;
    private promiseFuncs;
}
