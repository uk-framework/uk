"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function createReader(func, byteCount) {
    return function (idx, noassert) {
        const noidx = idx === undefined;
        const rv = func.call(this.source, noidx ? this.index : idx, noassert);
        if (noidx)
            this.index += byteCount;
        return rv;
    };
}
function createWriter(func, byteCount) {
    return function (value, idx, noassert) {
        const noidx = idx === undefined;
        const rv = func.call(this.source, value, noidx ? this.index : idx, noassert);
        if (noidx)
            this.index += byteCount;
        return rv;
    };
}
exports.BinBuffer = function BinBuffer(buffer, index) {
    this.source = buffer;
    this.index = index || 0;
};
for (const n in Buffer.prototype) {
    const member = Buffer.prototype[n];
    if (member instanceof Function) {
        let func;
        switch (n) {
            case 'readUInt8':
            case 'readInt8':
                func = createReader(member, 1);
                break;
            case 'readUInt16LE':
            case 'readUInt16BE':
            case 'readInt16LE':
            case 'readInt16BE':
                func = createReader(member, 2);
                break;
            case 'readUInt32LE':
            case 'readUInt32BE':
            case 'readInt32LE':
            case 'readInt32BE':
                func = createReader(member, 4);
                break;
            case 'writeUInt8':
            case 'writeInt8':
                func = createWriter(member, 1);
                break;
            case 'writeUInt16LE':
            case 'writeUInt16BE':
            case 'writeInt16LE':
            case 'writeInt16BE':
                func = createWriter(member, 2);
                break;
            case 'writeUInt32LE':
            case 'writeUInt32BE':
            case 'writeInt32LE':
            case 'writeInt32BE':
                func = createWriter(member, 4);
                break;
            default:
                func = function () {
                    member.apply(this.source, arguments);
                };
                break;
        }
        exports.BinBuffer.prototype[n] = func;
    }
    else {
        exports.BinBuffer.prototype[n] = member;
    }
}
exports.BinBuffer.prototype.readPackedInt = function (offset, noAssert) {
    if (offset >= 0)
        this.index = +offset;
    let rv = 0, byte = 0, idx = 0;
    this.packetReadNext = false;
    this.packedBitCount = 0;
    do {
        byte = this.source.readUInt8(this.index++);
        rv = rv << 7 | byte & 0x7F;
        this.packedBitCount += 7;
    } while (byte & 0x80 && idx++ < 4);
    this.packetReadNext = (byte & 0x80) !== 0;
    return rv;
};
//# sourceMappingURL=binbuffer.js.map