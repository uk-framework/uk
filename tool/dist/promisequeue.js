"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PromiseQueue {
    constructor() {
        this.queue = [];
        this.promiseFuncs = [];
    }
    get length() { return this.queue.length; }
    enqueue(arg) {
        const rv = new Promise((resolve, reject) => {
            this.promiseFuncs.push({ resolve: resolve, reject: reject });
            this.queue.push(arg);
        });
        rv.catch(() => { });
        return rv;
    }
    resolve(ret) {
        for (const p of this.promiseFuncs) {
            p.resolve(ret);
        }
        this.clean();
    }
    reject(err) {
        for (const p of this.promiseFuncs) {
            p.reject(err);
        }
        this.clean();
    }
    clean() {
        this.queue.length = 0;
        this.promiseFuncs.length = 0;
    }
    [Symbol.iterator]() {
        let idx = 0;
        let queue = this.queue;
        return {
            next() {
                if (idx < queue.length) {
                    return {
                        done: false,
                        value: queue[idx++]
                    };
                }
                else {
                    return {
                        done: true,
                        value: undefined
                    };
                }
            }
        };
    }
}
exports.PromiseQueue = PromiseQueue;
//# sourceMappingURL=promisequeue.js.map