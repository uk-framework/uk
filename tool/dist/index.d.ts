export declare const isNode: boolean;
export declare const isBrowser: boolean;
export { envConfig } from "./envconfig";
export { route } from "./route";
export { BinBuffer } from "./binbuffer";
export { PromiseQueue } from "./promisequeue";
export { Constructor } from "./constructor";
export { Json } from "./json";
export { ActionEvent } from "./actionevent";
export { parsePath } from './parsepath';
declare type Writeable<T> = {
    -readonly [P in keyof T]: T[P];
};
export declare function noReadOnly<T>(obj: T): Writeable<T>;
