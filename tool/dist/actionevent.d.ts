export declare class ActionEvent<THandler extends Function> {
    private handlers;
    private recurrentId;
    private onceId;
    get hasHandlers(): boolean;
    constructor();
    once(handler: THandler): number;
    on(handler: THandler): number;
    promise(): Promise<any[]>;
    off(handlerOrId: THandler | number): void;
    fire: THandler;
    private fireInternal;
}
