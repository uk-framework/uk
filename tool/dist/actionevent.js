"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class ActionEvent {
    constructor() {
        this.handlers = new Map();
        this.recurrentId = 1;
        this.onceId = -1;
        this.fire = this.fireInternal;
    }
    get hasHandlers() {
        return this.handlers.size > 0;
    }
    once(handler) {
        this.handlers.set(this.recurrentId, handler);
        return this.recurrentId++;
    }
    on(handler) {
        this.handlers.set(this.onceId, handler);
        return this.onceId--;
    }
    promise() {
        return new Promise(resolve => {
            this.once((...args) => {
                resolve(args);
            });
        });
    }
    off(handlerOrId) {
        if (typeof handlerOrId === 'number') {
            this.handlers.delete(handlerOrId);
        }
        else {
            this.handlers.forEach((handler, key) => {
                if (handler === handler) {
                    this.handlers.delete(key);
                }
            });
        }
    }
    fireInternal(...args) {
        this.handlers.forEach((handler, key) => __awaiter(this, void 0, void 0, function* () {
            yield handler(...args);
        }));
    }
}
exports.ActionEvent = ActionEvent;
//# sourceMappingURL=actionevent.js.map