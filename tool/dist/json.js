"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const JsonSafe = require("json-stringify-safe");
var Json;
(function (Json) {
    Json.parse = JSON.parse;
    Json.stringify = JSON.stringify;
    let Safe;
    (function (Safe) {
        Safe.stringify = JsonSafe;
        function parse(text, reviverOrDefault, defaultVal) {
            const isfunc = typeof reviverOrDefault === "function";
            try {
                if (isfunc) {
                    return JSON.parse(text, reviverOrDefault);
                }
                else {
                    return JSON.parse(text);
                }
            }
            catch (err) {
                return isfunc ? defaultVal : reviverOrDefault;
            }
        }
        Safe.parse = parse;
    })(Safe = Json.Safe || (Json.Safe = {}));
})(Json = exports.Json || (exports.Json = {}));
//# sourceMappingURL=json.js.map