export interface FilePathData {
    root?: string;
    dir?: string;
    base?: string;
    ext?: string;
    name?: string;
    extname?: string;
    basename?: string;
    dirname?: string;
    stem?: string;
    path: string;
    absolute: string;
    isAbsolute: boolean;
}
export declare const parsePath: (path: string) => FilePathData;
