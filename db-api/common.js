"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function modelToControllerName(model) {
    return model.$.name.toLowerCase();
}
exports.modelToControllerName = modelToControllerName;
//# sourceMappingURL=common.js.map