import * as api from "@uk/api/server";
import { Driver, Model, Row, Query, WhereCondition } from "@uk/db";
export declare class Controller<TApp extends api.Application<TApp['$connectedUser'], TApp['$connectedPeer']>, TModel extends Model.IModel<any>> extends api.Controller<TApp> {
    protected model: TModel;
    constructor(api: TApp, model: TModel);
    insert(docs: Row<any>[]): Promise<{
        pk: any;
    }>;
    update(where: WhereCondition<any>, doc: Row<any>): Promise<Driver.UpdateResult>;
    remove(doc: any): Promise<Driver.RemoveResult>;
    select(query: Query.Serialized): Promise<Driver.SelectResult<any>>;
    private $log;
}
