import { Driver, Model, DriverFactory, Query, Schema, Row, WhereCondition } from "@uk/db"
import { IClient } from "@uk/api/client"
import { modelToControllerName } from "../common"
import { Log } from "@uk/log"

const log = new Log("UK-DBAPI");

export class DbApiDriver<TSchema extends Schema<TSchema>> extends Driver {
    constructor(protected client: IClient) {
        super();
    }

    insert(model: Model<TSchema>, row: Row<TSchema>[]) {
        return this.client.$call(modelToControllerName(model), 'insert', row);
    }

    update(model: Model<TSchema>, where: WhereCondition<TSchema>, doc: Partial<Row<TSchema>>) {
        return this.client.$call(modelToControllerName(model), 'update', where, doc);
    }

    remove(model: Model<TSchema>, row: Row<TSchema>) {
        return this.client.$call(modelToControllerName(model), 'remove', row);
    }

    select(query: Query<TSchema>) {
        return this.client.$call(modelToControllerName(query.model), 'select', Query.serialize(query));
    }

    count(query: Query<TSchema>): Promise<number> {
        return this.client.$call(modelToControllerName(query.model), 'count', Query.serialize(query));
    }

    exists(query: Query<TSchema>): Promise<boolean> {
        return this.client.$call(modelToControllerName(query.model), 'exists', Query.serialize(query));
    }

    release(): void {
    }

    static pool(client: IClient): DriverFactory {
        const drv = new DbApiDriver<any>(client);
        return function() {
            return Promise.resolve(drv);
        }
    }
}
