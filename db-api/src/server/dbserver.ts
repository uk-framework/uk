import * as api from "@uk/api/server"
import { Driver, Model, Row, Query, WhereCondition } from "@uk/db"
import { Log } from "@uk/log"

export class Controller<TApp extends api.Application<TApp['$connectedUser'], TApp['$connectedPeer']>,
                        TModel extends Model.IModel<any>>
                    extends api.Controller<TApp> {
    constructor(api: TApp, protected model: TModel) {
        super(api);
    }

    @api.expose async insert(docs: Row<any>[]) {
        this.$log.info('Inserting', docs);
        const inserted = await this.model.insert(docs, {
            user: this.$app.$connectedUser,
        });
        return { pk: inserted.map((i: any)=> i[this.model.$.pk.name]) };
    }

    @api.expose async update(where: WhereCondition<any>, doc: Row<any>) {
        return this.model.update(where, doc, {
            user: this.$app.$connectedUser,
        });
    }

    @api.expose async remove(doc: any) {
        return this.model.remove(doc, {
            user: this.$app.$connectedUser,
        });
    }

    @api.expose async select(query: Query.Serialized) {
        return Query.deserialize(query, this.model).array();
    }

    private $log = new Log("UK-DBAPI:" + this.model.$.name.toUpperCase());
}
