import { Model, Query } from "@uk/db"

export function modelToControllerName(model: Model.Any) {
    return model.$.name.toLowerCase();
}

export interface SelectResult {
}
