import { Driver, Model, DriverFactory, Query, Schema, Row, WhereCondition } from "@uk/db";
import { IClient } from "@uk/api/client";
export declare class DbApiDriver<TSchema extends Schema<TSchema>> extends Driver {
    protected client: IClient;
    constructor(client: IClient);
    insert(model: Model<TSchema>, row: Row<TSchema>[]): Promise<any>;
    update(model: Model<TSchema>, where: WhereCondition<TSchema>, doc: Partial<Row<TSchema>>): Promise<any>;
    remove(model: Model<TSchema>, row: Row<TSchema>): Promise<any>;
    select(query: Query<TSchema>): Promise<any>;
    count(query: Query<TSchema>): Promise<number>;
    exists(query: Query<TSchema>): Promise<boolean>;
    release(): void;
    static pool(client: IClient): DriverFactory;
}
