"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("@uk/db");
const common_1 = require("../common");
const log_1 = require("@uk/log");
const log = new log_1.Log("UK-DBAPI");
class DbApiDriver extends db_1.Driver {
    constructor(client) {
        super();
        this.client = client;
    }
    insert(model, row) {
        return this.client.$call(common_1.modelToControllerName(model), 'insert', row);
    }
    update(model, where, doc) {
        return this.client.$call(common_1.modelToControllerName(model), 'update', where, doc);
    }
    remove(model, row) {
        return this.client.$call(common_1.modelToControllerName(model), 'remove', row);
    }
    select(query) {
        return this.client.$call(common_1.modelToControllerName(query.model), 'select', db_1.Query.serialize(query));
    }
    count(query) {
        return this.client.$call(common_1.modelToControllerName(query.model), 'count', db_1.Query.serialize(query));
    }
    exists(query) {
        return this.client.$call(common_1.modelToControllerName(query.model), 'exists', db_1.Query.serialize(query));
    }
    release() {
    }
    static pool(client) {
        const drv = new DbApiDriver(client);
        return function () {
            return Promise.resolve(drv);
        };
    }
}
exports.DbApiDriver = DbApiDriver;
//# sourceMappingURL=dbclient.js.map