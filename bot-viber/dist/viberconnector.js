"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const VBot = require('viber-bot');
const ViberBot = VBot.Bot;
const ViberMessage = VBot.Message;
const RichMedia = ViberMessage.RichMedia;
const express = require("express");
const BotEvents = VBot.Events;
const fs = require("fs");
var btoa = require('btoa');
const uuid = require("uuid");
const bot_1 = require("@uk/bot");
const log_1 = require("@uk/log");
const defaultKeyboardStyle = {
    BgColor: "#cdc7f3",
    TextVAlign: "middle",
    TextHAlign: "middle"
};
const log = new log_1.Log("UK.BOTVB");
class ViberConnector extends bot_1.Connector {
    constructor(opts) {
        super();
        this.opts = opts;
        this.log = new log_1.Log(log.tag);
        this.filesStorage = new Map();
    }
    async start(bot) {
        if (!this.opts.key)
            throw new Error("Viber authToken is required!");
        if (!this.opts.port)
            throw new Error("Port is required!");
        if (!this.opts.webHook)
            throw new Error("WebHook is required!");
        if (!this.opts.filedir) {
            this.opts.filedir = __dirname;
            log.info(`Seting as filesStorage path: ${this.opts.filedir}`);
        }
        this.opts.keyboardStyle = (this.opts.keyboardStyle) ? modifyStyle(this.opts.keyboardStyle) : defaultKeyboardStyle;
        this.vbbot = new ViberBot({
            authToken: this.opts.key,
            name: this.opts.name,
            avatar: this.opts.avatar
        });
        this.bot = bot;
        this.vbbot.on(BotEvents.MESSAGE_RECEIVED, async (message, response) => {
            await this.onMessage(Object.assign(Object.assign({}, message), { from: response.userProfile }));
        });
        this.vbbot.on(BotEvents.CONVERSATION_STARTED, async (response, flag, ctx) => {
            log.info(`Get context: ${ctx}`);
            const peer = this.deserializePeer(response.userProfile);
            await this.bot['onMessage'](peer, {
                info: { id: null, chatId: peer.id },
                command: { name: "/start", args: [ctx] }
            });
        });
        this.vbbot.on(BotEvents.SUBSCRIBED, async (response) => {
            const peer = this.deserializePeer(response.userProfile);
            await this.bot['onMessage'](peer, {
                info: { id: null, chatId: peer.id },
                command: { name: "/start" }
            });
        });
        this.vbbot.on(BotEvents.ERROR, (err) => {
            log.fatal(err);
        });
        const app = express();
        try {
            if (!fs.existsSync(this.opts.filedir)) {
                fs.mkdirSync(this.opts.filedir);
            }
            app.use('/files', express.static(`${this.opts.filedir}`));
            app.use(this.vbbot.middleware()).listen(+this.opts.port, this.vbbot.setWebhook(this.opts.webHook.url));
            this.botInfo = await this.getInfoBot();
            log.info("BOT INFO", this.botInfo);
            const fileLivePeriod = +this.opts.fileLivePeriod * 60 * 1000;
            setInterval(() => {
                const currentTime = new Date();
                const storages = this.filesStorage.values();
                for (const storage of storages) {
                    for (const file of storage.values()) {
                        if (+currentTime - fileLivePeriod > +file.loadTime) {
                            fs.unlink(file.path, (err) => {
                                if (err)
                                    throw err;
                                log.debug(`Deleted ${file.path}`);
                                storage.delete(file.path);
                            });
                        }
                    }
                }
            }, fileLivePeriod / 2);
        }
        catch (err) {
            throw new Error(`Can't create bot: ${err}`);
        }
    }
    async onMessage(vbmsg) {
        try {
            const peer = this.deserializePeer(vbmsg.from);
            const ctx = {
                peer: peer,
                vbmsg: vbmsg,
                ukmsg: { info: { id: vbmsg.message_id, chatId: peer.id } },
            };
            this.deserializeText(ctx);
            this.deserializeImages(ctx);
            this.deserializeFile(ctx);
            this.deserializeVideo(ctx);
            this.deserializeContact(ctx);
            this.deserializeCommands(ctx);
            ctx.vbmsg = undefined;
            await this.bot['onMessage'](ctx.peer, ctx.ukmsg);
        }
        catch (err) {
            log.error('onMessage', err);
        }
    }
    async send(peer, msg) {
        if ("document" in msg) {
            const doc = msg.document;
            try {
                const link = this.serializeBuffer(doc.data, doc.type, peer.id);
                const file = new ViberMessage.File(link, doc.data.buffer.byteLength, `${doc.name}${doc.type ? "." + doc.type : ''}`);
                return await this.vbbot.sendMessage(peer, file);
            }
            catch (err) {
                log.error(err);
            }
        }
        if ('images' in msg) {
            const images = Array.isArray(msg.images) ? msg.images : [msg.images];
            for (const image of images) {
                const url = (Buffer.isBuffer(image.image)) ? this.serializeBuffer(image.image, image.type, peer.id) : image.image;
                await this.vbbot.sendMessage(peer, new ViberMessage.Picture(url, image.caption));
            }
            return;
        }
        const sendOptions = {};
        if ('text' in msg) {
            sendOptions.text = msg.text;
        }
        if ('keyboard' in msg && !msg.keyboard.hide) {
            const keys = [];
            const kb = msg.keyboard.input || msg.keyboard.inline;
            let keyboardHeight = kb.length;
            if (kb && kb.length !== 0) {
                for (const keyRow of kb) {
                    const kbwidth = getKeyboardWidth(keyRow.length);
                    for (const key of keyRow) {
                        if ('requestContact' in key) {
                            keys.push({
                                Columns: kbwidth,
                                Rows: 1,
                                Text: `<b>${key.text}</b>`,
                                ActionType: 'share-phone',
                                ActionBody: key.text,
                                TextVAlign: this.opts.keyboardStyle.TextVAlign,
                                TextHAlign: this.opts.keyboardStyle.TextHAlign
                            });
                        }
                        else {
                            keys.push({
                                Columns: kbwidth,
                                Rows: 1,
                                Text: `<b>${key.text}</b>`,
                                ActionType: 'reply',
                                ActionBody: key.text,
                                TextVAlign: this.opts.keyboardStyle.TextVAlign,
                                TextHAlign: this.opts.keyboardStyle.TextHAlign
                            });
                        }
                    }
                }
                ;
                sendOptions.keyboard = {
                    "BgColor": this.opts.keyboardStyle.BgColor,
                    "ButtonsGroupColumns": 6,
                    "ButtonsGroupRows": keyboardHeight,
                    "Type": "keyboard",
                    "Buttons": keys
                };
                if (msg.keyboard.inline) {
                    return await this.vbbot.sendMessage(peer, new RichMedia(sendOptions.keyboard));
                }
            }
        }
        await this.vbbot.sendMessage(peer, new ViberMessage.Text(sendOptions.text, sendOptions.keyboard, null, null, null, 7));
    }
    async sendAction() {
    }
    async confirmPayment() {
    }
    serializeBuffer(buffer, type, peerId) {
        try {
            const dname = btoa(peerId);
            const dpath = `${this.opts.filedir}/${dname}`;
            if (!fs.existsSync(dpath)) {
                fs.mkdirSync(dpath);
            }
            const fName = `${uuid.v4()}${type ? "." + type : ''}`;
            const fpath = `${dpath}/${fName}`;
            fs.writeFileSync(fpath, buffer);
            this.pushToFileStorage(peerId, {
                path: fpath,
                loadTime: new Date()
            });
            return `${this.opts.webHook.url}/files/${dname}/${fName}`;
        }
        catch (err) {
            log.error("Got error");
        }
    }
    async pushToFileStorage(peerId, file) {
        if (!this.filesStorage.has(peerId)) {
            this.filesStorage.set(peerId, new Map());
        }
        const files = this.filesStorage.get(peerId);
        files.set(file.path, file);
        log.debug(`Set file ${file.path} to ${peerId}`);
    }
    async deserializeImages(ctx) {
        if (ctx.vbmsg.thumbnail && !ctx.vbmsg.duration) {
            ctx.ukmsg.images = [];
            ctx.ukmsg.images.push({
                all: [{
                        id: null,
                        path: ctx.vbmsg.url
                    }],
                caption: ctx.vbmsg.text
            });
        }
    }
    async deserializeText(ctx) {
        if (ctx.vbmsg.text)
            ctx.ukmsg.text = ctx.vbmsg.text;
    }
    deserializePeer(user) {
        try {
            return {
                id: user.id,
                connector: this,
                username: user.name,
                sessionId: `${this.botInfo.id}${user.id}}`
            };
        }
        catch (err) {
            log.error(err);
        }
    }
    deserializeCommands(ctx) {
        if (ctx.vbmsg.text && ctx.vbmsg.text.startsWith('/')) {
            let cmd = ctx.vbmsg.text.split(' ');
            ctx.ukmsg.command = { name: cmd[0], args: cmd.slice(1, cmd.length) };
            delete ctx.ukmsg.text;
        }
    }
    deserializeContact(ctx) {
        if (ctx.vbmsg.contactPhoneNumber) {
            ctx.ukmsg.contact = {
                phoneNumber: ctx.vbmsg.contactPhoneNumber,
                name: {
                    first: ctx.vbmsg.from.name.split(' ')[0],
                    last: ctx.vbmsg.from.name.split(' ')[1]
                },
                userId: ctx.peer.id
            };
        }
    }
    deserializeVideo(ctx) {
        if (ctx.vbmsg.duration) {
            ctx.ukmsg.videos = [];
            ctx.ukmsg.videos.push({
                id: ctx.vbmsg,
                path: ctx.vbmsg.url,
                caption: ctx.vbmsg.text,
                size: ctx.vbmsg.size
            });
        }
    }
    deserializeFile(ctx) {
        if (ctx.vbmsg.filename) {
            ctx.ukmsg.document = {
                id: null,
                path: ctx.vbmsg.url,
                name: ctx.vbmsg.filename,
                size: ctx.vbmsg.sieInBytes
            };
        }
    }
    async getInfoBot() {
        try {
            const bot = await this.vbbot.getBotProfile();
            return {
                connector: null,
                id: bot.id.toString(),
                name: {
                    first: bot.name,
                    last: bot.name
                },
                username: bot.uri,
                sessionId: undefined
            };
        }
        catch (err) {
            this.log.error(err);
        }
    }
}
exports.ViberConnector = ViberConnector;
function getKeyboardWidth(btnCount) {
    switch (btnCount) {
        case 3: return 2;
        case 2: return 3;
        case 1: return 6;
        default: return 1;
    }
}
function modifyStyle(style) {
    if (!style)
        return defaultKeyboardStyle;
    for (const key in defaultKeyboardStyle) {
        if (!style[key])
            style[key] = defaultKeyboardStyle[key];
    }
    return style;
}
//# sourceMappingURL=viberconnector.js.map