/// <reference types="node" />
import { Connector, Bot, Out, In, Peer } from "@uk/bot";
import { Log } from "@uk/log";
export interface Context {
    vbmsg: any;
    ukmsg: In.Message;
    peer: Peer;
}
export declare class ViberConnector extends Connector {
    opts: ViberConnector.Options;
    protected bot: Bot<any>;
    protected vbbot: any;
    protected log: Log;
    protected filesStorage: Map<string, Map<string, File>>;
    protected botInfo: Peer;
    constructor(opts: ViberConnector.Options);
    start(bot: Bot<any>): Promise<void>;
    onMessage(vbmsg: any): Promise<void>;
    send(peer: Peer, msg: Out.Message): Promise<any>;
    sendAction(): Promise<void>;
    confirmPayment(): Promise<void>;
    serializeBuffer(buffer: Buffer, type: string, peerId: string): string;
    pushToFileStorage(peerId: string, file: File): Promise<void>;
    deserializeImages(ctx: Context): Promise<void>;
    deserializeText(ctx: Context): Promise<void>;
    deserializePeer(user: any): Peer;
    private deserializeCommands;
    deserializeContact(ctx: Context): void;
    deserializeVideo(ctx: Context): void;
    deserializeFile(ctx: Context): void;
    getInfoBot(): Promise<Peer>;
}
export declare namespace ViberConnector {
    interface Options {
        key: string;
        webHook: {
            url: string;
        };
        name: string;
        avatar: string;
        port: string;
        filedir?: string;
        fileLivePeriod?: string;
        keyboardStyle?: KeyboardStyle;
    }
    interface KeyboardStyle {
        BgColor?: string;
        TextVAlign?: string;
        TextHAlign?: string;
    }
}
export interface File {
    path: string;
    loadTime: Date;
}
