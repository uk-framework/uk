"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const api = require("@uk/api/server");
const model_1 = require("../model");
const auth_1 = require("./auth");
class App extends api.Application {
    constructor() {
        super(...arguments);
        this.auth = new auth_1.Auth(this);
    }
}
exports.App = App;
let t;
model_1.User.create({});
//# sourceMappingURL=app.js.map