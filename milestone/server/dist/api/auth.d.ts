import * as api from "@uk/api/server";
import { App } from ".";
export declare class Auth extends api.Controller<App> {
    login(login: string, password: string): Promise<boolean>;
}
