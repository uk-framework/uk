import * as api from "@uk/api/server";
import { User } from "../model";
import { Auth } from "./auth";
export declare class App extends api.Application<User> {
    auth: Auth;
}
