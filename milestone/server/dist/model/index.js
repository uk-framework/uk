"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const db_1 = require("@uk/db");
const schema = require("@milestone/model");
const db_mongo_1 = require("@uk/db-mongo");
exports.db = new db_1.Database();
exports.User = exports.db.register(schema.User);
exports.UserGroup = exports.db.register(schema.UserGroup);
exports.db.init(db_mongo_1.MongoDriver.pool(""));
exports.UserGroup.save({
    name: "UserGroup-" + Date.now(),
});
//# sourceMappingURL=index.js.map