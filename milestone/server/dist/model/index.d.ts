import { Database, Row } from "@uk/db";
import * as schema from "@milestone/model";
export declare const db: Database;
export declare type User = Row<schema.User>;
export declare const User: {
    name: {
        first: {
            readonly $: schema.field._String;
        };
        last?: {
            readonly $: schema.field._String;
        };
        mid?: {
            readonly $: schema.field._String;
        };
        alias?: {
            readonly $: schema.field._String;
        };
        full?: {
            readonly $: schema.field._Virtual<schema.field._PersonNameData, string>;
        };
    } & {
        readonly $: schema.field._PersonName;
    };
    group: {
        name: {
            readonly $: schema.field._String;
        };
        createdAt: {
            readonly $: schema.field._DateTime;
        };
        updatedAt: {
            readonly $: schema.field._DateTime;
        };
        createdBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        updatedBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        _id: {
            readonly $: schema.field._MongoID;
        };
    } & {
        readonly $: schema.field._One<schema.UserGroup>;
    };
    createdAt: {
        readonly $: schema.field._DateTime;
    };
    updatedAt: {
        readonly $: schema.field._DateTime;
    };
    createdBy: {
        name: {
            first: {
                readonly $: schema.field._String;
            };
            last?: {
                readonly $: schema.field._String;
            };
            mid?: {
                readonly $: schema.field._String;
            };
            alias?: {
                readonly $: schema.field._String;
            };
            full?: {
                readonly $: schema.field._Virtual<schema.field._PersonNameData, string>;
            };
        } & {
            readonly $: schema.field._PersonName;
        };
        group: any & {
            readonly $: schema.field._One<schema.UserGroup>;
        };
        createdAt: {
            readonly $: schema.field._DateTime;
        };
        updatedAt: {
            readonly $: schema.field._DateTime;
        };
        createdBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        updatedBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        _id: {
            readonly $: schema.field._MongoID;
        };
    } & {
        readonly $: schema.field._One<schema.User>;
    };
    updatedBy: {
        name: {
            first: {
                readonly $: schema.field._String;
            };
            last?: {
                readonly $: schema.field._String;
            };
            mid?: {
                readonly $: schema.field._String;
            };
            alias?: {
                readonly $: schema.field._String;
            };
            full?: {
                readonly $: schema.field._Virtual<schema.field._PersonNameData, string>;
            };
        } & {
            readonly $: schema.field._PersonName;
        };
        group: any & {
            readonly $: schema.field._One<schema.UserGroup>;
        };
        createdAt: {
            readonly $: schema.field._DateTime;
        };
        updatedAt: {
            readonly $: schema.field._DateTime;
        };
        createdBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        updatedBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        _id: {
            readonly $: schema.field._MongoID;
        };
    } & {
        readonly $: schema.field._One<schema.User>;
    };
    _id: {
        readonly $: schema.field._MongoID;
    };
} & import("../../../../db/dist/model").Model.IModel<schema.User, import("../../../../db/dist/model").Model.Row<schema.User>> & {};
export declare type UserGroup = Row<schema.UserGroup>;
export declare const UserGroup: {
    name: {
        readonly $: schema.field._String;
    };
    createdAt: {
        readonly $: schema.field._DateTime;
    };
    updatedAt: {
        readonly $: schema.field._DateTime;
    };
    createdBy: {
        name: {
            first: {
                readonly $: schema.field._String;
            };
            last?: {
                readonly $: schema.field._String;
            };
            mid?: {
                readonly $: schema.field._String;
            };
            alias?: {
                readonly $: schema.field._String;
            };
            full?: {
                readonly $: schema.field._Virtual<schema.field._PersonNameData, string>;
            };
        } & {
            readonly $: schema.field._PersonName;
        };
        group: any & {
            readonly $: schema.field._One<schema.UserGroup>;
        };
        createdAt: {
            readonly $: schema.field._DateTime;
        };
        updatedAt: {
            readonly $: schema.field._DateTime;
        };
        createdBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        updatedBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        _id: {
            readonly $: schema.field._MongoID;
        };
    } & {
        readonly $: schema.field._One<schema.User>;
    };
    updatedBy: {
        name: {
            first: {
                readonly $: schema.field._String;
            };
            last?: {
                readonly $: schema.field._String;
            };
            mid?: {
                readonly $: schema.field._String;
            };
            alias?: {
                readonly $: schema.field._String;
            };
            full?: {
                readonly $: schema.field._Virtual<schema.field._PersonNameData, string>;
            };
        } & {
            readonly $: schema.field._PersonName;
        };
        group: any & {
            readonly $: schema.field._One<schema.UserGroup>;
        };
        createdAt: {
            readonly $: schema.field._DateTime;
        };
        updatedAt: {
            readonly $: schema.field._DateTime;
        };
        createdBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        updatedBy: any & {
            readonly $: schema.field._One<schema.User>;
        };
        _id: {
            readonly $: schema.field._MongoID;
        };
    } & {
        readonly $: schema.field._One<schema.User>;
    };
    _id: {
        readonly $: schema.field._MongoID;
    };
} & import("../../../../db/dist/model").Model.IModel<schema.UserGroup, import("../../../../db/dist/model").Model.Row<schema.UserGroup>> & {};
