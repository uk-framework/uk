import * as api from "@uk/api/server"
import { User } from "../model"
import { Auth } from "./auth"

export class App extends api.Application<User> {
    auth = new Auth(this);
}

let t: User;
User.create({});
