import { Database, Row, Model } from "@uk/db"
import * as schema from "@milestone/model"
import { MongoDriver } from "@uk/db-mongo"

export const db = new Database();

function subscribe(model: Model.Any) {
    model.$.pre.insert.on(ctx=> {
        ctx.row.createdBy = ctx.row.updatedBy = ctx.user;
        ctx.row.createdAt = ctx.row.updatedAt = new Date();
    });
    model.$.pre.update.on(ctx=> {
        ctx.patch.updatedBy = ctx.user;
        ctx.patch.updatedAt = new Date();
    });
}

export type User = Row<schema.User>
export const User = db.register(schema.User);
subscribe(User);

export type UserGroup = Row<schema.UserGroup>
export const UserGroup = db.register(schema.UserGroup);

db.init(MongoDriver.pool(""));

UserGroup.save({
    name: "UserGroup-" + Date.now(),

});
