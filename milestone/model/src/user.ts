import { field, Entity } from "./base"
import { UserGroup } from "./usergroup";

export class User extends Entity {
    name = field.PersonName();
    group: field.One<UserGroup> = field.One(UserGroup);
}
