export { field } from "@uk/db"
import { field } from "@uk/db"

export class Base {
    _id = field.MongoID();
}

export class Entity extends Base {
    createdAt = field.DateTime();
    updatedAt = field.DateTime();
    createdBy: field.One<User> = field.One(User);
    updatedBy: field.One<User> = field.One(User);
}

import { User } from "./user"
