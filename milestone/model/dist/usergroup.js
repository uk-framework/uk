"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("./base");
class UserGroup extends base_1.Entity {
    constructor() {
        super(...arguments);
        this.name = base_1.field.String();
    }
}
exports.UserGroup = UserGroup;
//# sourceMappingURL=usergroup.js.map