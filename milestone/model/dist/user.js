"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("./base");
const usergroup_1 = require("./usergroup");
class User extends base_1.Entity {
    constructor() {
        super(...arguments);
        this.name = base_1.field.PersonName();
        this.group = base_1.field.One(usergroup_1.UserGroup);
    }
}
exports.User = User;
//# sourceMappingURL=user.js.map