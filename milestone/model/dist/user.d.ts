import { field, Entity } from "./base";
import { UserGroup } from "./usergroup";
export declare class User extends Entity {
    name: field._PersonName;
    group: field.One<UserGroup>;
}
