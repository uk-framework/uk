export { field } from "@uk/db";
import { field } from "@uk/db";
export declare class Base {
    _id: field._MongoID;
}
export declare class Entity extends Base {
    createdAt: field._DateTime;
    updatedAt: field._DateTime;
    createdBy: field.One<User>;
    updatedBy: field.One<User>;
}
import { User } from "./user";
