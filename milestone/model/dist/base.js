"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var db_1 = require("@uk/db");
exports.field = db_1.field;
const db_2 = require("@uk/db");
class Base {
    constructor() {
        this._id = db_2.field.MongoID();
    }
}
exports.Base = Base;
class Entity extends Base {
    constructor() {
        super(...arguments);
        this.createdAt = db_2.field.DateTime();
        this.updatedAt = db_2.field.DateTime();
        this.createdBy = db_2.field.One(user_1.User);
        this.updatedBy = db_2.field.One(user_1.User);
    }
}
exports.Entity = Entity;
const user_1 = require("./user");
//# sourceMappingURL=base.js.map