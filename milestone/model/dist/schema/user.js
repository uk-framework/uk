"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const base_1 = require("./base");
class User extends base_1.Base {
    constructor() {
        super(...arguments);
        this.name = base_1.field.PersonName();
    }
}
exports.User = User;
//# sourceMappingURL=user.js.map