"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var db_1 = require("@uk/db");
exports.field = db_1.field;
const db_2 = require("@uk/db");
class Base {
    constructor() {
        this._id = db_2.field.MongoID();
    }
}
exports.Base = Base;
//# sourceMappingURL=base.js.map