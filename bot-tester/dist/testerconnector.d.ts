import { Bot, Connector, Peer, In, Out, State, Session } from "@uk/bot";
export { Session };
export interface Context<TSession extends Session> extends Compare {
    msg: In.Message;
    answer: Out.Message;
    state: State<TSession>;
}
export interface Compare {
    equalText?: string;
    equalInputKeyboard?: Out.Keyboard;
}
export declare class BotTester<TSession extends Session> extends Connector {
    bot: Bot;
    answer: Out.Message;
    start(bot: Bot): void;
    getInfoBot(): Promise<Peer>;
    send(peer: Peer, msg: Out.Message): Promise<void>;
    sendAction(): Promise<void>;
    talk(peer: Peer, msg: In.Message, cmp: Compare): Promise<void>;
}
