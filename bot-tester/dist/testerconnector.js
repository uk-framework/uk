"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bot_1 = require("@uk/bot");
exports.Session = bot_1.Session;
const log_1 = require("@uk/log");
const tests = require("./tests");
const log = new log_1.Log(__filename);
class BotTester extends bot_1.Connector {
    start(bot) {
        this.bot = bot;
        tests.start();
    }
    async getInfoBot() {
        return {
            connector: this,
            id: '1111111111',
            name: {
                first: 'bot',
                last: 'tester'
            },
            username: 'bottester'
        };
    }
    async send(peer, msg) {
        peer;
        log.debug('Answer', msg);
        this.answer = msg;
    }
    async sendAction() {
        throw new Error('sendAction not implemented!');
    }
    async talk(peer, msg, cmp) {
        const session = this.bot.sessionByUserId(peer.id);
        if (session && await session.callCallback(msg)) { }
        else {
            await this.bot['onMessage'](peer, msg);
        }
        const ctx = {
            answer: this.answer,
            msg: msg,
            state: session && session.state && session.state
        };
        for (let test in cmp) {
            ctx[test] = cmp[test];
        }
        return tests.run(ctx);
    }
}
exports.BotTester = BotTester;
//# sourceMappingURL=testerconnector.js.map