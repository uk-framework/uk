"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const assert = require("assert-plus");
async function run(ctx) {
    const infoMsg = `MSG: ${JSON.stringify(ctx.msg)}\n  ${ctx.state ? 'NEXT STATE: ' + ctx.state['stateName'].split('.')[1] : 'BOT'}`;
    describe(infoMsg, async function () {
        if (ctx.equalText)
            equalText(ctx);
        if (ctx.equalInputKeyboard)
            equalInputKeyboard(ctx);
    });
}
exports.run = run;
function equalText(ctx) {
    it('equalText', () => {
        if ('text' in ctx.answer) {
            assert.equal(ctx.answer.text, ctx.equalText);
        }
        else
            throw new Error('text was not found in the message');
    });
}
function equalInputKeyboard(ctx) {
    it('equalInputKeyboard', () => {
        if ('input' in ctx.answer.keyboard) {
            ctx.answer.keyboard.input.map((k) => k.map((keyboard) => {
                delete keyboard.onClick;
                delete keyboard['value'];
            }));
            assert.deepEqual(ctx.answer.keyboard.input, ctx.equalInputKeyboard);
        }
        else
            throw new Error('input keyboard was not found in the message');
    });
}
function start() {
    describe("BOT", async function () {
        it("START TEST", () => {
            assert.equal('start', 'start');
        });
    });
}
exports.start = start;
//# sourceMappingURL=tests.js.map