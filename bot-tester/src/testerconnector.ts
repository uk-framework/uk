import { Bot, Connector, Peer, In, Out, State, Session } from "@uk/bot"
import { Log } from "@uk/log"
import * as tests from "./tests";
export { Session }
const log = new Log(__filename)

export interface Context<TSession extends Session> extends Compare {
    msg: In.Message
    answer: Out.Message
    state: State<TSession>
}

export interface Compare {
    equalText?: string
    equalInputKeyboard?: Out.Keyboard
}


export class BotTester<TSession extends Session> extends Connector {
    bot: Bot
    answer: Out.Message

    start(bot: Bot): void {
        this.bot = bot;
        tests.start();
    }
    async getInfoBot(): Promise<Peer> {
        return {
            connector: this,
            id: '1111111111',
            name: {
                first: 'bot',
                last: 'tester'
            },
            username: 'bottester'
        }
    }
    async send(peer: Peer, msg: Out.Message) {
        peer;
        log.debug('Answer', msg)
        this.answer = msg;
    }
    async sendAction() {
        throw new Error('sendAction not implemented!');
    }


    async talk(peer: Peer, msg: In.Message, cmp: Compare) {
        //log.debug('Message', msg);

        const session = this.bot.sessionByUserId(peer.id);
        if (session && await session.callCallback(msg)) { }
        else {
            await this.bot['onMessage'](peer, msg);
        }

        const ctx: Context<TSession> = {
            answer: this.answer,
            msg: msg,
            state: session && session.state && session.state
        }
        for(let test in cmp){
            ctx[test] = cmp[test];
        }
        return tests.run(ctx);
    }
}
