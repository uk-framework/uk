import * as assert from "assert-plus"
import { Context, Session } from "."

export async function run(ctx: Context<Session>) {
    const infoMsg = `MSG: ${JSON.stringify(ctx.msg)}\n  ${ctx.state ? 'NEXT STATE: ' + ctx.state['stateName'].split('.')[1] : 'BOT'}`
    describe(infoMsg, async function() {
        if (ctx.equalText) equalText(ctx);
        if (ctx.equalInputKeyboard) equalInputKeyboard(ctx);
    });
}

function equalText(ctx: Context<Session>) {
    it('equalText', () => {
        if ('text' in ctx.answer) {
            assert.equal(ctx.answer.text, ctx.equalText);
        } else throw new Error('text was not found in the message');
    });
}

function equalInputKeyboard(ctx: Context<Session>) {
    it('equalInputKeyboard', () => {
        if ('input' in ctx.answer.keyboard) {
            ctx.answer.keyboard.input.map((k) => k.map((keyboard) => {
                delete keyboard.onClick
                delete keyboard['value']
            }))
            assert.deepEqual(ctx.answer.keyboard.input, ctx.equalInputKeyboard);
        } else throw new Error('input keyboard was not found in the message');
    });
}

export function start() {
    describe("BOT", async function() {
        it("START TEST", () => {
            assert.equal('start', 'start');
        });
    });
}
//
// function equalInlineKeyboard(infoMsg: any, keyboard: Out.InlineKeyboard, answer: Out.Message){
//     it(JSON.stringify(infoMsg), () => {
//         if('inline' in answer.keyboard){
//             assert.deepEqual(answer.keyboard.inline, keyboard);
//         } else throw new Error('inline keyboard was not found in the message');
//     });
// }
